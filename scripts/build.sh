#!/usr/bin/bash
./setup.sh
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=build/release/conan_toolchain.cmake . -B build/release
cmake --build "build/release"
