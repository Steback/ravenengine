#!/usr/bin/bash
current_directory=$(basename "$PWD")
if [ "$current_directory" == "scripts" ]; then
  cd ..
fi

current_directory="$PWD"
echo "Working directory: $current_directory"

venv_directory="$current_directory/venv"
if [ ! -d "$venv_directory" ]; then
  python -m venv venv
fi

echo "Python venv: $venv_directory"

source "$venv_directory/bin/activate"

echo "Installing python requirements"
pip install -r requirements.txt

build_directory="$current_directory/build"
if [ ! -d  "$build_directory" ]; then
    mkdir "$build_directory"
    echo "Build directory created: $build_directory"
fi

conan_directory="$build_directory/conan"
export CONAN_HOME=$conan_directory

echo "Conan directory: $conan_directory"

conan_profile="$build_directory/conan_profile"
if [ ! -f "$conan_profile" ]; then
    echo "Create conan profile: $conan_profile"
    conan profile detect --name "$conan_profile"
fi

build_directory_debug="$build_directory/debug"
if [ ! -d "$build_directory_debug" ]; then
    mkdir "$build_directory_debug"
    echo "Debug build directory created: $build_directory_debug"
fi

echo "Debug build directory: $build_directory_debug"
conan install . -pr:h="$conan_profile" -pr:b="$conan_profile" -of="$build_directory_debug" --build=missing -s build_type="Debug"

build_directory_release="$build_directory/release"
if [ ! -d "$build_directory_release" ]; then
    mkdir "$build_directory_release"
    echo "Release build directory created: $build_directory_release"
fi

echo "Release build directory: $build_directory_release"
conan install . -pr:h="$conan_profile" -pr:b="$conan_profile" -of="$build_directory_release" --build=missing -s build_type="Release"
