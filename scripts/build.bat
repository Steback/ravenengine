@echo off
call setup.bat

cmake -G "Visual Studio 17 2022" -A x64 -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=build\release\conan_toolchain.cmake . -B build\release
cmake --build "build\release" --config Release
