@echo off
for %%A in ("%CD%") do set "current_directory=%%~nA"
if "%current_directory%" == "scripts" (
    cd ..
)

set "current_directory=%CD%"
echo Working directory: %current_directory%

set "venv_directory=%current_directory%\venv"
if not exist "%venv_directory%" (
    python -m venv venv
)

echo Python venv: %venv_directory%

call "%venv_directory%\Scripts\activate.bat"

echo Installing Python requirements
pip install -r requirements.txt

set "build_directory=%current_directory%\build"
if not exist "%build_directory%" (
    mkdir "%build_directory%"
    echo Build directory created: %build_directory%
)

set "conan_directory=%build_directory%\conan"
set "CONAN_HOME=%conan_directory%"
echo Conan directory: %conan_directory%

set "conan_profile=%build_directory%\conan_profile"
if not exist "%conan_profile%" (
    echo Creating Conan profile: %conan_profile%
    conan profile detect --name "%conan_profile%"
)

set "build_directory_debug=%build_directory%\debug"
if not exist "%build_directory_debug%" (
    mkdir "%build_directory_debug%"
    echo Debug build directory created: %build_directory_debug%
)

echo Debug build directory: %build_directory_debug%
conan install . -pr:h="%conan_profile%" -pr:b="%conan_profile%" -of="%build_directory_debug%" --build=missing -s build_type="Debug"

set "build_directory_release=%build_directory%\release"
if not exist "%build_directory_release%" (
    mkdir "%build_directory_release%"
    echo Release build directory created: %build_directory_release%
)

echo Release build directory: %build_directory_release%
conan install . -pr:h="%conan_profile%" -pr:b="%conan_profile%" -of="%build_directory_release%" --build=missing -s build_type="Release"
