# Raven Engine
Raven engine is a modular and lightweight game engine. Focused in be Cross-Platform, Data-Driven and fully wrote in C. The modular will come at compile time, creating 'modules' that will be linked to the executable, according to platform or other systems. With the Data-Driven in mind, the engine will design using Entity Component system(ECS) architecture. And will be use C, because is the best language for this goal.

## Dependencies
All are installed using the [Conan](https://conan.io/) package manager.

| Name                                         | Version | Licence                                                        |
|----------------------------------------------|---------|----------------------------------------------------------------|
| [cJSON](https://github.com/DaveGamble/cJSON) | 1.17.0  | [MIT](https://github.com/DaveGamble/cJSON/blob/master/LICENSE) |
| [GLFW](https://github.com/glfw/glfw)         | 3.4     | [zlib](https://github.com/glfw/glfw/blob/master/LICENSE.md)    |
| [xxHash](https://github.com/Cyan4973/xxHash) | 0.8.3   | [BSD 2](https://github.com/Cyan4973/xxHash/blob/dev/LICENSE)   |

# Build
### Requirements
* [CMake](https://cmake.org/) >= v3.27.
* [Conan](https://conan.io/) >= v2.0.5.
* [Python](https://www.python.org/) >= v3.11.3.

### Linux
* [GCC](https://gcc.gnu.org/) >= v13.2.1.
```
./scrips/build.sh
```

### Windows
* [Visual Studio](https://visualstudio.microsoft.com/) >= 2022
```
.\scrips\build.bat
```

## Credits
This project couldn't be possible without the help these resources and persons.
* [Travis Vroman's Kohi series](https://www.youtube.com/watch?v=dHPuU-DJoBM&list=PLv8Ddw9K0JPg1BEO-RS-0MYs423cvLVtj)
* [Vulkan tutorial](https://vulkan-tutorial.com/)
* [Sacha Willems vulkan examples](https://github.com/SaschaWillems/Vulkan)
* [Sacha Willems GLTF PBR example](https://github.com/SaschaWillems/Vulkan-glTF-PBR)
* [OpenGL 4 Shading Language Cookbook - Third Edition](https://www.amazon.com/-/es/David-Wolff-dp-1789342252/dp/1789342252/ref=dp_ob_image_bk)
