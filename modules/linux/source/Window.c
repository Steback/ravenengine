#include "RavenEngine/platform/Window.h"

#include <GLFW/glfw3.h>

#include "RavenEngine/input/InputSystem.h"


// TODO: Check events and move this to header file
typedef struct ReWindow {
    ReWindowSize size;
    ReWindowSize framebuffer;
    GLFWwindow* handle;
    ReLogger* logger;
    ReMemoryAllocator* allocator;
    ReEventSystem* eventSystem;
} ReWindow;

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    const ReWindow* handle = glfwGetWindowUserPointer(window);

    ReEventData context;
    context.size = sizeof(ReInputEventKey);

    ReInputEventKey* input = (ReInputEventKey*)context.buffer;
    input->key = key;
    input->mods = mods;
    input->mode = action;

    reEventSystemQueueEvent(handle->eventSystem, RE_EVENT_TYPE_KEY, NULL, &context);
}

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    const ReWindow* handle = glfwGetWindowUserPointer(window);

    ReEventData context;
    context.size = sizeof(ReInputEventKey);

    ReInputEventButton* input = (ReInputEventButton*)context.buffer;
    input->button = button;
    input->mods = mods;
    input->mode = action;

    reEventSystemQueueEvent(handle->eventSystem, RE_EVENT_TYPE_BUTTON, NULL, &context);
}

void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos) {
    const ReWindow* handle = glfwGetWindowUserPointer(window);

    ReEventData context;
    context.size = sizeof(ReInputEventKey);

    ReInputEventMouse* input = (ReInputEventMouse*)context.buffer;
    input->x = (float)xpos;
    input->y = (float)ypos;
    input->mods = RE_INPUT_MOD_NONE;

    reEventSystemQueueEvent(handle->eventSystem, RE_EVENT_TYPE_MOUSE, NULL, &context);
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
    const ReWindow* handle = glfwGetWindowUserPointer(window);

    ReEventData context;
    context.size = sizeof(ReInputEventKey);

    ReInputEventMouseWheel* input = (ReInputEventMouseWheel*)context.buffer;
    input->y = (float)xoffset;
    input->y = (float)yoffset;

    reEventSystemQueueEvent(handle->eventSystem, RE_EVENT_TYPE_WHEEL, NULL, &context);
}

void windowFocusCallback(GLFWwindow* window, int focused) {
    const ReWindow* handle = glfwGetWindowUserPointer(window);

    ReEventData context;
    context.size = sizeof(ReInputEventKey);

    ReInputEventWindowFocus* input = (ReInputEventWindowFocus*)context.buffer;
    input->focused = focused;

    reEventSystemQueueEvent(handle->eventSystem, RE_EVENT_TYPE_WINDOW_FOCUS, NULL, &context);
}

void windowSizeCallback(GLFWwindow* window, int width, int height) {
    // TODO: Implement window size callback
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    // TODO: Implement framebuffer size callback
}

void cursorEnterCallback(GLFWwindow* window, int entered) {
    const ReWindow* handle = glfwGetWindowUserPointer(window);

    ReEventData context;
    context.size = sizeof(ReInputEventKey);

    ReInputEventCursorEnter* input = (ReInputEventCursorEnter*)context.buffer;
    input->entered = entered;

    reEventSystemQueueEvent(handle->eventSystem, RE_EVENT_TYPE_CURSOR_ENTER, NULL, &context);
}

void charCallback(GLFWwindow* window, unsigned int c) {
    const ReWindow* handle = glfwGetWindowUserPointer(window);

    ReEventData context;
    context.size = sizeof(ReInputEventKey);

    ReInputEventChar* input = (ReInputEventChar*)context.buffer;
    input->character = c;

    reEventSystemQueueEvent(handle->eventSystem, RE_EVENT_TYPE_CHAR, NULL, &context);
}

void errorCallback(int error, const char* description) {
    LOG_ERROR(NULL, "GLFW error: %d | %s", error, description);
}

ReWindow* reWindowCreate(const ReWindowCreateInfo* createInfo) {
    glfwSetErrorCallback(errorCallback);

    if (glfwInit() == GLFW_FALSE) {
        LOG_ERROR(createInfo->logger, "Failed to initialize GLFW!");

        return NULL;
    }

    ReWindow* window = RE_ALLOCATE(createInfo->allocator, ReWindow, RE_MEMORY_TAG_ENGINE);
    window->allocator = createInfo->allocator;
    window->logger = createInfo->logger;
    window->eventSystem = createInfo->eventSystem;

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    window->handle = glfwCreateWindow(createInfo->size.width, createInfo->size.height, createInfo->title, NULL, NULL);
    if (window->handle == NULL) {
        reWindowDestroy(window);

        LOG_ERROR(window->logger, "Failed to create GLFW window");

        return NULL;
    }

    glfwSetWindowUserPointer(window->handle, window);
    glfwSetKeyCallback(window->handle, keyCallback);
    glfwSetMouseButtonCallback(window->handle, mouseButtonCallback);
    glfwSetCursorPosCallback(window->handle, cursorPositionCallback);
    glfwSetScrollCallback(window->handle, scrollCallback);
    glfwSetWindowFocusCallback(window->handle, windowFocusCallback);
    glfwSetWindowSizeCallback(window->handle, windowSizeCallback);
    glfwSetFramebufferSizeCallback(window->handle, framebufferSizeCallback);
    glfwSetCursorEnterCallback(window->handle, cursorEnterCallback);
    glfwSetCharCallback(window->handle, charCallback);
    glfwGetWindowSize(window->handle, &window->size.width, &window->size.height);
    glfwGetFramebufferSize(window->handle, &window->framebuffer.width, &window->framebuffer.height);

    return window;
}

void reWindowDestroy(ReWindow* window) {
    if (window->handle != NULL) {
        glfwDestroyWindow(window->handle);
    }

    RE_FREE(window->allocator, window, RE_MEMORY_TAG_ENGINE);

    glfwTerminate();
}

ReWindowSize reWindowGetSize(const ReWindow* window) {
    return window->size;
}

ReWindowSize reWindowGetFramebufferSize(const ReWindow* window) {
    return window->framebuffer;
}

void reWindowUpdateTitle(const ReWindow* window, const char* title) {
    glfwSetWindowTitle(window->handle, title);
}

b8 reWindowProcessInput(const ReWindow* window) {
    glfwPollEvents();

    return !glfwWindowShouldClose(window->handle);
}
