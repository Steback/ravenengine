#include "RavenEngine/platform/Platform.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/sysinfo.h>

#include <GLFW/glfw3.h>

#include "RavenEngine/filesystem/Path.h"


#define RE_PLATFORM_LINUX_NAME "Linux"

#define MAGENTA_COLOR "\033[35m"
#define BLACK_COLOR "\033[1;30m"
#define RED_COLOR "\033[1;31m"
#define GREEN_COLOR "\033[1;32m"
#define YELLOW_COLOR "\033[1;33m"
#define BLUE_COLOR "\033[1;34m"
#define PURPLE_COLOR "\033[1;35m"
#define CYAN_COLOR "\033[1;36m"
#define WHITE_COLOR "\033[1;37m"

#define RESET_COLOR "\033[0m"


static char* linuxConsoleColor[RE_CONSOLE_COLOR_MAX] = {
    RESET_COLOR,
    MAGENTA_COLOR,
    RED_COLOR,
    YELLOW_COLOR,
    GREEN_COLOR,
    BLUE_COLOR,
    CYAN_COLOR
};

RePlatform* rePlatformCreate(ReMemoryAllocator* allocator) {
    RePlatform* platform = RE_ALLOCATE(allocator, RePlatform, RE_MEMORY_TAG_ENGINE);
    platform->allocator = allocator;
    strcpy(platform->name, RE_PLATFORM_LINUX_NAME);

    FILE* osFile = fopen("/etc/os-release", "r");
    if (osFile != NULL) {
        char line[256];
        while (fgets(line, sizeof(line), osFile)) {
            if (strncmp(line, "PRETTY_NAME=", 12) == 0) {
                char* name = strchr(line, '=');
                if (name != NULL) {
                    name++; // Skip '='
                    name[strcspn(name, "\n")] = 0; // Remove newline
                    if (name[0] == '"' && name[strlen(name) - 1] == '"') {
                        name[strlen(name) - 1] = '\0'; // Remove trailing quote
                        name++; // Remove leading quote
                    }

                    strcpy(platform->os, name);
                    break;
                }
            }
        }

        fclose(osFile);
    }

    FILE* cpuInfoFile = fopen("/proc/cpuinfo", "r");
    if (cpuInfoFile != NULL) {
        static char cpu[128];
        char line[256];
        while (fgets(line, sizeof(line), cpuInfoFile)) {
            if (strncmp(line, "model name", 10) == 0) {
                const char* name = strchr(line, ':');
                if (name != NULL) {
                    name++; // Skip the ':'
                    if (name[0] == ' ') {
                        name++; // Skip the ' '
                    }

                    strncpy(cpu, name, sizeof(cpu) - 1);
                    cpu[strcspn(cpu, "\n")] = 0; // Remove newline

                    strcpy(platform->cpu, cpu);

                    break;
                }
            }
        }

        fclose(cpuInfoFile);
    }

    struct sysinfo sys_info;
    if (sysinfo(&sys_info) == 0) {
        platform->ram = sys_info.totalram / (1024 * 1024); // Convert to MB
    }

    return platform;
}

void rePlatformDestroy(RePlatform* platform) {
    RE_FREE(platform->allocator, platform, RE_MEMORY_TAG_ENGINE);
}

void rePlatformConsoleWrite(const ReConsoleColor color, const char* fmt, ...) {
    char output[RE_MAX_MESSAGE_SIZE];

    __builtin_va_list args;
    va_start(args, fmt);
    vsnprintf(output, RE_MAX_MESSAGE_SIZE, fmt, args);
    va_end(args);

    printf("%s%s%s\n", linuxConsoleColor[color], output, RESET_COLOR);
}

void* rePlatformMemoryAllocate(const u64 size) {
    return calloc(1, size);
}

void* rePlatformMemoryReallocate(void* block, u64 size) {
    return realloc(block, size);
}

void rePlatformMemoryFree(void* block) {
    free(block);
}

void* rePlatformMemoryZero(void* block, const u64 size) {
    return memset(block, 0, size);
}

void rePlatformMemoryCopy(void* dst, const void* src, const u64 size) {
    memcpy(dst, src, size);
}

void* rePlatformMemorySet(void* block, const i32 value, const u64 size) {
    return memset(block, value, size);
}

b8 rePlatformPathExists(const char* path) {
    DIR* dir = opendir(path);
    if (dir != NULL) {
        closedir(dir);

        return true;
    }

    FILE* file = fopen(path, "r");
    if (file != NULL) {
        fclose(file);

        return true;
    }

    return false;
}

b8 rePlatformPathIsDirectory(const char* path) {
    DIR* dir = opendir(path);
    if (dir != NULL) {
        closedir(dir);

        return true;
    }

    return false;
}

b8 rePlatformPathIsAbsolute(const char* path) {
    if (path[0] == '/') {
        return true;
    }

    return false;
}

void rePlatformGetCurrentPath(char* output) {
    getcwd(output, RE_PATH_MAX_SIZE);
}

void rePlatformCreateDirectory(const char* path) {
    struct stat st = {0};
    if (stat(path, &st) == -1) {
        mkdir(path, 0700);
    }
}

f64 rePlatformGetCurrentTime() {
    return glfwGetTime();
}
