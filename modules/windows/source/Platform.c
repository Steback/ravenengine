#include "RavenEngine/platform/Platform.h"

#include <stdio.h>
#include <stdarg.h>
#include <direct.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <sys/stat.h>

#include <GLFW/glfw3.h>

#include "RavenEngine/core/Memory.h"
#include "RavenEngine/filesystem/Path.h"


#define RE_PLATFORM_WINDOWS_NAME "Windows"
#define RE_CONSOLE_RESET_COLOR (FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED)


static i32 windowsConsoleColors[RE_CONSOLE_COLOR_MAX] = {
    RE_CONSOLE_RESET_COLOR,
    FOREGROUND_RED | FOREGROUND_BLUE,
    FOREGROUND_RED,
    FOREGROUND_RED | FOREGROUND_GREEN,
    FOREGROUND_GREEN,
    FOREGROUND_BLUE,
    FOREGROUND_GREEN | FOREGROUND_BLUE
};

RePlatform* rePlatformCreate() {
    RePlatform* platform = reMemoryAllocate(sizeof(RePlatform), RE_MEMORY_TAG_ENGINE);
    strcpy_s(platform->name, 16, RE_PLATFORM_WINDOWS_NAME);

    OSVERSIONINFOEX osvi;
    reMemoryZero(&osvi, sizeof(osvi));

    osvi.dwOSVersionInfoSize = sizeof(osvi);
    if (GetVersionEx((OSVERSIONINFO*)&osvi)) {
        snprintf(platform->os, 32, "Windows %ld.%ld", osvi.dwMajorVersion, osvi.dwMinorVersion);
    } else {
        snprintf(platform->os, 32, "Unknown OS");
    }

    SYSTEM_INFO si;
    GetSystemInfo(&si);

    switch (si.wProcessorArchitecture) {
        case PROCESSOR_ARCHITECTURE_AMD64:
            snprintf(platform->cpu, 64, "x64 (AMD or Intel)");
            break;
        case PROCESSOR_ARCHITECTURE_INTEL:
            snprintf(platform->cpu, 64, "x86 (Intel)");
            break;
        case PROCESSOR_ARCHITECTURE_ARM:
            snprintf(platform->cpu, 64, "ARM");
            break;
        case PROCESSOR_ARCHITECTURE_IA64:
            snprintf(platform->cpu, 64, "IA-64 (Intel Itanium)");
            break;
        default:
            snprintf(platform->cpu, 64, "Unknown CPU Architecture");
            break;
    }

    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof(statex);

    if (GlobalMemoryStatusEx(&statex)) {
        platform->ram = (u32)(statex.ullTotalPhys / (1024 * 1024));
    } else {
        platform->ram = 0;
    }

    return platform;
}

void rePlatformDestroy(RePlatform* platform) {
    reMemoryFree(platform, sizeof(RePlatform), RE_MEMORY_TAG_ENGINE);
}

void rePlatformConsoleWrite(const ReConsoleColor color, const char* fmt, ...) {
    char output[RE_MAX_MESSAGE_SIZE];

    va_list args;
    va_start(args, fmt);
    vsnprintf(output, RE_MAX_MESSAGE_SIZE, fmt, args);
    va_end(args);

    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), windowsConsoleColors[color]);
    printf("%s\n", output);
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), RE_CONSOLE_RESET_COLOR);
}

void* rePlatformMemoryAllocate(const u64 size) {
    return malloc(size);
}

void rePlatformMemoryFree(void* block) {
    free(block);
}

void* rePlatformMemoryZero(void* block, const u64 size) {
    return memset(block, 0, size);
}

void rePlatformMemoryCopy(void* dst, const void* src, const u64 size) {
    _memccpy(dst, src, 0, size);
}

void* rePlatformMemorySet(void* block, const i32 value, const u64 size) {
    return memset(block, value, size);
}

b8 rePlatformPathExists(const char* path) {
    WIN32_FIND_DATA findFileData;
    HANDLE hFind = FindFirstFile(path, &findFileData);
    if (hFind != INVALID_HANDLE_VALUE) {
        FindClose(hFind);

        return true;
    }

    FILE* file = NULL;
    fopen_s(&file, path, "r");
    if (file != NULL) {
        fclose(file);

        return true;
    }

    return false;
}

b8 rePlatformPathIsDirectory(const char* path) {
    WIN32_FIND_DATA findFileData;
    HANDLE hFind = FindFirstFile(path, &findFileData);
    if (hFind != INVALID_HANDLE_VALUE) {
        FindClose(hFind);

        return true;
    }

    return false;
}

b8 rePlatformPathIsAbsolute(const char* path) {
    if (path[0] == '/') {
        return true;
    }

    return false;
}

void rePlatformGetCurrentPath(char* output) {
    _getcwd(output, RE_PATH_MAX_SIZE);
}

void rePlatformCreateDirectory(const char* path) {
    struct stat st = {0};
    if (stat(path, &st) == -1) {
        _mkdir(path);
    }
}

f64 rePlatformGetCurrentTime() {
    return glfwGetTime();
}
