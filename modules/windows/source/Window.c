#include "RavenEngine/platform/Window.h"

#include <GLFW/glfw3.h>


typedef struct ReWindow {
    ReWindowSize size;
    ReWindowSize framebuffer;
    GLFWwindow* handle;
    ReLogger* logger;
    ReMemoryAllocator* allocator;
} ReWindow;


void errorCallback(int error, const char* description) {
    LOG_ERROR(NULL, "GLFW error: %d | %s", error, description);
}

ReWindow* reWindowCreate(const ReWindowCreateInfo* createInfo) {
    glfwSetErrorCallback(errorCallback);

    if (glfwInit() == GLFW_FALSE) {
        LOG_ERROR(createInfo->logger, "Failed to initialize GLFW!");

        return NULL;
    }

    ReWindow* window = RE_ALLOCATE(createInfo->allocator, ReWindow, RE_MEMORY_TAG_ENGINE);
    window->allocator = createInfo->allocator;
    window->logger = createInfo->logger;

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    window->handle = glfwCreateWindow(createInfo->size.width, createInfo->size.height, createInfo->title, NULL, NULL);
    if (window->handle == NULL) {
        reWindowDestroy(window);

        LOG_ERROR(window->logger, "Failed to create GLFW window");

        return NULL;
    }

    glfwSetWindowUserPointer(window->handle, window);
    glfwGetWindowSize(window->handle, &window->size.width, &window->size.height);
    glfwGetFramebufferSize(window->handle, &window->framebuffer.width, &window->framebuffer.height);

    return window;
}

void reWindowDestroy(ReWindow* window) {
    if (window->handle != NULL) {
        glfwDestroyWindow(window->handle);
    }

    RE_FREE(window->allocator, window, ReWindow, RE_MEMORY_TAG_ENGINE);

    glfwTerminate();
}

ReWindowSize reWindowGetSize(const ReWindow* window) {
    return window->size;
}

ReWindowSize reWindowGetFramebufferSize(const ReWindow* window) {
    return window->framebuffer;
}

void reWindowUpdateTitle(const ReWindow* window, const char* title) {
    glfwSetWindowTitle(window->handle, title);
}

b8 reWindowProcessInput(const ReWindow* window) {
    glfwPollEvents();

    return !glfwWindowShouldClose(window->handle);
}
