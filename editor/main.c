#include "RavenEditor/Editor.h"


int main() {
    ReApplicationCreateInfo applicationCreateInfo = {};
    applicationCreateInfo.name = EDITOR_NAME;
    applicationCreateInfo.setup = reEditorSetup;
    applicationCreateInfo.shutdown = reEditorShutdown;
    applicationCreateInfo.update = reEditorUpdate;
    applicationCreateInfo.render = reEditorRender;

    ReEngineCreateInfo engineCreateInfo = {};
    engineCreateInfo.applicationCreateInfo = &applicationCreateInfo;

    ReEngine* context = reSetup(&engineCreateInfo);
    reRun(context);

    return 0;
}