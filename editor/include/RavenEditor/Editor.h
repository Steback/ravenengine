/// @file Editor.h
#ifndef RAVEN_EDITOR_EDITOR_H
#define RAVEN_EDITOR_EDITOR_H


#include "RavenEditor/Defines.h"
#include "RavenEngine/core/Application.h"


/**
 * @brief Editor setup
 * @param application Application reference
 * @param context Engine context reference
 */
void reEditorSetup(ReApplication* application, ReEngine* context);

/**
 * @brief Editor shutdown
 * @param application Application reference
 * @param context Engine context reference
 */
void reEditorShutdown(ReApplication* application, ReEngine* context);

/**
 * @brief Editor update
 * @param application Application reference
 * @param context Engine context reference
 * @param dt Deltatime
 */
void reEditorUpdate(ReApplication* application, ReEngine* context, float dt);

/**
 * @brief Editor renderer
 * @param application Application reference
 * @param context Engine context reference
 */
void reEditorRender(ReApplication* application, ReEngine* context);


#endif //RAVEN_EDITOR_EDITOR_H
