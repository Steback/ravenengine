#include "RavenEditor/Editor.h"

#include <RavenEngine/core/Logger.h>


void reEditorSetup(ReApplication* application, ReEngine* context) {
    LOG_INFO(application->logger, "%s v%d.%d.%d", EDITOR_NAME, EDITOR_VERSION_MAJOR, EDITOR_VERSION_MINOR, EDITOR_VERSION_PATCH);
}

void reEditorShutdown(ReApplication* application, ReEngine* context) {
}

void reEditorUpdate(ReApplication* application, ReEngine* context, float dt) {
}

void reEditorRender(ReApplication* application, ReEngine* context) {
}
