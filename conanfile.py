import os

from conan import ConanFile
from conan.tools.files import copy


class MultiplayerGameRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "CMakeToolchain", "CMakeDeps"

    def requirements(self):
        self.requires("cjson/1.7.18")
        self.requires("glfw/3.4")
        self.requires("xxhash/0.8.2")

    def configure(self):
        pass

    def generate(self):
        pass
