#include "RavenEngine/containers/String.h"

#include <string.h>


ReString reStringCreate(const char* data, const u32 length, ReMemoryAllocator* allocator) {
    ReString string = {};
    if (length > 0) {
        string.data = RE_ALLOCATE_SIZE(allocator, length + 1, RE_MEMORY_TAG_STRING);

        reMemoryCopy(string.data, data, length);

        string.data[length] = '\0';
        string.length = length;
    } else {
        string.data = NULL;
        string.length = 0;
    }

    string.allocator = allocator;

    return string;
}

void reStringDestroy(ReString* string) {
    reStringClear(string);

    string->allocator = NULL;
}

void reStringClear(ReString* string) {
    if (string->data != NULL) {
        RE_FREE_SIZE(string->allocator, string->data, RE_MEMORY_TAG_STRING);
    }

    string->length = 0;
    string->data = NULL;
}

void reStringAppend(ReString* string, const char* data) {
    const u32 length = strlen(data);
    const u32 newLength = string->length + length + 1;
    if (string->length == 0) {
        RE_ALLOCATE_SIZE(string->allocator, newLength, RE_MEMORY_TAG_STRING);
    } else {
        RE_REALLOCATE(string->allocator, string->data, newLength, RE_MEMORY_TAG_STRING);
    }

    strcat(string->data, data);

    string->length = newLength;
}

b8 reStringEqual(const ReString* a, const ReString* b) {
    return strcmp(a->data, b->data) == 0;
}

i32 reStringFind(const ReString* string, const char* data) {
    const char* p = strstr(string->data, data);
    if (p == NULL) {
        return -1;
    }

    return (i32)(p - string->data);
}

ReString reStringCopy(const ReString* string) {
    ReString copy = {};
    copy.length = string->length;
    copy.allocator = string->allocator;
    copy.data = RE_ALLOCATE_SIZE(string->allocator, string->length + 1, RE_MEMORY_TAG_STRING);

    reMemoryCopy(copy.data, string->data, string->length + 1);

    return copy;
}

ReString reStringSubstring(const ReString* string, i32 index, u32 length) {
    ReString copy = {};
    copy.length = length + 1;
    copy.allocator = string->allocator;
    copy.data = RE_ALLOCATE_SIZE(string->allocator, length + 1, RE_MEMORY_TAG_STRING);

    reMemoryCopy(copy.data + index, string->data, length);

    return copy;
}
