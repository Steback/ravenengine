#include "RavenEngine/core/Logger.h"

#include <time.h>
#include <stdio.h>
#include <stdarg.h>

#include "RavenEngine/filesystem/Path.h"
#include "RavenEngine/platform/Platform.h"


static const char* levelStr[RE_LOG_LEVEL_MAX] = {
    "Fatal",
    "Error",
    "Warn",
    "Info",
    "Debug",
    "Trace"
};

static i32 levelColor[RE_LOG_LEVEL_MAX] = {
    RE_CONSOLE_COLOR_MAGENTA,
    RE_CONSOLE_COLOR_RED,
    RE_CONSOLE_COLOR_YELLOW,
    RE_CONSOLE_COLOR_GREEN,
    RE_CONSOLE_COLOR_BLUE,
    RE_CONSOLE_COLOR_CYAN
};

ReLogger* reLoggerCreate(const ReLoggerCreteInfo* creationInfo) {
    ReLogger* logger = RE_ALLOCATE(creationInfo->allocator, ReLogger, RE_MEMORY_TAG_LOGGER);
    logger->callback = creationInfo->callback;
    logger->allocator = creationInfo->allocator;
    logger->file = fopen(creationInfo->path->path, "w+");

    return logger;
}

void reLoggerDestroy(ReLogger* logger) {
    if (logger->file != NULL) {
        fclose(logger->file);
    }

    RE_FREE(logger->allocator, logger, RE_MEMORY_TAG_LOGGER);
}

void reLoggerLog(const ReLogger* logger, const ReLogLevel level, const char* file, const unsigned int line, const char* fmt, ...) {
    char formatted[RE_MAX_MESSAGE_SIZE];

#ifdef _MSC_VER
    va_list args;
#else
    __builtin_va_list args;
#endif
    va_start(args, fmt);
    vsnprintf(formatted, RE_MAX_MESSAGE_SIZE, fmt, args);
    va_end(args);

    time_t timer;
    char timeStr[26];
    timer = time(NULL);
    const struct tm* tmInfo = localtime(&timer);
    strftime(timeStr, RE_MAX_DATE_FORMAt_SIZE, "%Y-%m-%d %H:%M:%S", tmInfo);

    rePlatformConsoleWrite(levelColor[level], "[%s] [%s] [%s:%u] %s", timeStr, levelStr[level], file, line, formatted);

    if (logger != NULL) {
        if (logger->file != NULL) {
            fprintf(logger->file, "[%s] [%s] [%s:%u] %s\n", timeStr, levelStr[level], file, line, formatted);
            fflush(logger->file);
        }

        if (logger->callback != NULL) {
            logger->callback(level, file, line, formatted);
        }
    }
}
