#include "RavenEngine/core/Memory.h"

#include "RavenEngine/core/Assert.h"
#include "RavenEngine/core/Logger.h"
#include "RavenEngine/utils/ToString.h"
#include "RavenEngine/platform/Platform.h"


ReMemorySystem* reMemorySystemCreate(const ReMemorySystemCreateInfo* createInfo) {
    u8* memory = rePlatformMemoryAllocate(createInfo->blockSize);
    if (memory == NULL) {
        return NULL;
    }

    const u32 size = RE_GET_SIZE_ALIGNED(sizeof(ReMemorySystem), RE_DEFAULT_ALIGNMENT);
    if (size >= createInfo->blockSize) {
        rePlatformMemoryFree(memory);

        return NULL;
    }

    ReMemorySystem* system = (ReMemorySystem*)memory;
    system->size = createInfo->blockSize - size;
    system->memory = memory + size;

    return system;
}

void reMemorySystemDestroy(ReMemorySystem* self) {
    RE_ASSERT(self);
    RE_ASSERT(self->memory);

    self->size = 0;
    self->memory = NULL;

    rePlatformMemoryFree(self);
}

ReMemoryAllocator* reMemoryAllocatorCreate(const ReMemoryAllocatorCreateInfo* createInfo) {
    ReMemoryAllocator* allocator = rePlatformMemoryAllocate(sizeof(ReMemoryAllocator));
    allocator->system = createInfo->system;
    allocator->allocate = createInfo->allocate;
    allocator->reallocate = createInfo->reallocate;
    allocator->free = createInfo->free;

    return allocator;
}

void reMemoryAllocatorDestroy(ReMemoryAllocator* self) {
    rePlatformMemoryFree(self);
}

void* reMemoryAllocate(ReMemoryAllocator* allocator, const u64 size, const ReMemoryTag tag) {
    if (tag == RE_MEMORY_TAG_UNKNOWN) {
        LOG_WARN(NULL, "Memory tag isn't know to allocate: %s", reToStringMemoryTag(tag));
    }

    if (allocator == NULL) {
        LOG_WARN(NULL, "Using heap to allocate memory block: %lu | %s", size, reToStringMemoryTag(tag));

        return rePlatformMemoryAllocate(size);
    }

    return allocator->allocate(allocator, size, tag);
}

void* reMemoryReallocate(ReMemoryAllocator* allocator, void* block, const u64 size, const ReMemoryTag tag) {
    if (tag == RE_MEMORY_TAG_UNKNOWN) {
        LOG_WARN(NULL, "Memory tag isn't know to reallocate: %s", reToStringMemoryTag(tag));
    }

    if (allocator == NULL) {
        LOG_WARN(NULL, "Using heap to reallocate memory block: %lu | %s", size, reToStringMemoryTag(tag));

        return rePlatformMemoryReallocate(block, size);
    }

    return allocator->reallocate(allocator, block, size, tag);
}

void reMemoryFree(ReMemoryAllocator* allocator, void* block, const ReMemoryTag tag) {
    if (tag == RE_MEMORY_TAG_UNKNOWN) {
        LOG_WARN(NULL, "Memory tag isn't know to free: %s", reToStringMemoryTag(tag));
    }

    if (allocator == NULL) {
        LOG_WARN(NULL, "Using heap to free memory block | %s", reToStringMemoryTag(tag));

        return rePlatformMemoryFree(block);
    }

    allocator->free(allocator, block, tag);
}

void* reMemoryZero(void* block, const u64 size) {
    return rePlatformMemoryZero(block, size);
}

void reMemoryCopy(void* dst, const void* src, const u64 size) {
    rePlatformMemoryCopy(dst, src, size);
}

void* reMemorySet(void* dst, const i32 value, const u64 size) {
    return rePlatformMemorySet(dst, value, size);
}
