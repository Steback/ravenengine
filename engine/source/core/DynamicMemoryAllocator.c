#include "RavenEngine/core/DynamicMemoryAllocator.h"

#include "RavenEngine/core/Assert.h"
#include "RavenEngine/utils/ToString.h"


void* reDynamicMemoryAllocatorAllocate(ReMemoryAllocator* self, u64 size, ReMemoryTag tag);

void* reDynamicMemoryAllocatorReallocate(ReMemoryAllocator* self, void* ptr, u64 size, ReMemoryTag tag);

void reDynamicMemoryAllocatorFree(ReMemoryAllocator* self, void* ptr, ReMemoryTag tag);

typedef struct ReMemoryFreeBlock {
    u32 size;
    struct ReMemoryFreeBlock* next;
} ReMemoryFreeBlock;

typedef struct ReDynamicMemoryAllocator {
    ReMemoryFreeBlock* freeBlock;
} ReDynamicMemoryAllocator;

typedef struct ReMemoryBlockHeader {
    u32 size;
} ReMemoryBlockHeader;

ReMemoryAllocator* reDynamicMemoryAllocatorCreate(ReMemoryAllocatorCreateInfo* createInfo) {
    RE_ASSERT(createInfo);
    RE_ASSERT(createInfo->system);
    RE_ASSERT(createInfo->system->memory);

    const u32 size = RE_GET_SIZE_ALIGNED(sizeof(ReMemoryAllocator) + sizeof(ReDynamicMemoryAllocator), RE_DEFAULT_ALIGNMENT);
    RE_ASSERT(size <= createInfo->system->size);

    u8* memory = createInfo->system->memory;
    ReMemoryAllocator* allocator = (ReMemoryAllocator*)memory;
    allocator->alignment = createInfo->alignment;
    allocator->system = createInfo->system;

    allocator->internal = (void*)(memory + sizeof(ReMemoryAllocator));
    ReDynamicMemoryAllocator* dynamicMemoryAllocator = (ReDynamicMemoryAllocator*)allocator->internal;

    dynamicMemoryAllocator->freeBlock = (ReMemoryFreeBlock*)(memory + size);
    dynamicMemoryAllocator->freeBlock->size = createInfo->system->size - size;
    dynamicMemoryAllocator->freeBlock->next = NULL;

    allocator->allocate = reDynamicMemoryAllocatorAllocate;
    allocator->reallocate = reDynamicMemoryAllocatorReallocate;
    allocator->free = reDynamicMemoryAllocatorFree;

    return allocator;
}

void reDynamicMemoryAllocatorDestroy(ReMemoryAllocator* self) {
    RE_ASSERT(self);

    ReDynamicMemoryAllocator* allocator = (ReDynamicMemoryAllocator*)self->internal;
    allocator->freeBlock = NULL;

    self->internal = NULL;

    reMemoryZero(self->system->memory, self->system->size);

    self = NULL;
}

void* reDynamicMemoryAllocatorAllocate(ReMemoryAllocator* self, u64 size, ReMemoryTag tag) {
    RE_ASSERT(self != NULL);
    RE_ASSERT(size > 0);

    const u32 sizeAligned = RE_GET_SIZE_ALIGNED(size + sizeof(ReMemoryBlockHeader), self->alignment);
    ReDynamicMemoryAllocator* allocator = (ReDynamicMemoryAllocator*)self->internal;

    ReMemoryBlockHeader* header = NULL;
    ReMemoryFreeBlock* current = allocator->freeBlock;
    while (current != NULL) {
        if (current->size == sizeAligned) {
            allocator->freeBlock = current->next;

            reMemoryZero(current, sizeAligned);
            header = (ReMemoryBlockHeader*)current;
            header->size = sizeAligned;

            break;
        }

        if (current->size > sizeAligned) {
            ReMemoryFreeBlock* next = (ReMemoryFreeBlock*)((u8*)current + sizeAligned);
            next->size = current->size - sizeAligned;
            next->next = current->next;
            allocator->freeBlock = next;

            reMemoryZero(current, sizeAligned);
            header = (ReMemoryBlockHeader*)current;
            header->size = sizeAligned;

            break;
        }

        current = current->next;
    }

    if (header != NULL) {
        self->system->stats.taggedAllocations[tag] += header->size;
        self->system->stats.totalMemoryAllocated += header->size;

        LOG_TRACE(NULL, "Memory allocated: Allocator: %p | Tag: %s | Size: %lu | Tag allocated: %lu | Total allocated: %lu",
            allocator,
            reToStringMemoryTag(tag),
            header->size,
            self->system->stats.taggedAllocations[tag],
            self->system->stats.totalMemoryAllocated);

        return (u8*)header + sizeof(ReMemoryBlockHeader);
    }

    return NULL;
}

void* reDynamicMemoryAllocatorReallocate(ReMemoryAllocator* self, void* ptr, u64 size, ReMemoryTag tag) {
    RE_ASSERT(self != NULL);
    RE_ASSERT(ptr != NULL);
    RE_ASSERT(size > 0);

    ReMemoryBlockHeader* header = (ReMemoryBlockHeader*)((u8*)ptr - sizeof(ReMemoryBlockHeader));
    const u32 sizeAligned = RE_GET_SIZE_ALIGNED(size + sizeof(ReMemoryBlockHeader), self->alignment);
    const u32 sizeDiff = sizeAligned - header->size;
    const u32 sizeRequired = header->size + sizeDiff;
    if (header->size >= sizeRequired) {
        return ptr;
    }

    ReDynamicMemoryAllocator* allocator = (ReDynamicMemoryAllocator*)self->internal;
    ReMemoryFreeBlock* current = allocator->freeBlock;
    if ((u8*)header + header->size == (u8*)current) {
        if (current->size == sizeDiff) {
            allocator->freeBlock = current->next;

            header->size += sizeDiff;

            self->system->stats.taggedAllocations[tag] += sizeDiff;
            self->system->stats.totalMemoryAllocated += sizeDiff;

            return (u8*)header + sizeof(ReMemoryBlockHeader);
        }

        if (current->size > sizeDiff) {
            ReMemoryFreeBlock* next = (ReMemoryFreeBlock*)((u8*)current + sizeDiff);
            next->size = current->size - sizeDiff;
            next->next = current->next;
            allocator->freeBlock = next;

            header->size += sizeDiff;

            self->system->stats.taggedAllocations[tag] += sizeDiff;
            self->system->stats.totalMemoryAllocated += sizeDiff;

            return (u8*)header + sizeof(ReMemoryBlockHeader);
        }
    }

    u8* block = reMemoryAllocate(self, header->size + sizeRequired, tag);
    if (block == NULL) {
        return NULL;
    }

    reMemoryCopy(block, ptr, header->size);
    reMemoryFree(self, ptr, tag);

    return block;
}

void reDynamicMemoryAllocatorFree(ReMemoryAllocator* self, void* ptr, ReMemoryTag tag) {
    RE_ASSERT(self != NULL);
    RE_ASSERT(ptr != NULL);

    ReDynamicMemoryAllocator* allocator = (ReDynamicMemoryAllocator*)self->internal;

    ReMemoryFreeBlock* prev = NULL;
    ReMemoryFreeBlock* next = allocator->freeBlock;
    while (next != NULL && (u8*)next < (u8*)ptr) {
        prev = next;
        next = next->next;
    }

    ReMemoryBlockHeader* header = (ReMemoryBlockHeader*)((u8*)ptr - sizeof(ReMemoryBlockHeader));
    const u32 blockSize = header->size;
    reMemoryZero(header, blockSize);

    ptr = NULL;

    if (prev != NULL && (u8*)prev + prev->size == (u8*)header) {
        prev->size += blockSize;

        self->system->stats.taggedAllocations[tag] -= blockSize;
        self->system->stats.totalMemoryAllocated -= blockSize;

        LOG_TRACE(NULL, "Memory free: Allocator: %p | Tag: %s | Size: %lu | Tag allocated: %lu | Total allocated: %lu",
            allocator,
            reToStringMemoryTag(tag),
            blockSize,
            self->system->stats.taggedAllocations[tag],
            self->system->stats.totalMemoryAllocated);

        return;
    }

    if (next != NULL && (u8*)header + blockSize == (u8*)next) {
        ReMemoryFreeBlock* block = (ReMemoryFreeBlock*)header;
        block->size = blockSize + next->size;
        block->next = next->next;

        if (prev != NULL) {
            prev->next = block;
        } else {
            allocator->freeBlock = block;
        }

        self->system->stats.taggedAllocations[tag] -= blockSize;
        self->system->stats.totalMemoryAllocated -= blockSize;

        LOG_TRACE(NULL, "Memory free: Allocator: %p | Tag: %s | Size: %lu | Tag allocated: %lu | Total allocated: %lu",
            allocator,
            reToStringMemoryTag(tag),
            blockSize,
            self->system->stats.taggedAllocations[tag],
            self->system->stats.totalMemoryAllocated);

        return;
    }

    ReMemoryFreeBlock* block = (ReMemoryFreeBlock*)header;
    block->size = blockSize;
    if (next != NULL) {
        block->next = next;
    }

    if (prev != NULL) {
        prev->next = block;
    } else {
        allocator->freeBlock = block;
    }

    self->system->stats.taggedAllocations[tag] -= blockSize;
    self->system->stats.totalMemoryAllocated -= blockSize;

    LOG_TRACE(NULL, "Memory free: Allocator: %p | Tag: %s | Size: %lu | Tag allocated: %lu | Total allocated: %lu",
              allocator,
              reToStringMemoryTag(tag),
              blockSize,
              self->system->stats.taggedAllocations[tag],
              self->system->stats.totalMemoryAllocated);
}
