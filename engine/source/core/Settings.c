#include "RavenEngine/core/Settings.h"

#include <stdlib.h>

#include "RavenEngine/core/Logger.h"
#include "RavenEngine/filesystem/Path.h"
#include "RavenEngine/serialize/JsonArchive.h"


ReSettings* reSettingsCreate(const ReSettingsCreateInfo* createInfo) {
    ReSettings* settings = RE_ALLOCATE(createInfo->allocator, ReSettings, RE_MEMORY_TAG_ENGINE);

    reSettingSetDefaults(settings);

    settings->logger = createInfo->logger;
    settings->allocator = createInfo->allocator;

    return settings;
}

void reSettingsDestroy(ReSettings* settings) {
    RE_FREE(settings->allocator, settings, RE_MEMORY_TAG_ENGINE);
}

void reSettingSetDefaults(ReSettings* settings) {
    settings->width = 1920;
    settings->height = 1080;
}

b8 reSettingsSave(const ReSettings* settings, const RePath* path) {
    ReArchive* archive = reArchiveJsonCreate(settings->allocator);
    if (archive == NULL) {
        return false;
    }

    reArchiveSerializeSettings(archive, settings);
    reArchiveSave(archive, path->path);

    reArchiveJsonDestroy(archive);

    return true;
}

b8 reSettingsLoad(const RePath* path, ReSettings* settings) {
    ReArchive* archive = reArchiveJsonCreate(settings->allocator);
    if (archive == NULL) {
        return false;
    }

    if (!rePathExists(path)) {
        return false;
    }

    reArchiveLoad(path->path, archive);
    reArchiveDeserializeSettings(archive, settings);

    LOG_TRACE(settings->logger, "Settings loaded:")
    LOG_TRACE(settings->logger, "\t width: %d", settings->width)
    LOG_TRACE(settings->logger, "\t height: %d", settings->height)

    reArchiveJsonDestroy(archive);

    return true;
}

void reArchiveSerializeSettings(ReArchive* archive, const ReSettings* settings) {
    RE_SERIALIZE(archive, U32, "width", settings->width);
    RE_SERIALIZE(archive, U32, "height", settings->height);
}

void reArchiveDeserializeSettings(ReArchive* archive, ReSettings* settings) {
    RE_DESERIALIZE(archive, U32, "width", &settings->width);
    RE_DESERIALIZE(archive, U32, "height", &settings->height);
}
