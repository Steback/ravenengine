#include "RavenEngine/input/InputSystem.h"

#include "RavenEngine/core/Assert.h"


b8 reInputSystemKeyEventCallback(u32 id, void* sender, void* listener, const ReEventData* data);

b8 reInputSystemButtonEventCallback(u32 id, void* sender, void* listener, const ReEventData* data);

b8 reInputSystemMouseEventCallback(u32 id, void* sender, void* listener, const ReEventData* data);

b8 reInputSystemWheelEventCallback(u32 id, void* sender, void* listener, const ReEventData* data);

ReInputSystem* reInputSystemCreate(const ReInputSystemCreateInfo* createInfo) {
    RE_ASSERT(createInfo->eventSystem != NULL);

    ReInputSystem* system = RE_ALLOCATE(createInfo->allocator, ReInputSystem, RE_MEMORY_TAG_INPUT);
    system->logger = createInfo->logger;
    system->allocator = createInfo->allocator;
    system->eventSystem = createInfo->eventSystem;

    reEventSystemAddListener(createInfo->eventSystem, RE_EVENT_TYPE_KEY, system, reInputSystemKeyEventCallback);
    reEventSystemAddListener(createInfo->eventSystem, RE_EVENT_TYPE_BUTTON, system, reInputSystemButtonEventCallback);
    reEventSystemAddListener(createInfo->eventSystem, RE_EVENT_TYPE_MOUSE, system, reInputSystemMouseEventCallback);
    reEventSystemAddListener(createInfo->eventSystem, RE_EVENT_TYPE_WHEEL, system, reInputSystemWheelEventCallback);

    return system;
}

void reInputSystemDestroy(ReInputSystem* system) {
    RE_ASSERT(system != NULL);

    RE_FREE(system->allocator, system, RE_MEMORY_TAG_INPUT);
}

b8 reInputSystemKeyEventCallback(u32 id, void* sender, void* listener, const ReEventData* data) {
    const ReInputSystem* system = (ReInputSystem*)listener;
    RE_ASSERT(system != NULL);

    const ReInputEventKey* input = (ReInputEventKey*)data->buffer;
    RE_ASSERT(input != NULL);

    LOG_TRACE(system->logger, "Input key event: %d - %d - %d", input->key, input->mods, input->mode);

    return true;
}

b8 reInputSystemButtonEventCallback(u32 id, void* sender, void* listener, const ReEventData* data) {
    const ReInputSystem* system = (ReInputSystem*)listener;
    RE_ASSERT(system != NULL);

    const ReInputEventButton* input = (ReInputEventButton*)data->buffer;
    RE_ASSERT(input != NULL);

    LOG_TRACE(system->logger, "Input button event: %d - %d - %d", input->button, input->mods, input->mode);

    return true;
}

b8 reInputSystemMouseEventCallback(u32 id, void* sender, void* listener, const ReEventData* data) {
    const ReInputSystem* system = (ReInputSystem*)listener;
    RE_ASSERT(system != NULL);

    const ReInputEventMouse* input = (ReInputEventMouse*)data->buffer;
    RE_ASSERT(input != NULL);

    LOG_TRACE(system->logger, "Input mouse event: (%f, %f) | %d", input->x, input->y, input->mods);

    return true;
}

b8 reInputSystemWheelEventCallback(u32 id, void* sender, void* listener, const ReEventData* data) {
    const ReInputSystem* system = (ReInputSystem*)listener;
    RE_ASSERT(system != NULL);

    const ReInputEventMouseWheel* input = (ReInputEventMouseWheel*)data->buffer;
    RE_ASSERT(input != NULL);

    LOG_TRACE(system->logger, "Input wheel event: %f | %d", input->y, input->mods);

    return true;
}
