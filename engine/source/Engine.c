#include "RavenEngine/Engine.h"

#include <stdlib.h>
#include <RavenEngine/platform/Platform.h>

#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Logger.h"
#include "RavenEngine/core/Memory.h"
#include "RavenEngine/core/Settings.h"
#include "RavenEngine/utils/ToString.h"
#include "RavenEngine/filesystem/Path.h"
#include "RavenEngine/platform/Window.h"
#include "RavenEngine/core/Application.h"
#include "RavenEngine/event/EventSystem.h"
#include "RavenEngine/input/InputSystem.h"
#include "RavenEngine/filesystem/Filesystem.h"
#include "RavenEngine/core/DynamicMemoryAllocator.h"


ReEngine* reSetup(const ReEngineCreateInfo* createInfo) {
    ReMemorySystemCreateInfo memorySystemCreateInfo;
    memorySystemCreateInfo.blockSize = RE_MEMORY_BLOCK_SIZE;
    ReMemorySystem* memorySystem = reMemorySystemCreate(&memorySystemCreateInfo);

    ReMemoryAllocatorCreateInfo memoryAllocatorCreateInfo = {};
    memoryAllocatorCreateInfo.system = memorySystem;
    memoryAllocatorCreateInfo.alignment = RE_DEFAULT_ALIGNMENT;
    ReMemoryAllocator* allocator = reDynamicMemoryAllocatorCreate(&memoryAllocatorCreateInfo);

    ReEngine* engine = RE_ALLOCATE(allocator, ReEngine, RE_MEMORY_TAG_ENGINE);
    engine->isRunning = false;
    engine->version = MAKE_VERSION(VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);
    engine->deltaTime = 0.0;
    engine->memorySystem = memorySystem;
    engine->allocator = allocator;

    ReFilesystemCreateInfo filesystemCreateInfo = {};
    filesystemCreateInfo.allocator = engine->allocator;
    engine->filesystem = reFilesystemCreate(&filesystemCreateInfo);
    if (!engine->filesystem) {
        LOG_FATAL(NULL, "Failed to create filesystem", 0);

        return engine;
    }

    RePath logsPath = rePathCreate(engine->filesystem->paths[RE_FILESYSTEM_PATH_LOGS].path, engine->allocator);
    rePathAppend(&logsPath, "runtime.log");

    ReLoggerCreteInfo loggerCreateInfo = {};
    loggerCreateInfo.callback = NULL;
    loggerCreateInfo.path = &logsPath;
    loggerCreateInfo.allocator = engine->allocator;
    engine->logger = reLoggerCreate(&loggerCreateInfo);

    rePathDestroy(&logsPath);

    LOG_INFO(engine->logger, "%s v%d.%d.%d", ENGINE_NAME, VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);

    engine->platform = rePlatformCreate(engine->allocator);
    if (engine->platform == NULL) {
        LOG_FATAL(engine->logger, "Failed to get platform info!")

       return engine;
    }

    LOG_INFO(engine->logger, "Platform info:");
    LOG_INFO(engine->logger, "\t Name: %s", engine->platform->name);
    LOG_INFO(engine->logger, "\t OS: %s", engine->platform->os);
    LOG_INFO(engine->logger, "\t CPU: %s", engine->platform->cpu);
    LOG_INFO(engine->logger, "\t RAM: %d MB", engine->platform->ram);

    LOG_INFO(engine->logger, "Filesystem registered paths: ");
    for (int i = 0; i < RE_FILESYSTEM_PATH_MAX; ++i) {
        LOG_INFO(engine->logger, "\t %s: %s", reToStringFilesystemPath(i), engine->filesystem->paths[i].path);
    }

    ReSettingsCreateInfo settingsCreateInfo = {};
    settingsCreateInfo.allocator = engine->allocator;
    settingsCreateInfo.logger = engine->logger;
    engine->settings = reSettingsCreate(&settingsCreateInfo);

    RePath settingsPath = rePathCreate(engine->filesystem->paths[RE_FILESYSTEM_PATH_CACHE].path, engine->allocator);
    rePathAppend(&settingsPath, SETTINGS_FILENAME);

    if (!rePathExists(&settingsPath)) {
        reSettingsSave(engine->settings, &settingsPath);
    }

    reSettingsLoad(&settingsPath, engine->settings);
    rePathDestroy(&settingsPath);

    ReEventSystemCreateInfo eventSystemCreateInfo = {};
    eventSystemCreateInfo.logger = engine->logger;
    eventSystemCreateInfo.allocator = engine->allocator;
    engine->eventSystem = reEventSystemCreate(&eventSystemCreateInfo);
    if (engine->eventSystem == NULL) {
        LOG_FATAL(engine->logger, "Failed to create event system");

        return engine;
    }

    if (createInfo->applicationCreateInfo == NULL) {
        LOG_FATAL(engine->logger, "Failed to create application, create info is NULL");

        return engine;
    }

    engine->application = RE_ALLOCATE(engine->allocator, ReApplication, RE_MEMORY_TAG_ENGINE);
    engine->application->name = createInfo->applicationCreateInfo->name;
    engine->application->setup = createInfo->applicationCreateInfo->setup;
    engine->application->shutdown = createInfo->applicationCreateInfo->shutdown;
    engine->application->update = createInfo->applicationCreateInfo->update;
    engine->application->render = createInfo->applicationCreateInfo->render;
    engine->application->allocator = engine->allocator;
    engine->application->logger = engine->logger;

    engine->application->setup(engine->application, engine);

    ReWindowCreateInfo windowCreateInfo = {};
    windowCreateInfo.allocator = engine->allocator;
    windowCreateInfo.logger = engine->logger;
    windowCreateInfo.size = (ReWindowSize){(i32)engine->settings->width, (i32)engine->settings->height};
    windowCreateInfo.title = engine->application->name;
    windowCreateInfo.eventSystem = engine->eventSystem;
    engine->window = reWindowCreate(&windowCreateInfo);
    if (engine->window == NULL) {
        LOG_FATAL(engine->logger, "Failed to create window");

        return engine;
    }

    ReInputSystemCreateInfo inputSystemCreateInfo = {};
    inputSystemCreateInfo.logger = engine->logger;
    inputSystemCreateInfo.allocator = engine->allocator;
    inputSystemCreateInfo.eventSystem = engine->eventSystem;
    engine->inputSystem = reInputSystemCreate(&inputSystemCreateInfo);
    if (engine->inputSystem == NULL) {
        LOG_FATAL(engine->logger, "Failed to create input system");

        return engine;
    }

    engine->isRunning = true;

    return engine;
}

void reShutdown(ReEngine* engine) {
    if (engine->inputSystem != NULL) {
        reInputSystemDestroy(engine->inputSystem);
    }

    if (engine->window != NULL) {
        reWindowDestroy(engine->window);
    }

    if (engine->application != NULL) {
        engine->application->shutdown(engine->application, engine);

        RE_FREE(engine->allocator, engine->application, RE_MEMORY_TAG_ENGINE);
    }

    if (engine->eventSystem != NULL) {
        reEventSystemDestroy(engine->eventSystem);
    }

    if (engine->settings != NULL) {
        reSettingsDestroy(engine->settings);
    }

    if (engine->platform != NULL) {
        rePlatformDestroy(engine->platform);
    }

    if (engine->logger != NULL) {
        reLoggerDestroy(engine->logger);
    }

    if (engine->filesystem != NULL) {
        reFilesystemDestroy(engine->filesystem);
    }

    ReMemorySystem* memorySystem = engine->memorySystem;
    ReMemoryAllocator* allocator = engine->allocator;
    RE_FREE(allocator, engine, RE_MEMORY_TAG_ENGINE);

    reDynamicMemoryAllocatorDestroy(allocator);
    reMemorySystemDestroy(memorySystem);
}

void reRun(ReEngine* engine) {
    f64 lastTime = rePlatformGetCurrentTime();
    while (engine->isRunning) {
        if (!reWindowProcessInput(engine->window)) {
            engine->isRunning = false;

            const ReEventData data = {};
            reEventSystemQueueEvent(engine->eventSystem, RE_EVENT_TYPE_SHUTDOWN, engine, &data);
        }

        reEventSystemUpdate(engine->eventSystem);

        const f64 currentTime = rePlatformGetCurrentTime();
        engine->deltaTime = (currentTime - lastTime);
        lastTime = currentTime;

        reUpdate(engine, (float)engine->deltaTime);
        reRender(engine);
    }

    reShutdown(engine);
}

void reUpdate(ReEngine* engine, float dt) {
    engine->application->update(engine->application, engine, dt);
}

void reRender(ReEngine* engine) {
    engine->application->render(engine->application, engine);
}
