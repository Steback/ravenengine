#include "RavenEngine/serialize/JsonArchive.h"

#include <malloc.h>
#include <stdlib.h>

#include <cjson/cJSON.h>

#include "RavenEngine/core/Logger.h"


typedef struct ReArchiveJson {
    cJSON* root;
    cJSON_Hooks hooks;
} ReArchiveJson;

static ReMemoryAllocator* gAllocator = NULL;

void* reArchiveJsonAllocate(size_t size) {
    return reMemoryAllocate(gAllocator, size, RE_MEMORY_TAG_SERIALIZATION);
}

void reArchiveJsonFree(void* block) {
    reMemoryFree(gAllocator, block, RE_MEMORY_TAG_SERIALIZATION);
}

b8 reArchiveJsonSave(ReArchive* archive, const char* path) {
    const ReArchiveJson* jsonArchive = archive->internal;
    char* content = cJSON_Print(jsonArchive->root);

    // TODO: Create File handle wrappers and use them instead
    FILE* file = fopen(path, "w+");
    if (file == NULL) {
        return false;
    }

    fwrite(content, sizeof(char), strlen(content), file);
    fclose(file);

    jsonArchive->hooks.free_fn(content);

    return true;
}

b8 reArchiveJsonLoad(const char* path, ReArchive* archive) {
    ReArchiveJson* jsonArchive = archive->internal;
    FILE* file = fopen(path, "r");
    if (file == NULL) {
        return false;
    }

    fseek(file, 0L, SEEK_END);

    const u64 size = ftell(file);
    char* data = reMemoryAllocate(archive->allocator, size + 1, RE_MEMORY_TAG_SERIALIZATION);

    fseek(file, 0L, SEEK_SET);
    fread(data, sizeof(char), size, file);
    fclose(file);

    if (jsonArchive->root != NULL) {
        cJSON_Delete(jsonArchive->root);
    }

    jsonArchive->root = cJSON_Parse(data);
    if (jsonArchive->root == NULL) {
        const char* error = cJSON_GetErrorPtr();
        if (error != NULL) {
            LOG_ERROR(NULL, "Failed to parse JSON: %s", error);
        }

        reMemoryFree(archive->allocator, data, RE_MEMORY_TAG_SERIALIZATION);
        return false;
    }

    reMemoryFree(archive->allocator, data, RE_MEMORY_TAG_SERIALIZATION);

    return true;
}

b8 reArchiveJsonSerializeStartField(ReArchive* archive, const char* name, b8 isArray) {
    return true;
}

void reArchiveJsonSerializeEndField(ReArchive* archive) {

}

void reArchiveJsonSerialize(ReArchive* archive, ReSerializeValue* value) {
    ReArchiveJson* jsonArchive = archive->internal;
    ReSerializeField* field = archive->currentField;

    switch (value->type) {
    case RE_SERIALIZE_VALUE_TYPE_UNKNOWN:
        break;
    case RE_SERIALIZE_VALUE_TYPE_BOOL:
        cJSON_AddBoolToObject(jsonArchive->root, field->name, value->bool);
        break;
    case RE_SERIALIZE_VALUE_TYPE_U8:
        cJSON_AddNumberToObject(jsonArchive->root, field->name, value->u8);
        break;
    case RE_SERIALIZE_VALUE_TYPE_U16:
        cJSON_AddNumberToObject(jsonArchive->root, field->name, value->u16);
        break;
    case RE_SERIALIZE_VALUE_TYPE_U32:
        if (cJSON_AddNumberToObject(jsonArchive->root, field->name, value->u32) == NULL) {
            LOG_ERROR(NULL, "Failed to add number to object");
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_U64:
        cJSON_AddNumberToObject(jsonArchive->root, field->name, value->u64);
        break;
    case RE_SERIALIZE_VALUE_TYPE_I8:
        cJSON_AddNumberToObject(jsonArchive->root, field->name, value->i8);
        break;
    case RE_SERIALIZE_VALUE_TYPE_I16:
        cJSON_AddNumberToObject(jsonArchive->root, field->name, value->i16);
        break;
    case RE_SERIALIZE_VALUE_TYPE_I32:
        cJSON_AddNumberToObject(jsonArchive->root, field->name, value->i32);
        break;
    case RE_SERIALIZE_VALUE_TYPE_I64:
        cJSON_AddNumberToObject(jsonArchive->root, field->name, value->i64);
        break;
    case RE_SERIALIZE_VALUE_TYPE_F32:
        cJSON_AddNumberToObject(jsonArchive->root, field->name, value->f32);
        break;
    case RE_SERIALIZE_VALUE_TYPE_F64:
        cJSON_AddNumberToObject(jsonArchive->root, field->name, value->f64);
        break;
    case RE_SERIALIZE_VALUE_TYPE_STRING:
        cJSON_AddStringToObject(jsonArchive->root, field->name, value->string);
        break;
    case RE_SERIALIZE_VALUE_TYPE_MAX:
        break;
    default: ;
    }
}

u32 reArchiveJsonDeserializeStartField(ReArchive* archive, const char* name, b8 isArray) {
    return 0;
}

void reArchiveJsonDeserializeEndField(ReArchive* archive) {

}

void reArchiveJsonDeserialize(ReArchive* archive, ReSerializeValue* value) {
    const ReArchiveJson* jsonArchive = archive->internal;
    const ReSerializeField* field = archive->currentField;

    const cJSON* item = NULL;
    switch (value->type) {
    case RE_SERIALIZE_VALUE_TYPE_UNKNOWN:
        break;
    case RE_SERIALIZE_VALUE_TYPE_BOOL:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsBool(item)) {
            value->bool = cJSON_IsTrue(item);
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_U8:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsNumber(item)) {
            value->u8 = item->valueint;
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_U16:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsNumber(item)) {
            value->u16 = item->valueint;
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_U32:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsNumber(item)) {
            value->u32 = item->valueint;
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_U64:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsNumber(item)) {
            value->u64 = item->valueint;
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_I8:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsNumber(item)) {
            value->i8 = item->valueint;
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_I16:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsNumber(item)) {
            value->i16 = item->valueint;
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_I32:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsNumber(item)) {
            value->i32 = item->valueint;
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_I64:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsNumber(item)) {
            value->i64 = item->valueint;
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_F32:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsNumber(item)) {
            value->f32 = item->valuedouble;
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_F64:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsNumber(item)) {
            value->f64 = item->valuedouble;
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_STRING:
        item = cJSON_GetObjectItemCaseSensitive(jsonArchive->root, field->name);
        if (item != NULL && cJSON_IsString(item)) {
            strcpy(value->string, item->valuestring);
        }
        break;
    case RE_SERIALIZE_VALUE_TYPE_MAX:
        break;
    default: ;
    }
}

ReArchive* reArchiveJsonCreate(ReMemoryAllocator* allocator) {
    gAllocator = allocator;

    ReArchive* archive = RE_ALLOCATE(allocator, ReArchive, RE_MEMORY_TAG_SERIALIZATION);
    archive->type = RE_ARCHIVE_TYPE_JSON;
    archive->allocator = allocator;
    archive->save = reArchiveJsonSave;
    archive->load = reArchiveJsonLoad;
    archive->serializeStartField = reArchiveJsonSerializeStartField;
    archive->serializeEndField = reArchiveJsonSerializeEndField;
    archive->serialize = reArchiveJsonSerialize;
    archive->deserializeStartField = reArchiveJsonDeserializeStartField;
    archive->deserializeEndField = reArchiveJsonDeserializeEndField;
    archive->deserialize = reArchiveJsonDeserialize;
    archive->internal = RE_ALLOCATE(allocator, ReArchiveJson, RE_MEMORY_TAG_SERIALIZATION);

    ReArchiveJson* jsonArchive = archive->internal;
    jsonArchive->hooks.malloc_fn = reArchiveJsonAllocate;
    jsonArchive->hooks.free_fn = reArchiveJsonFree;
    cJSON_InitHooks(&jsonArchive->hooks);

    jsonArchive->root = cJSON_CreateObject();

    return archive;
}

void reArchiveJsonDestroy(ReArchive* archive) {
    ReArchiveJson* jsonArchive = archive->internal;

    cJSON_Delete(jsonArchive->root);
    jsonArchive->root = NULL;

    gAllocator = NULL;

    reMemoryFree(archive->allocator, archive->internal, RE_MEMORY_TAG_SERIALIZATION);
    reMemoryFree(archive->allocator, archive, RE_MEMORY_TAG_SERIALIZATION);
}
