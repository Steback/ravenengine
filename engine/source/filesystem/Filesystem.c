#include "RavenEngine/filesystem/Filesystem.h"

#include <string.h>

#include "RavenEngine/core/Memory.h"
#include "RavenEngine/core/Logger.h"
#include "RavenEngine/platform/Platform.h"


ReFilesystem* reFilesystemCreate(const ReFilesystemCreateInfo* createInfo) {
    ReFilesystem* system = RE_ALLOCATE(createInfo->allocator, ReFilesystem, RE_MEMORY_TAG_FILESYSTEM);
    if (system == NULL) {
        return NULL;
    }

    system->allocator = createInfo->allocator;

    char cwd[RE_PATH_MAX_SIZE];
    reMemoryZero(cwd, RE_PATH_MAX_SIZE);
    rePlatformGetCurrentPath(cwd);

    if (strlen(cwd) == 0) {
        reFilesystemDestroy(system);

        LOG_ERROR(NULL, "Failed to get current working directory path", 0);

        return NULL;
    }

    system->paths[RE_FILESYSTEM_PATH_BINARIES] = rePathCreate(cwd, createInfo->allocator);

    char rootRaw[RE_PATH_MAX_SIZE];
    reMemoryZero(rootRaw, RE_PATH_MAX_SIZE);
    rePathParent(&system->paths[RE_FILESYSTEM_PATH_BINARIES], rootRaw);

    system->paths[RE_FILESYSTEM_PATH_ROOT] = rePathCreate(rootRaw, createInfo->allocator);

    system->paths[RE_FILESYSTEM_PATH_DATA] = rePathCreate(rootRaw, createInfo->allocator);
    rePathAppend(&system->paths[RE_FILESYSTEM_PATH_DATA], RE_DATA_DIR_NAME);

    system->paths[RE_FILESYSTEM_PATH_CACHE] = rePathCreate(rootRaw, createInfo->allocator);
    rePathAppend(&system->paths[RE_FILESYSTEM_PATH_CACHE], RE_CACHE_DIR_NAME);

    if (!rePathExists(&system->paths[RE_FILESYSTEM_PATH_CACHE])) {
        reFilesystemCreateDirectory(&system->paths[RE_FILESYSTEM_PATH_CACHE]);
    }

    system->paths[RE_FILESYSTEM_PATH_LOGS] = rePathCreate(system->paths[RE_FILESYSTEM_PATH_CACHE].path, createInfo->allocator);
    rePathAppend(&system->paths[RE_FILESYSTEM_PATH_LOGS], RE_LOGS_DIR_NAME);

    if (!rePathExists(&system->paths[RE_FILESYSTEM_PATH_LOGS])) {
        reFilesystemCreateDirectory(&system->paths[RE_FILESYSTEM_PATH_LOGS]);
    }

    return system;
}

void reFilesystemDestroy(ReFilesystem* filesystem) {
    for (int i = 0; i < RE_FILESYSTEM_PATH_MAX; ++i) {
        rePathDestroy(&filesystem->paths[i]);
    }

    RE_FREE(filesystem->allocator, filesystem, RE_MEMORY_TAG_FILESYSTEM);
}

RePath reFilesystemSearch(const ReFilesystem* filesystem, const char* path) {
    for (int i = 0; i < RE_FILESYSTEM_PATH_MAX; ++i) {
        char buffer[RE_PATH_MAX_SIZE];
        reMemoryZero(buffer, RE_PATH_MAX_SIZE);

        strcat(buffer, filesystem->paths[i].path);
        strcat(buffer, RE_PATH_SEPARATOR_STR);
        strcat(buffer, path);

        if (rePlatformPathExists(buffer)) {
            const RePath result = rePathCreate(buffer, filesystem->allocator);
            return result;
        }
    }

    const RePath result = rePathCreate("", filesystem->allocator);
    return result;
}

void reFilesystemCreateDirectory(const RePath* path) {
    rePlatformCreateDirectory(path->path);
}
