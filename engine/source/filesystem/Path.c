#include "RavenEngine/filesystem/Path.h"

#include <string.h>

#include "RavenEngine/platform/Platform.h"


RePath rePathCreate(const char* data, ReMemoryAllocator* allocator) {
    RePath path = {};
    path.path = NULL;
    path.allocator = allocator;

    path.length = strlen(data);
    if (path.length == 0) {
        return path;
    }

    path.path = RE_ALLOCATE_SIZE(allocator, path.length + 1, RE_MEMORY_TAG_FILESYSTEM);
    strcpy(path.path, data);

    return path;
}

void rePathDestroy(RePath* path) {
    if (path->path != NULL) {
        RE_FREE(path->allocator, path->path, RE_MEMORY_TAG_FILESYSTEM);
    }

    path->path = NULL;
    path->length = 0;
    path->allocator = NULL;
}

void rePathAppend(RePath* path, const char* data) {
    const u32 newLength = path->length + strlen(data) + 1; // the separator character
    if (newLength == 0) {
        return;
    }

    if (path->path == NULL) {
        path->path = RE_ALLOCATE_SIZE(path->allocator, newLength, RE_MEMORY_TAG_FILESYSTEM);
    } else {
        path->path = RE_REALLOCATE(path->allocator, path->path, newLength, RE_MEMORY_TAG_FILESYSTEM);
    }

    strcat(path->path, RE_PATH_SEPARATOR_STR);
    strcat(path->path, data);

    path->length = newLength;
}

b8 rePathExists(const RePath* path) {
    return rePlatformPathExists(path->path);
}

b8 rePathIsDirectory(const RePath* path) {
    return rePlatformPathIsDirectory(path->path);
}

b8 rePathIsAbsolute(const RePath* path) {
    return rePlatformPathIsAbsolute(path->path);
}

void rePathFilename(const RePath* path, char* outFilename) {
    if (path->path == NULL) {
        return;
    }

    const char* it = strrchr(path->path, RE_PATH_SEPARATOR);
    if (it == NULL) {
        return;
    }


    it += 1;

    strcpy(outFilename, it);
}

void rePathStem(const RePath* path, char* outFilename) {
    if (path->path == NULL) {
        return;
    }

    const char* it = strrchr(path->path, RE_PATH_SEPARATOR);
    if (it == NULL) {
        return;
    }

    it += 1;

    const char* itExt = strrchr(outFilename, '.');
    if (itExt == NULL) {
        strcpy(outFilename, it);

        return;
    }

    const u32 length = itExt - it;
    strncpy(outFilename, it, length);
}

void rePathExtension(const RePath* path, char* outExtension) {
    if (path->path == NULL) {
        return;
    }

    const char* it = strrchr(path->path, '.');
    if (it == NULL) {
        return;
    }

    it += 1;

    strcpy(outExtension, it);
}

void rePathParent(const RePath* path, char* outPath) {
    if (path->path == NULL) {
        return;
    }

    const char* it = strrchr(path->path, RE_PATH_SEPARATOR);
    if (it == NULL) {
        return;
    }

    const u32 length = it - path->path;
    strncpy(outPath, path->path, length);
}
