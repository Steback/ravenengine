#include "RavenEngine/event/Event.h"


void reEventFire(const ReEvent* event, void* sender, const ReEventData* data) {
    for (int i = 0; i < event->listeners.size; ++i) {
        event->listeners.data[i].callback(event->id, sender, event->listeners.data[i].listener, data);
    }

    LOG_TRACE(NULL, "Event fired: %d", event->id);
}

b8 reEventAddListener(ReEvent* event, void* listener, const ReEventCallback callback) {
    const ReEventListener listenerData = {listener, callback};
    if (reArrayEventListenerContains(&event->listeners, &listenerData)) {
        return false;
    }

    reArrayEventListenerPush(&event->listeners, &listenerData);

    return true;
}

void reEventRemoveListener(ReEvent* event, void* listener, ReEventCallback callback) {
    const ReEventListener listenerData = {listener, callback};
    const i32 index = reArrayEventListenerFind(&event->listeners, &listenerData);
    if (index == -1) {
        return;
    }

    reArrayEventListenerRemove(&event->listeners, index);
}
