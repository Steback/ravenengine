#include "RavenEngine/event/EventSystem.h"


ReEventSystem* reEventSystemCreate(const ReEventSystemCreateInfo* createInfo) {
    ReEventSystem* system = RE_ALLOCATE(createInfo->allocator, ReEventSystem, RE_MEMORY_TAG_EVENT);
    system->logger = createInfo->logger;
    system->allocator = createInfo->allocator;
    system->events = reArrayEventCreate(0, NULL, system->allocator);
    system->eventQueue = reQueueEventFireDataCreate(RE_QUEUE_DEFAULT_CAPACITY, system->allocator);

    for (int i = 0; i < RE_EVENT_TYPE_MAX_ENGINE_EVENTS; ++i) {
        const u32 id = reEventSystemRegisterEvent(system);
        RE_ASSERT(id == i);
    }

    return system;
}

void reEventSystemDestroy(ReEventSystem* system) {
    RE_ASSERT(system != NULL);

    reQueueEventFireDataDestroy(&system->eventQueue);

    for (int i = 0; i < system->events.size; i++) {
        ReEvent* event = &system->events.data[i];
        if (event->listeners.size == 0) {
            continue;
        }

        reArrayEventListenerDestroy(&event->listeners);
    }

    if (system->events.size > 0) {
        reArrayEventDestroy(&system->events);
    }

    RE_FREE(system->allocator, system, RE_MEMORY_TAG_EVENT);
}

u32 reEventSystemRegisterEvent(ReEventSystem* system) {
    RE_ASSERT(system != NULL);

    const u32 id = system->events.size;
    ReEvent event = {};
    event.id = (i32)id;
    event.listeners = reArrayEventListenerCreate(0, NULL, system->allocator);

    reArrayEventPush(&system->events, &event);

    LOG_TRACE(system->logger, "Event registered : %d", id)

    return id;
}

b8 reEventSystemAddListener(const ReEventSystem* system, const u32 event, void* listener, const ReEventCallback callback) {
    RE_ASSERT(system != NULL);
    RE_ASSERT(event < system->events.size);

    return reEventAddListener(&system->events.data[event], listener, callback);
}

void reEventSystemRemoveListener(const ReEventSystem* system, const u32 event, void* listener, const ReEventCallback callback) {
    RE_ASSERT(system != NULL);
    RE_ASSERT(event < system->events.size);

    reEventRemoveListener(&system->events.data[event], listener, callback);
}

void reEventSystemFire(const ReEventSystem* system, const u32 event, void* sender, const ReEventData* data) {
    RE_ASSERT(system != NULL);
    RE_ASSERT(event < system->events.size);

    reEventFire(&system->events.data[event], sender, data);
}

void reEventSystemQueueEvent(ReEventSystem* system, const u32 event, void* sender, const ReEventData* data) {
    RE_ASSERT(system != NULL);
    RE_ASSERT(event < system->events.size);

    ReEventFireData fireData = {};
    fireData.event = event;
    fireData.sender = sender;
    reMemoryCopy(&fireData.data, data, sizeof(ReEventData));

    reQueueEventFireDataPush(&system->eventQueue, &fireData);

    if (system->eventQueue.size == system->eventQueue.capacity) {
        LOG_WARN(system->logger, "Event queue is full");
    }
}

void reEventSystemUpdate(ReEventSystem* system) {
    RE_ASSERT(system != NULL);

    while (system->eventQueue.size > 0) {
        ReEventFireData fireData = reQueueEventFireDataPop(&system->eventQueue);
        reEventFire(&system->events.data[fireData.event], fireData.sender, &fireData.data);
    }
}
