/// @file String.h
#ifndef RAVEN_ENGINE_CONTAINERS_STRING_H
#define RAVEN_ENGINE_CONTAINERS_STRING_H


#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Memory.h"


/**
* @brief String container
*/
typedef struct ReString {
    u32 length;
    char* data;
    ReMemoryAllocator* allocator;
} ReString;

/**
 * @brief Create string container
 * @param data C-Style string to fill container, could be null
 * @param length String length to allocate, could be 0
 * @param allocator Memory allocator to be used, could be null
 * @return String container
 */
ReString reStringCreate(const char* data, u32 length, ReMemoryAllocator* allocator);

/**
 * @brief String free and reset
 * @param string Reference to string to free
 */
void reStringDestroy(ReString* string);

/**
 * @brief Clear string data
 * @param string Reference to string to clear
 */
void reStringClear(ReString* string);

/**
 * @brief Append C-Style string
 * @param string Reference to string
 * @param data C-Style string to append
 */
void reStringAppend(ReString* string, const char* data);

/**
 * @brief Compare two strings if are equal
 * @param a
 * @param b
 * @return true if are equal
 */
b8 reStringEqual(const ReString* a, const ReString* b);

/**
 * @brief Search of index that match pattern in ReString
 * @param string Reference to ReString
 * @param data C-Style string with the patter to search
 * @return Index where start the match in strung, -1 if wasn't found
 */
i32 reStringFind(const ReString* string, const char* data);

/**
 * @brief Create copy of ReString
 * @param string Reference to ReString
 * @return Copy of ReString
 */
ReString reStringCopy(const ReString* string);

/**
 * @brief Get substring from string
 * @param string Reference to ReString
 * @param index Start position when start substring
 * @param length String length
 * @return String filled with the substring
 */
ReString reStringSubstring(const ReString* string, i32 index, u32 length);


#endif //RAVEN_ENGINE_CONTAINERS_STRING_H
