/// @file Queue.h
#ifndef RAVEN_ENGINE_CONTAINERS_QUEUE_H
#define RAVEN_ENGINE_CONTAINERS_QUEUE_H


#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Assert.h"
#include "RavenEngine/core/Memory.h"



#define RE_QUEUE_DEFAULT_CAPACITY 16

/**
 * @brief Define queue type name
 * @param type Type to use
 */
#define RE_QUEUE_TYPE(type) ReQueue##type

/**
 * @brief Define queue create for type
 * @param type Type to use
 */
#define RE_QUEUE_DEFINE_CREATE(type) \
RE_API RE_INLINE RE_QUEUE_TYPE(type) reQueue##type##Create(u32 capacity, ReMemoryAllocator* allocator) { \
    RE_QUEUE_TYPE(type) queue = {}; \
    queue.size = 0; \
    queue.capacity = capacity; \
    queue.rear = 0; \
    queue.front = 0; \
    queue.data = RE_ALLOCATE_SIZE(allocator, capacity * sizeof(type), RE_MEMORY_TAG_QUEUE); \
    queue.allocator = allocator; \
    \
    return queue; \
}

/**
 * @brief Define queue destroy for type
 * @param type Type to use
 */
#define RE_QUEUE_DEFINE_DESTROY(type) \
RE_API RE_INLINE void reQueue##type##Destroy(RE_QUEUE_TYPE(type) *queue) { \
    RE_ASSERT(queue != NULL); \
    \
    if (queue->capacity > 0) { \
        RE_FREE_SIZE(queue->allocator, queue->data, RE_MEMORY_TAG_QUEUE); \
    } \
    \
    queue->size = 0; \
    queue->capacity = 0; \
    queue->rear = 0; \
    queue->front = 0; \
    queue->data = NULL; \
    queue->allocator = NULL; \
}

/**
 * @brief Define queue clear for type
 * @param type Type to use
 */
#define RE_QUEUE_DEFINE_CLEAR(type) \
RE_API RE_INLINE void reQueue##type##Clear(RE_QUEUE_TYPE(type) *queue) { \
    RE_ASSERT(queue != NULL); \
    \
    reMemoryZero(queue->data, queue->capacity * sizeof(type)); \
    \
    queue->size = 0; \
    queue->rear = 0; \
    queue->front = 0; \
}

/**
 * @brief Define queue push for type
 * @param type Type to use
 */
#define RE_QUEUE_DEFINE_PUSH(type) \
RE_API RE_INLINE void reQueue##type##Push(RE_QUEUE_TYPE(type) *queue, const type* value) { \
    RE_ASSERT(queue != NULL); \
    RE_ASSERT(value != NULL); \
    \
    if (queue->size == queue->capacity) { \
        return; \
    } \
    \
    reMemoryCopy(&queue->data[queue->rear], value, sizeof(type)); \
    \
    queue->rear = (queue->rear + 1) % queue->capacity; \
    queue->size++; \
}

/**
 * @brief Define queue pop for type
 * @param type Type to use
 */
#define RE_QUEUE_DEFINE_POP(type) \
RE_API RE_INLINE type reQueue##type##Pop(RE_QUEUE_TYPE(type) *queue) { \
    RE_ASSERT(queue != NULL); \
    RE_ASSERT(queue->size > 0); \
    \
    type value = queue->data[0]; \
    \
    for (int i = 1; i < queue->size; ++i) { \
        queue->data[i - 1] = queue->data[i]; \
    } \
    \
    queue->front = (queue->front + 1) % queue->capacity; \
    queue->size--; \
    \
    return value; \
}

/**
 * @brief Define queue of type
 * @param type Type to use
 * @param name Alternative name, used to remove prefix or something, could be the same value as type
 */
#define RE_QUEUE_DEFINE(type, name) \
typedef type name; \
typedef struct RE_QUEUE_TYPE(name) { \
    u32 capacity; \
    u32 size; \
    u32 rear; \
    u32 front; \
    type* data; \
    ReMemoryAllocator* allocator; \
} RE_QUEUE_TYPE(name); \
\
RE_QUEUE_DEFINE_CREATE(name) \
RE_QUEUE_DEFINE_DESTROY(name) \
RE_QUEUE_DEFINE_CLEAR(name) \
RE_QUEUE_DEFINE_PUSH(name) \
RE_QUEUE_DEFINE_POP(name)

RE_QUEUE_DEFINE(b8, b8);
RE_QUEUE_DEFINE(u8, u8);
RE_QUEUE_DEFINE(u16, u16);
RE_QUEUE_DEFINE(u32, u32);
RE_QUEUE_DEFINE(u64, u64);
RE_QUEUE_DEFINE(i8, i8);
RE_QUEUE_DEFINE(i16, i16);
RE_QUEUE_DEFINE(i32, i32);
RE_QUEUE_DEFINE(i64, i64);
RE_QUEUE_DEFINE(f32, f32);
RE_QUEUE_DEFINE(f64, f64);
RE_QUEUE_DEFINE(const char*, cstr);


#endif //RAVEN_ENGINE_CONTAINERS_QUEUE_H
