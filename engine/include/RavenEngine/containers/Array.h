/// @file Array.h
#ifndef RAVEN_ENGINE_CONTAINER_ARRAY_H
#define RAVEN_ENGINE_CONTAINER_ARRAY_H


#include <stdlib.h>
#include <string.h>

#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Assert.h"
#include "RavenEngine/core/Memory.h"


/**
 * @brief Define array type name
 * @param type Type to use
 */
#define RE_ARRAY_TYPE(type) ReArray##type

/**
 * @brief Define create array for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_CREATE(type) \
RE_API RE_INLINE RE_ARRAY_TYPE(type) reArray##type##Create(u32 n, type* data, ReMemoryAllocator* allocator) { \
    RE_ARRAY_TYPE(type) array = {}; \
    array.allocator = allocator; \
    array.size = n; \
    if (n > 0) { \
        array.data = RE_ALLOCATE_SIZE(allocator, n * sizeof(type), RE_MEMORY_TAG_ARRAY); \
    } \
    \
    return array; \
}

/**
 * @brief Define destroy array for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_DESTROY(type) \
RE_API RE_INLINE void reArray##type##Destroy(RE_ARRAY_TYPE(type) *array) { \
    RE_ASSERT(array != NULL) \
    \
    if (array->size > 0) { \
        RE_FREE_SIZE(array->allocator, array->data, RE_MEMORY_TAG_ARRAY); \
    } \
    \
    reMemoryZero(array, sizeof(RE_ARRAY_TYPE(type))); \
}

/**
 * @brief Define copy array for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_COPY(type) \
RE_API RE_INLINE RE_ARRAY_TYPE(type) reArray##type##Copy(const RE_ARRAY_TYPE(type) *array) { \
    RE_ASSERT(array != NULL) \
    \
    RE_ARRAY_TYPE(type) copy = {}; \
    copy.allocator = array->allocator; \
    \
    if (array->size > 0) { \
        copy.data = RE_ALLOCATE_SIZE(array->allocator, array->size * sizeof(type), RE_MEMORY_TAG_ARRAY); \
        reMemoryCopy(copy.data, array->data, array->size * sizeof(type)); \
    } \
    \
    copy.size = array->size; \
    \
    return copy; \
}

/**
 * @brief Define resize array for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_RESIZE(type) \
RE_API RE_INLINE void reArray##type##Resize(RE_ARRAY_TYPE(type) *array, u32 size) { \
    RE_ASSERT(array != NULL) \
    RE_ASSERT(size > 0) \
    \
    if (size == 0 ) { \
        return; \
    } \
    \
    if (array->size > 0) { \
        array->data = RE_REALLOCATE(array->allocator, array->data, size * sizeof(type), RE_MEMORY_TAG_ARRAY); \
    } else { \
        array->data = RE_ALLOCATE_SIZE(array->allocator, size * sizeof(type), RE_MEMORY_TAG_ARRAY); \
    } \
    \
    array->size = size; \
}

/**
 * @brief Define array clear fot type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_CLEAR(type) \
RE_API RE_INLINE void reArray##type##Clear(RE_ARRAY_TYPE(type) *array) { \
    RE_ASSERT(array != NULL) \
    \
    if (array->size > 0) { \
        RE_FREE_SIZE(array->allocator, array->data, RE_MEMORY_TAG_ARRAY); \
    } \
    \
    array->data = NULL; \
    array->size = 0; \
}

/**
 * @brief Define get array element for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_GET(type) \
RE_API RE_INLINE type *reArray##type##Get(const RE_ARRAY_TYPE(type) *array, const u32 index) { \
    RE_ASSERT(array != NULL) \
    RE_ASSERT(index < array->size) \
    \
    return index < array->size ? &array->data[index] : NULL; \
}

/**
 * @brief Define set array element for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_SET(type) \
RE_API RE_INLINE void reArray##type##Set(RE_ARRAY_TYPE(type) *array, const u32 index, const type *value) { \
    RE_ASSERT(array != NULL) \
    RE_ASSERT(index < array->size) \
    RE_ASSERT(value != NULL) \
    \
    if (index < array->size) { \
        reMemoryCopy(&array->data[index], value, sizeof(type)); \
    } \
}

/**
 * @brief Define push array element for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_PUSH(type) \
RE_API RE_INLINE void reArray##type##Push(RE_ARRAY_TYPE(type) *array, const type *value) { \
    RE_ASSERT(array != NULL) \
    RE_ASSERT(value != NULL) \
    \
    const u32 size = array->size + 1; \
    if (array->size > 0) { \
        array->data = RE_REALLOCATE(array->allocator, array->data, size * sizeof(type), RE_MEMORY_TAG_ARRAY); \
    } else { \
        array->data = RE_ALLOCATE_SIZE(array->allocator, size * sizeof(type), RE_MEMORY_TAG_ARRAY); \
    } \
    \
    reMemoryCopy(&array->data[array->size], value, sizeof(type)); \
    \
    array->size = size; \
}

/**
 * @brief Define pop array element for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_POP(type) \
RE_API RE_INLINE void reArray##type##Pop(RE_ARRAY_TYPE(type) *array) { \
    RE_ASSERT(array != NULL) \
    RE_ASSERT(array->size > 0) \
    \
    const u32 size = array->size - 1; \
    if (array->size > 0) { \
        array->data = RE_REALLOCATE(array->allocator, array->data, size * sizeof(type), RE_MEMORY_TAG_ARRAY); \
    } else { \
        array->data = RE_ALLOCATE_SIZE(array->allocator, size * sizeof(type), RE_MEMORY_TAG_ARRAY); \
    } \
    \
    array->size = size; \
}

/**
 * @brief Define insert array element for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_INSERT(type) \
RE_API RE_INLINE void reArray##type##Insert(RE_ARRAY_TYPE(type) *array, const u32 index, const type* value) { \
    RE_ASSERT(array != NULL) \
    RE_ASSERT(index < array->size) \
    \
    if (index >= array->size) { \
        return; \
    } \
    \
    const u32 size = array->size + 1; \
    array->data = RE_REALLOCATE(array->allocator, array->data, size * sizeof(type), RE_MEMORY_TAG_ARRAY); \
    \
    reMemoryCopy(&array->data[index], &array->data[index + 1], (array->size - index) * sizeof(type)); \
    reMemoryCopy(&array->data[index], value, sizeof(type)); \
    \
    array->size = size; \
}

/**
 * @brief Define remove array element for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_REMOVE(type) \
RE_API RE_INLINE void reArray##type##Remove(RE_ARRAY_TYPE(type) *array, const u32 index) { \
    RE_ASSERT(array != NULL) \
    RE_ASSERT(index < array->size) \
    \
    reMemoryCopy(&array->data[index - 1], &array->data[index], (array->size - index) * sizeof(type)); \
    \
    const u32 size = array->size - 1; \
    array->data = RE_REALLOCATE(array->allocator, array->data, size * sizeof(type), RE_MEMORY_TAG_ARRAY); \
    \
    array->size = size; \
}

/**
 * @brief Define find element in array for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_FIND(type) \
RE_API RE_INLINE i32 reArray##type##Find(RE_ARRAY_TYPE(type) *array, const type *value) { \
    RE_ASSERT(array != NULL) \
    RE_ASSERT(value != NULL) \
    \
    if (array->size == 0) { \
        return -1; \
    } \
    \
    for (i32 i = 0; i < array->size; ++i) { \
        if (memcmp(&array->data[i], value, sizeof(type)) == 0) { \
            return i; \
        } \
    } \
    \
    return -1; \
}

/**
 * @brief Define contains element in array for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_CONTAINS(type) \
RE_API RE_INLINE b8 reArray##type##Contains(RE_ARRAY_TYPE(type) *array, const type *value) { \
    RE_ASSERT(array != NULL) \
    RE_ASSERT(value != NULL) \
    \
    if (array->size == 0) { \
        return false; \
    } \
    \
    for (i32 i = 0; i < array->size; ++i) { \
        if (memcmp(&array->data[i], value, sizeof(type)) == 0) { \
            return true; \
        } \
    } \
    \
    return false; \
}

/**
 * @brief Define sort array for type
 * @param type Type to use
 */
#define RE_ARRAY_DEFINE_SORT(type) \
RE_API RE_INLINE void reArray##type##Sort(RE_ARRAY_TYPE(type) *array, int (*compar)(const void *, const void *)) { \
    RE_ASSERT(array != NULL) \
    RE_ASSERT(array->size > 0) \
    \
    qsort(array->data, array->size, sizeof(type), compar); \
}

/**
 * @brief Define array of type
 * @param type Type to use
 * @param name Alternative name, used to remove prefix or something, could be the same value as type
 */
#define RE_ARRAY_DEFINE(type, name) \
typedef type name; \
typedef struct RE_ARRAY_TYPE(name) { \
    u32 size; \
    type* data; \
    ReMemoryAllocator* allocator; \
} RE_ARRAY_TYPE(name); \
\
RE_ARRAY_DEFINE_CREATE(name) \
RE_ARRAY_DEFINE_DESTROY(name) \
RE_ARRAY_DEFINE_COPY(name) \
RE_ARRAY_DEFINE_RESIZE(name) \
RE_ARRAY_DEFINE_CLEAR(name) \
RE_ARRAY_DEFINE_GET(name) \
RE_ARRAY_DEFINE_SET(name) \
RE_ARRAY_DEFINE_PUSH(name) \
RE_ARRAY_DEFINE_POP(name) \
RE_ARRAY_DEFINE_INSERT(name) \
RE_ARRAY_DEFINE_REMOVE(name) \
RE_ARRAY_DEFINE_FIND(name) \
RE_ARRAY_DEFINE_CONTAINS(name) \
RE_ARRAY_DEFINE_SORT(name)

RE_ARRAY_DEFINE(b8, b8);
RE_ARRAY_DEFINE(u8, u8);
RE_ARRAY_DEFINE(u16, u16);
RE_ARRAY_DEFINE(u32, u32);
RE_ARRAY_DEFINE(u64, u64);
RE_ARRAY_DEFINE(i8, i8);
RE_ARRAY_DEFINE(i16, i16);
RE_ARRAY_DEFINE(i32, i32);
RE_ARRAY_DEFINE(i64, i64);
RE_ARRAY_DEFINE(f32, f32);
RE_ARRAY_DEFINE(f64, f64);
RE_ARRAY_DEFINE(const char*, cstr);


#endif //RAVEN_ENGINE_CONTAINER_ARRAY_H
