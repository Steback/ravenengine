/// @file Vector3.h
#ifndef RAVEN_ENGINE_MATH_VECTOR3_H
#define RAVEN_ENGINE_MATH_VECTOR3_H


#include "RavenEngine/math/Math.h"


/**
 * @brief Vector 3-Dimension
 */
typedef struct ReVector3 {
    union {
        struct {
            f32 x;
            f32 y;
            f32 z;
        };
        f32 values[3];
    };
} ReVector3;

/**
 * @brief Add two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector3 reVector3Add(const ReVector3* a, const ReVector3* b) {
    return (ReVector3){ a->x + b->x, a->y + b->y, a->z + b->z };
}

/**
 * @brief Subtract two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector3 reVector3Subtract(const ReVector3* a, const ReVector3* b) {
    return (ReVector3){ a->x - b->x, a->y - b->y, a->z - b->z };
}

/**
 * @brief Scale vector
 * @param a
 * @param s
 */
RE_API RE_INLINE ReVector3 reVector3Scale(const ReVector3* a, const f32 s) {
    return (ReVector3){ a->x * s, a->y * s, a->z * s };
}

/**
 * @brief Calculate vector dot-product
 * @param a
 * @param b
 */
RE_API RE_INLINE f32 reVector3Dot(const ReVector3* a, const ReVector3* b) {
    return a->x * b->x + a->y * b->y + a->z * b->z;
}

/**
 * @brief Calculate vector length
 * @param a
 */
RE_API RE_INLINE f32 reVector3Length(const ReVector3* a) {
    return reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z);
}

/**
 * @brief Calculate vector length square
 * @param a
 */
RE_API RE_INLINE f32 reVector3LengthSq(const ReVector3* a) {
    return a->x * a->x + a->y * a->y + a->z * a->z;
}

/**
 * @brief Normalize vector
 * @param a
 */
RE_API RE_INLINE void reVector3Normalize(ReVector3* a) {
    const f32 inLength = 1.0f / reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z);
    a->x *= inLength;
    a->y *= inLength;
    a->z *= inLength;
}

/**
 * @brief Calculate vector normal
 * @param a
 */
RE_API RE_INLINE ReVector3 reVector3Normalized(const ReVector3* a) {
    const f32 inLength = 1.0f / reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z);
    return  (ReVector3){
        a->x * inLength,
        a->y * inLength,
        a->z * inLength
    };
}

/**
 * @brief Calculate angle between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE f32 reVector3Angle(const ReVector3* a, const ReVector3* b) {
    const f32 aLen = reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z);
    const f32 bLen = reMathSqrtF(b->x * b->x + b->y * b->y + b->z * b->z);
    const f32 dot = a->x * b->x + a->y * b->y + a->z * b->z;
    return reMathAcosF(dot / (aLen * bLen));
}

/**
 * @brief Calculate projection between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector3 reVector3Project(const ReVector3* a, const ReVector3* b) {
    const f32 bLen = reVector3LengthSq(b);
    const f32 scale = reVector3Dot(a, b) / bLen;
    return reVector3Scale(b, scale);
}

/**
 * @brief Calculate rejection between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector3 reVector3Reject(const ReVector3* a, const ReVector3* b) {
    const ReVector3 project = reVector3Project(a, b);
    return reVector3Subtract(a, &project);
}

/**
 * @brief Calculate reflection between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector3 reVector3Reflect(const ReVector3* a, const ReVector3* b) {
    const f32 bLen = reVector3Length(b);
    const f32 scale = reVector3Dot(a, b) / bLen;
    const ReVector3 proj2 = reVector3Scale(b, (scale * 2));
    return reVector3Subtract(a, &proj2);
}

/**
 * @brief Calculate cross-product between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector3 reVector3Cross(const ReVector3* a, const ReVector3* b) {
    return (ReVector3){
            a->y * b->z - a->z * b->y,
            a->z * b->x - a->x * b->z,
            a->x * b->y - a->y * b->x
    };
}

/**
 * @brief Calculate linear interpolation between two vectors
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReVector3 reVector3Lerp(const ReVector3* a, const ReVector3* b, const f32 t) {
    return (ReVector3){
            a->x + (b->x - a->x) * t,
            a->y + (b->y - a->y) * t,
            a->z + (b->z - a->z) * t
    };
}

/**
 * @brief Calculate spherical interpolation between two vectors
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReVector3 reVector3Slerp(const ReVector3* a, const ReVector3* b, const f32 t) {
    const ReVector3 from = reVector3Normalized(a);
    const ReVector3 to = reVector3Normalized(b);
    const f32 theta = reVector3Angle(&from, &to);
    const f32 sinTheta = reMathSinF(theta);
    const f32 ap = reMathSinF(1.0f - t) * theta / sinTheta;
    const f32 bp = reMathSinF(t * theta) / sinTheta;

    const ReVector3 fromAp = reVector3Scale(&from, ap);
    const ReVector3 toBp = reVector3Scale(&to, bp);

    return reVector3Add(&fromAp, &toBp);
}

/**
 * @brief Calculate normal linear interpolation between two vectors
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReVector3 reVector3Nlerp(const ReVector3* a, const ReVector3* b, const f32 t) {
    const ReVector3 result = {
            a->x + (b->x - a->x) * t,
            a->y + (b->y - a->y) * t,
            a->z + (b->z - a->z) * t
    };

    return reVector3Normalized(&result);
}

/**
 * @brief Compare if two vectors are equal
 * @param a
 * @param b
 */
RE_API RE_INLINE b8 reVector3Equal(const ReVector3* a, const ReVector3* b) {
    const ReVector3 diff = reVector3Subtract(a, b);
    return reVector3LengthSq(&diff) < RE_EPSILON_F;
}


typedef ReVector3 ReVec3;


#define RE_VECTOR3_ZERO (ReVector3){0.0f, 0.0f, 0.0f}
#define RE_VECTOR3_ONE (ReVector3){1.0f, 1.0f, 1.0f}
#define RE_VECTOR3_X_AXIS (ReVector3){1.0f, 0.0f, 0.0f}
#define RE_VECTOR3_Y_AXIS (ReVector3){0.0f, 1.0f, 0.0f}
#define RE_VECTOR3_Z_AXIS (ReVector3){0.0f, 0.0f, 1.0f}


#endif //RAVEN_ENGINE_MATH_VECTOR3_H
