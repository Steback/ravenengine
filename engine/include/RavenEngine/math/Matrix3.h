/// @file Matrix3.h
#ifndef RAVEN_ENGINE_MATH_MATRIX3_H
#define RAVEN_ENGINE_MATH_MATRIX3_H


#include "RavenEngine/math/Vector3.h"


/**
 * @brief Matrix 3-Dimension
 */
typedef struct ReMatrix3 {
    union {
        f32 values[9];
        f32 raw[3][3];
        ReVec3 columns[3];
    };
} ReMatrix3;

/**
 * @brief Add two matrices
 * @param a
 * @param b
 */
RE_API RE_INLINE ReMatrix3 reMatrix3Add(const ReMatrix3* a, const ReMatrix3* b) {
    return (ReMatrix3){
            a->raw[0][0] + b->raw[0][0], a->raw[0][1] + b->raw[0][1], a->raw[0][2] + b->raw[0][2],
            a->raw[1][0] + b->raw[1][0], a->raw[1][1] + b->raw[1][1], a->raw[1][2] + b->raw[1][2],
            a->raw[2][0] + b->raw[2][0], a->raw[2][1] + b->raw[2][1], a->raw[2][2] + b->raw[2][2],
    };
}

/**
 * @brief Subtract two matrices
 * @param a
 * @param b
 */
RE_API RE_INLINE ReMatrix3 reMatrix3Subtract(const ReMatrix3* a, const ReMatrix3* b) {
    return (ReMatrix3){
            a->raw[0][0] - b->raw[0][0], a->raw[0][1] - b->raw[0][1], a->raw[0][2] - b->raw[0][2],
            a->raw[1][0] - b->raw[1][0], a->raw[1][1] - b->raw[1][1], a->raw[1][2] - b->raw[1][2],
            a->raw[2][0] - b->raw[2][0], a->raw[2][1] - b->raw[2][1], a->raw[2][2] - b->raw[2][2],
    };
}

/**
 * @brief Scale matrix
 * @param a
 * @param s
 */
RE_API RE_INLINE ReMatrix3 reMatrix3Scale(const ReMatrix3* a, const f32 s) {
    return (ReMatrix3){
            a->raw[0][0] * s, a->raw[0][1] * s, a->raw[0][2] * s,
            a->raw[1][0] * s, a->raw[1][1] * s, a->raw[1][2] * s,
            a->raw[2][0] * s, a->raw[2][1] * s, a->raw[2][2] * s,
    };
}

/**
 * @brief Multiply vector by matrix
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector3 reMatrix3MultiplyV(const ReMatrix3* a, const ReVector3* b) {
    return (ReVector3){
            a->raw[0][0] * b->x + a->raw[1][0] * b->y + a->raw[2][0] * b->z,
            a->raw[0][1] * b->x + a->raw[1][1] * b->y + a->raw[2][1] * b->z,
            a->raw[0][2] * b->x + a->raw[1][2] * b->y + a->raw[2][2] * b->z,
    };
}

/**
 * @brief Multiply two matrices
 * @param a
 * @param b
 */
RE_API RE_INLINE ReMatrix3 reMatrix3MultiplyM(const ReMatrix3* a, const ReMatrix3* b) {
    const f32 _00 = a->raw[0][0] * b->raw[0][0] + a->raw[1][0] * b->raw[0][1] + a->raw[2][0] * b->raw[0][2];
    const f32 _01 = a->raw[0][0] * b->raw[1][0] + a->raw[1][0] * b->raw[1][1] + a->raw[2][0] * b->raw[1][2];
    const f32 _02 = a->raw[0][0] * b->raw[2][0] + a->raw[1][0] * b->raw[2][1] + a->raw[2][0] * b->raw[2][2];

    const f32 _10 = a->raw[0][1] * b->raw[0][0] + a->raw[1][1] * b->raw[0][1] + a->raw[2][1] * b->raw[0][2];
    const f32 _11 = a->raw[0][1] * b->raw[1][0] + a->raw[1][1] * b->raw[1][1] + a->raw[2][1] * b->raw[1][2];
    const f32 _12 = a->raw[0][1] * b->raw[2][0] + a->raw[1][1] * b->raw[2][1] + a->raw[2][1] * b->raw[2][2];

    const f32 _20 = a->raw[0][2] * b->raw[0][0] + a->raw[1][2] * b->raw[0][1] + a->raw[2][2] * b->raw[0][2];
    const f32 _21 = a->raw[0][2] * b->raw[1][0] + a->raw[1][2] * b->raw[1][1] + a->raw[2][2] * b->raw[1][2];
    const f32 _22 = a->raw[0][2] * b->raw[2][0] + a->raw[1][2] * b->raw[2][1] + a->raw[2][2] * b->raw[2][2];

    // Column-major...
    return (ReMatrix3){
            _00, _10, _20,
            _01, _11, _21,
            _02, _12, _22,
    };
}

/**
 * @brief Transpose matrix
 * @param a
 */
RE_API RE_INLINE void reMatrix3Transpose(ReMatrix3* a) {
    reMathSwap(a->raw[0][1], a->raw[1][0])
    reMathSwap(a->raw[0][2], a->raw[2][0])
    reMathSwap(a->raw[1][2], a->raw[2][1])
}

/**
 * @brief Calculate matrix tranpose
 * @param a
 */
RE_API RE_INLINE ReMatrix3 reMatrix3Transposed(const ReMatrix3* a) {
    return (ReMatrix3){
            a->raw[0][0], a->raw[1][0], a->raw[2][0],
            a->raw[0][1], a->raw[1][1], a->raw[2][1],
            a->raw[0][2], a->raw[1][2], a->raw[2][2],
    };
}

/**
 * @brief Calculate matrix determinant
 * @param a
 */
RE_API RE_INLINE f32 reMatrix3Determinant(const ReMatrix3* a) {
    const f32 x = a->raw[0][0] * (a->raw[1][1] * a->raw[2][2] - a->raw[1][2] * a->raw[2][1]);
    const f32 y = a->raw[1][0] * (a->raw[0][1] * a->raw[2][2] - a->raw[0][2] * a->raw[2][1]);
    const f32 z = a->raw[2][0] * (a->raw[0][1] * a->raw[1][2] - a->raw[0][2] * a->raw[1][1]);

    return x - y + z;
}

/**
 * @brief Calculate matrix adjugate
 * @param a
 */
RE_API RE_INLINE ReMatrix3 reMatrix3Adjugate(const ReMatrix3* a) {
    ReMatrix3 result;
    result.raw[0][0] = (a->raw[1][1] * a->raw[2][2] - a->raw[1][2] * a->raw[2][1]);
    result.raw[0][1] = (a->raw[1][0] * a->raw[2][2] - a->raw[1][2] * a->raw[2][0]) * -1.0f;
    result.raw[0][2] = (a->raw[1][0] * a->raw[2][1] - a->raw[1][1] * a->raw[2][0]);

    result.raw[1][0] = (a->raw[0][1] * a->raw[2][2] - a->raw[0][2] * a->raw[2][1]) * -1.0f;
    result.raw[1][1] = (a->raw[0][0] * a->raw[2][2] - a->raw[0][2] * a->raw[2][0]);
    result.raw[1][2] = (a->raw[0][0] * a->raw[2][1] - a->raw[0][1] * a->raw[2][0]) * -1.0f;

    result.raw[2][0] = (a->raw[0][1] * a->raw[1][2] - a->raw[0][2] * a->raw[1][1]);
    result.raw[2][1] = (a->raw[0][0] * a->raw[1][2] - a->raw[0][2] * a->raw[1][0]) * -1.0f;
    result.raw[2][2] = (a->raw[0][0] * a->raw[1][1] - a->raw[0][1] * a->raw[1][0]);

    return reMatrix3Transposed(&result);
}

/**
 * @brief Invert matrix
 * @param a
 */
RE_API RE_INLINE void reMatrix3Invert(ReMatrix3* a) {
    const ReMatrix3 adjugate = reMatrix3Adjugate(a);
    *a = reMatrix3Scale(&adjugate, 1.0f / reMatrix3Determinant(a));
}

/**
 * @brief Calculate matrix inverse
 * @param a
 */
RE_API RE_INLINE ReMatrix3 reMatrix3Inverse(const ReMatrix3* a) {
    const ReMatrix3 adjugate = reMatrix3Adjugate(a);
    return reMatrix3Scale(&adjugate, 1.0f / reMatrix3Determinant(a));
}

/**
 * @brief Compare if two matrices are equal
 * @param a
 * @param b
 */
RE_API RE_INLINE b8 reMatrix3Equal(const ReMatrix3* a, const ReMatrix3* b) {
    return a->values[0] - b->values[0] < RE_EPSILON_F &&
           a->values[1] - b->values[1] < RE_EPSILON_F &&
           a->values[2] - b->values[2] < RE_EPSILON_F &&
           a->values[3] - b->values[3] < RE_EPSILON_F &&
           a->values[4] - b->values[4] < RE_EPSILON_F &&
           a->values[5] - b->values[5] < RE_EPSILON_F &&
           a->values[6] - b->values[6] < RE_EPSILON_F &&
           a->values[7] - b->values[7] < RE_EPSILON_F &&
           a->values[8] - b->values[8] < RE_EPSILON_F;
}


typedef ReMatrix3 ReMat3;


#define RE_MATRIX3_IDENTITY (ReMatrix3){ \
    1.0f, 0.0f, 0.0f,                    \
    0.0f, 1.0f, 0.0f,                    \
    0.0f, 0.0f, 1.0f                     \
}


#endif //RAVEN_ENGINE_MATH_MATRIX3_H
