/// @file Math.h
#ifndef RAVEN_ENGINE_MATH_MATH_H
#define RAVEN_ENGINE_MATH_MATH_H


#include <math.h>
#include <stdlib.h>

#include "RavenEngine/Defines.h"


/**
 * @brief Epsilon constant
 */
#define RE_EPSILON 0.000001

/**
 * @brief Epsilon constant as float
 */
#define RE_EPSILON_F ((float)RE_EPSILON)

/**
 * @brief e constant
 */
#define RE_E 2.71828182845904523536028747135266250

/**
 * @brief e constant as float
 */
#define RE_E_F ((float)RE_E)

/**
 * @brief PI constant
 */
#define RE_PI 3.14159265358979323846264338327950288

/**
 * @brief PI constant as constant
 */
#define RE_PI_F ((float)RE_PI)


/**
 * @brief Absolute value for integer
 * @param n
 */
#define reMathAbs(n) abs(n)

/**
 * @brief Absolute value for float
 * @param n
 */
#define reMathFAbsF(n) fabsf(n)

/**
 * @brief Absolute value for double
 * @param n
 */
#define reMathFAbs(n) fabs(n)

/**
 * @brief Square root for float
 * @param n
 */
#define reMathSqrtF(n) sqrtf(n)

/**
 * @brief Square root for double
 * @param n
 */
#define reMathSqrt(n) sqrt(n)

/**
 * @brief Trigonometric sine function for float
 * @param n
 */
#define reMathSinF(n) sinf(n)

/**
 * @brief Trigonometric sine function for double
 * @param n
 */
#define reMathSin(n) sin(n)

/**
 * @brief Trigonometric cosine function for float
 * @param n
 */
#define reMathCosF(n) cosf(n)

/**
 * @brief Trigonometric cosine function for double
 * @param n
 */
#define reMathCos(n) cos(n)

/**
 * @brief Trigonometric tangent function for float
 * @param n
 */
#define reMathTanF(n) tanf(n)

/**
 * @brief Trigonometric tangent function for double
 * @param n
 */
#define reMathTan(n) tan(n)

/**
 * @brief Trigonometric inverse sine function for float
 * @param n
 */
#define reMathAsinF(n) asinf(n)

/**
 * @brief Trigonometric inverse sine function for double
 * @param n
 */
#define reMathAsin(n) asin(n)

/**
 * @brief Trigonometric inverse cosine function for float
 * @param n
 */
#define reMathAcosF(n) acosf(n)

/**
 * @brief Trigonometric inverse cosine function for double
 * @param n
 */
#define reMathAcos(n) acos(n)

/**
 * @brief Trigonometric inverse tangent function for float
 * @param n
 */
#define reMathAtanF(n) atanf(n)

/**
 * @brief Trigonometric inverse tangent function for double
 * @param n
 */
#define reMathAtan(n) atan(n)

/**
 * @brief Calculate the 2-argument arc tangent for float
 * @param a
 * @param b
 */
#define reMathAtan2F(a, b) atan2f(a, b)

/**
 * @brief Calculate the 2-argument arc tangent for double
 * @param a
 * @param b
 */
#define reMathAtan2(a, b) atan2(a, b)

/**
 * @brief Modulus operation for int
 * @param n
 * @param divisor
 */
#define reMathMod(n, divisor) n % divisor

/**
 * @brief Modulus operation for float
 * @param n
 * @param divisor
 */
#define reMathFModF(n, divisor) fmodf(n, divisor)

/**
 * @brief Modulus operation for double
 * @param n
 * @param divisor
 */
#define reMathFMod(n, divisor) fmod(n, divisor)

/**
 * @brief Convert radians to degrees for float
 * @param radians
 */
#define reMathDegreesF(radians) radians * 57.295779513082320876798154814105f // radians * (180 / PI)

/**
 * @brief Convert radians to degrees for double
 * @param radians
 */
#define reMathDegrees(radians) radians * 57.295779513082320876798154814105 // radians * (180 / PI)

/**
 * @brief Convert degrees to radians for float
 * @param degrees
 */
#define reMathRadiansF(degrees) degrees * 0.0174532925199432957692369076848f // degrees * (PI / 180)

/**
 * @brief Convert degrees to radians for double
 * @param degrees
 */
#define reMathRadians(degrees) degrees * 0.0174532925199432957692369076848 // degrees * (PI / 180)

/**
 * @brief Get min value
 * @param min
 * @param value
 */
#define reMathMin(min, value) value <= min ? min : value

/**
 * @brief Get max value
 * @param max
 * @param value
 */
#define reMathMax(max, value) value >= max ? max : value

/**
 * @brief Get clamp value
 * @param min
 * @param max
 * @param value
 */
#define reMathClamp(min, max, value) value <= min ? min : (value >= max ? max : value)

/**
 * @brief Swap values
 * @param x
 * @param y
 */
#define reMathSwap(x, y) { typeof(x) temp = x; x = y; y = temp; }


#endif //RAVEN_ENGINE_MATH_MATH_H
