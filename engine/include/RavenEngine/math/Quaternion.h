/// @file Quaternion.h
#ifndef RAVEN_ENGINE_MATH_QUATERNION_H
#define RAVEN_ENGINE_MATH_QUATERNION_H


#include "RavenEngine/math/Vector3.h"
#include "RavenEngine/math/Matrix4.h"


/**
 * @brief Quaternion
 */
typedef struct ReQuaternion {
    union {
        struct {
            f32 x;
            f32 y;
            f32 z;
            f32 w;
        };
        struct {
            ReVec3 vector;
            f32 scalar;
        };
        f32 values[4];
    };
} ReQuaternion;

/**
 * @brief Add two quaternions
 * @param a
 * @param b
 */
RE_API RE_INLINE ReQuaternion reQuaternionAdd(const ReQuaternion* a, const ReQuaternion* b) {
    return (ReQuaternion){ a->x + b->x, a->y + b->y, a->z + b->z, a->w + b->w };
}

/**
 * @brief Subtract two quaternions
 * @param a
 * @param b
 */
RE_API RE_INLINE ReQuaternion reQuaternionSubtract(const ReQuaternion* a, const ReQuaternion* b) {
    return (ReQuaternion){ a->x - b->x, a->y - b->y, a->z - b->z, a->w - b->w };
}

/**
 * @brief Subtract two quaternions
 * @param a
 * @param s
 */
RE_API RE_INLINE ReQuaternion reQuaternionScale(const ReQuaternion* a, const f32 s) {
    return (ReQuaternion){a->x * s, a->y * s, a->z * s, a->w * s};
}

/**
 * @brief Multiply vector by quaternion
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVec3 reQuaternionMultiplyV(const ReQuaternion* a, const ReVec3* b) {
    const ReVec3 av2 = reVector3Scale(&a->vector, 2.0f);
    const ReVec3 x = reVector3Scale(&av2, reVector3Dot(&a->vector, b));
    const ReVec3 y = reVector3Scale(b, a->scalar * a->scalar - reVector3Dot(&a->vector, &a->vector));
    const ReVec3 abCross = reVector3Cross(&a->vector, b);
    const ReVec3 z = reVector3Scale(&abCross, 2.0f * a->scalar);
    const ReVec3 xAddy = reVector3Add(&x, &y);

    return reVector3Add(&xAddy, &z);
}

/**
 * @brief Multiply two quaternions
 * @param a
 * @param b
 */
RE_API RE_INLINE ReQuaternion reQuaternionMultiplyQ(const ReQuaternion* a, const ReQuaternion* b) {
    return (ReQuaternion){
            a->x * b->w + a->y * b->z - a->z * b->y + a->w * b->x,
            -a->x * b->z + a->y * b->w + a->z * b->x + a->w * b->y,
            a->x * b->y - a->y * b->x + a->z * b->w + a->w * b->z,
            -a->x * b->x - a->y * b->y - a->z * b->z + a->w * b->w
    };
}

/**
 * @brief Compare if two quaternions are equal
 * @param a
 * @param b
 */
RE_API RE_INLINE b8 reQuaternionEqual(const ReQuaternion* a, const ReQuaternion* b) {
    return reMathFAbsF(a->x - b->x) <= RE_EPSILON_F &&
           reMathFAbsF(a->y - b->y) <= RE_EPSILON_F &&
           reMathFAbsF(a->z - b->z) <= RE_EPSILON_F &&
           reMathFAbsF(a->w - b->w) <= RE_EPSILON_F;
}

/**
 * @brief Calculate power of quaternion
 * @param a
 * @param s
 */
RE_API RE_INLINE ReQuaternion reQuaternionPow(const ReQuaternion* a, const f32 s) {
    const f32 angle = 2.0f * reMathAcosF(a->scalar);
    const ReVec3 axis = reVector3Normalized(&a->vector);

    const f32 halfCos = reMathCosF((s * angle) * 0.5f);
    const f32 halfSin = reMathSinF((s * angle) * 0.5f);

    return (ReQuaternion){
            axis.x * halfSin,
            axis.y * halfSin,
            axis.z * halfSin,
            halfCos
    };
}

/**
 * @brief Calculate quaternion from angle and axis
 * @param angle
 * @param axis
 */
RE_API RE_INLINE ReQuaternion reQuaternionAngleAxis(const f32 angle, const ReVec3* axis) {
    const ReVec3 norm = reVector3Normalized(axis);
    const f32 s = reMathSinF(angle / 2.0f);

    return (ReQuaternion){
            norm.x * s,
            norm.y * s,
            norm.z * s,
            reMathCosF(angle / 2.0f)
    };
}

/**
 * @brief Calculate quaternion from two vectors
 * @param from
 * @param to
 */
RE_API RE_INLINE ReQuaternion reQuaternionFromTo(const ReVec3* from, const ReVec3* to) {
    const ReVec3 f = reVector3Normalized(from);
    const ReVec3 t = reVector3Normalized(to);

    if (reVector3Equal(&f, &t)) {
        return (ReQuaternion){};
    }

    const ReVec3 tNegate = reVector3Scale(&t, -1);
    if (reVector3Equal(&f, &tNegate)) {
        ReVec3 ortho = {1.0f, 0.0f, 0.0f};
        if (reMathFAbsF(f.y) < reMathFAbsF(f.x)) {
            ortho = (ReVec3){0, 1, 0};
        }

        if (reMathFAbsF(f.z) < reMathFAbsF(f.y) && reMathFAbsF(f.z) < reMathFAbsF(f.x)) {
            ortho = (ReVec3){0, 0, 1};
        }

        const ReVec3 fOrthoCross = reVector3Cross(&f, &ortho);
        const ReVec3 axis = reVector3Normalized(&fOrthoCross);

        return (ReQuaternion){ axis.x, axis.y, axis.z, 0 };
    }

    const ReVec3 fAddt = reVector3Add(&f, &t);
    const ReVec3 half = reVector3Normalized(&fAddt);
    const ReVec3 axis = reVector3Cross(&f, &half);

    return (ReQuaternion){ axis.x, axis.y, axis.z, reVector3Dot(&f, &half) };
}

/**
 * @brief Calculate rotation axis from quaternion
 * @param a
 */
RE_API RE_INLINE ReVec3 reQuaternionAxis(const ReQuaternion* a) {
    return reVector3Normalized(&a->vector);
}

/**
 * @brief Calculate rotation axis from quaternion
 * @param a
 */
RE_API RE_INLINE f32 reQuaternionAngle(const ReQuaternion* a) {
    return 2.0f * reMathAcosF(a->w);
}

/**
 * @brief Calculate dot-product between quaternions
 * @param a
 * @param b
 */
RE_API RE_INLINE f32 reQuaternionDot(const ReQuaternion* a, const ReQuaternion* b) {
    return a->x * b->x + a->y * b->y + a->z * b->z + a->w * b->w;
}

/**
 * @brief Calculate quaternion length
 * @param a
 */
RE_API RE_INLINE f32 reQuaternionLength(const ReQuaternion* a) {
    return reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w);
}

/**
 * @brief Calculate quaternion length square
 * @param a
 */
RE_API RE_INLINE f32 reQuaternionLengthSq(const ReQuaternion* a) {
    return a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w;
}

/**
 * @brief Normalize quaternion
 * @param a
 */
RE_API RE_INLINE void reQuaternionNormalize(ReQuaternion* a) {
    const f32 len = 1.0f / reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w);
    a->x *= len;
    a->y *= len;
    a->z *= len;
    a->w *= len;
}

/**
 * @brief Calculate quaternion normal
 * @param a
 */
RE_API RE_INLINE ReQuaternion reQuaternionNormalized(const ReQuaternion* a) {
    const f32 len = 1.0f / reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w);
    return (ReQuaternion){
            a->x * len,
            a->y * len,
            a->z * len,
            a->w * len
    };
}

/**
 * @brief Calculate quaternion conjugate
 * @param a
 */
RE_API RE_INLINE ReQuaternion reQuaternionConjugate(const ReQuaternion* a) {
    return (ReQuaternion){
            -a->x,
            -a->y,
            -a->z,
            a->w
    };
}

/**
 * @brief Invert quaternion
 * @param a
 */
RE_API RE_INLINE void reQuaternionInvert(ReQuaternion* a) {
    const f32 lenSq = a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w;
    a->x = -a->x / lenSq;
    a->y = -a->y / lenSq;
    a->z = -a->z / lenSq;
    a->w = a->w / lenSq;
}

/**
 * @brief Calculate quaternion inverse
 * @param a
 */
RE_API RE_INLINE ReQuaternion reQuaternionInverse(const ReQuaternion* a) {
    const f32 lenSq = a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w;
    return (ReQuaternion){
            -a->x / lenSq,
            -a->y / lenSq,
            -a->z / lenSq,
            a->w / lenSq
    };
}

/**
 * @brief Calculate mix between two quaternions
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReQuaternion reQuaternionMix(const ReQuaternion* a, const ReQuaternion* b, const f32 t) {
    const ReQuaternion aScaleT = reQuaternionScale(a, 1.0f - t);
    const ReQuaternion toT = reQuaternionScale(b, t);
    return reQuaternionAdd(&aScaleT, &toT);
}

/**
 * @brief Calculate linear interpolation between two quaternions
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReQuaternion reQuaternionNLerp(const ReQuaternion* a, const ReQuaternion* b, const f32 t) {
    const ReQuaternion direction = reQuaternionSubtract(b, a);
    const ReQuaternion fromAddDirection = reQuaternionAdd(a, &direction);
    const ReQuaternion result = reQuaternionScale(&fromAddDirection, t);
    return reQuaternionNormalized(&result);
}

/**
 * @brief Calculate spherical interpolation between two quaternions
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReQuaternion reQuaternionSLerp(const ReQuaternion* a, const ReQuaternion* b, const f32 t) {
    const ReQuaternion inverse = reQuaternionInverse(a);
    const ReQuaternion inverseTo = reQuaternionMultiplyQ(&inverse, b);
    const ReQuaternion pow = reQuaternionPow(&inverseTo, t);
    const ReQuaternion powB = reQuaternionMultiplyQ(&pow, a);
    return reQuaternionNormalized(&powB);
}

/**
 * @brief Convert quaterion to matrix
 * @param a
 */
RE_API RE_INLINE ReMatrix4 reQuaternionQuatToMat(const ReQuaternion* a) {
    const ReVec3 xAxis = {1.0f, 0.0f, 0.0f};
    const ReVec3 yAxis = {0.0f, 1.0f, 0.0f};
    const ReVec3 zAxis = {0.0f, 0.0f, 1.0f};
    const ReVec3 r = reQuaternionMultiplyV(a, &xAxis);
    const ReVec3 u = reQuaternionMultiplyV(a, &yAxis);
    const ReVec3 f = reQuaternionMultiplyV(a, &zAxis);

    return (ReMatrix4){
            r.x, r.y, r.z, 0,
            u.x, u.y, u.z, 0,
            f.x, f.y, f.z, 0,
            0, 0, 0, 1
    };
}

/**
 * @brief Convert matrix to quaternion
 * @param a
 */
RE_API RE_INLINE ReQuaternion reQuaternionMatToQuat(const ReMatrix4* a) {
    const f32 fourXSquaredMinus1 = a->raw[0][0] - a->raw[1][1] - a->raw[2][2];
    const f32 fourYSquaredMinus1 = a->raw[1][1] - a->raw[0][0] - a->raw[2][2];
    const f32 fourZSquaredMinus1 = a->raw[2][2] - a->raw[0][0] - a->raw[1][1];
    const f32 fourWSquaredMinus1 = a->raw[0][0] + a->raw[1][1] + a->raw[2][2];

    int biggestIndex = 0;
    f32 fourBiggestSquaredMinus1 = fourWSquaredMinus1;
    if (fourXSquaredMinus1 > fourBiggestSquaredMinus1) {
        fourBiggestSquaredMinus1 = fourXSquaredMinus1;
        biggestIndex = 1;
    }

    if (fourYSquaredMinus1 > fourBiggestSquaredMinus1) {
        fourBiggestSquaredMinus1 = fourYSquaredMinus1;
        biggestIndex = 2;
    }

    if (fourZSquaredMinus1 > fourBiggestSquaredMinus1) {
        fourBiggestSquaredMinus1 = fourZSquaredMinus1;
        biggestIndex = 3;
    }

    const f32 biggestVal = reMathSqrtF(fourBiggestSquaredMinus1 + 1.0f) / 2.0f;
    const f32 mult = 0.25f / biggestVal;

    switch(biggestIndex) {
        case 0:
            return (ReQuaternion){(a->raw[1][2] - a->raw[2][1]) * mult, (a->raw[2][0] - a->raw[0][2]) * mult, (a->raw[0][1] - a->raw[1][0]) * mult, biggestVal};
        case 1:
            return (ReQuaternion){biggestVal, (a->raw[0][1] + a->raw[1][0]) * mult, (a->raw[2][0] + a->raw[0][2]) * mult, (a->raw[1][2] - a->raw[2][1]) * mult};
        case 2:
            return (ReQuaternion){(a->raw[0][1] + a->raw[1][0]) * mult, biggestVal, (a->raw[1][2] + a->raw[2][1]) * mult, (a->raw[2][0] - a->raw[0][2]) * mult};
        case 3:
            return (ReQuaternion){(a->raw[2][0] + a->raw[0][2]) * mult, (a->raw[1][2] + a->raw[2][1]) * mult, biggestVal, (a->raw[0][1] - a->raw[1][0]) * mult};
        default: // Silence a -Wswitch-default warning in GCC. Should never actually get here. Assert is just for sanity.
            return (ReQuaternion){0.0f, 0.0f, 0.0f, 1.0f};
    }
}


typedef ReQuaternion ReQuat;


#define RE_QUATERNION_IDENTITY (ReQuaternion){0.0f, 0.0f, 0.0f, 1.0f}


#endif //RAVEN_ENGINE_MATH_QUATERNION_H
