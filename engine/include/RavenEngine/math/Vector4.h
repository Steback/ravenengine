/// @file Vector4.h
#ifndef RAVEN_ENGINE_MATH_VECTOR4_H
#define RAVEN_ENGINE_MATH_VECTOR4_H


#include "RavenEngine/math/Math.h"


/**
 * @brief Vector 4-Dimension
 */
typedef struct ReVector4 {
    union {
        struct {
            f32 x;
            f32 y;
            f32 z;
            f32 w;
        };
        f32 values[4];
    };
} ReVector4;

/**
 * @brief Add two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector4 reVector4Add(const ReVector4* a, const ReVector4* b) {
    return (ReVector4){ a->x + b->x, a->y + b->y, a->z + b->z, a->w + b->w };
}

/**
 * @brief Subtract two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector4 reVector4Subtract(const ReVector4* a, const ReVector4* b) {
    return (ReVector4){ a->x - b->x, a->y - b->y, a->z - b->z, a->w - b->w };
}

/**
 * @brief Scale vector
 * @param a
 * @param s
 */
RE_API RE_INLINE ReVector4 reVector4Scale(const ReVector4* a, const f32 s) {
    return (ReVector4){ a->x * s, a->y * s, a->z * s, a->w * s };
}

/**
 * @brief Calculate dot-product between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE f32 reVector4Dot(const ReVector4* a, const ReVector4* b) {
    return a->x * b->x + a->y * b->y + a->z * b->z + a->w * b->w;
}

/**
 * @brief Calculate vector length
 * @param a
 */
RE_API RE_INLINE f32 reVector4Length(const ReVector4* a) {
    return reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w);
}

/**
 * @brief Calculate vector length square
 * @param a
 */
RE_API RE_INLINE f32 reVector4LengthSq(const ReVector4* a) {
    return a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w;
}

/**
 * @brief Normalize vector
 * @param a
 */
RE_API RE_INLINE void reVector4Normalize(ReVector4* a) {
    const f32 lenSq = 1.0f / reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w);
    a->x *= lenSq;
    a->y *= lenSq;
    a->z *= lenSq;
    a->w *= lenSq;
}

/**
 * @brief Calculate vector normal
 * @param a
 */
RE_API RE_INLINE ReVector4 reVector4Normalized(const ReVector4* a) {
    const f32 lenSq = 1.0f / reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w);
    return (ReVector4){
            a->x * lenSq,
            a->y * lenSq,
            a->z * lenSq,
            a->w * lenSq
    };
}

/**
 * @brief Calculate angle between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE f32 reVector4Angle(const ReVector4* a, const ReVector4* b) {
    const f32 aLen = reMathSqrtF(a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w);
    const f32 bLen = reMathSqrtF(b->x * b->x + b->y * b->y + b->z * b->z + b->w * b->w);
    const f32 dot = a->x * b->x + a->y * b->y + a->z * b->z + a->w * b->w;
    return reMathAcosF(dot / (aLen * bLen));
}

/**
 * @brief Calculate projection between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector4 reVector4Project(const ReVector4* a, const ReVector4* b) {
    const f32 bLen = reVector4LengthSq(b);
    const f32 scale = reVector4Dot(a, b) / bLen;
    return reVector4Scale(b, scale);
}

/**
 * @brief Calculate rejection between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector4 reVector4Reject(const ReVector4* a, const ReVector4* b) {
    const ReVector4 project = reVector4Project(a, b);
    return reVector4Subtract(a, &project);
}

/**
 * @brief Calculate reflection between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector4 reVector4Reflect(const ReVector4* a, const ReVector4* b) {
    const f32 bLen = reVector4Length(b);
    const f32 scale = reVector4Dot(a, b) / bLen;
    const ReVector4 proj2 = reVector4Scale(b, (scale * 2));
    return reVector4Subtract(a, &proj2);
}

/**
 * @brief Calculate linear interpolation between two vectors
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReVector4 reVector4Lerp(const ReVector4* a, const ReVector4* b, const f32 t) {
    return (ReVector4){
            a->x + (b->x - a->x) * t,
            a->y + (b->y - a->y) * t,
            a->z + (b->z - a->z) * t,
            a->w + (b->w - a->w) * t
    };
}

/**
 * @brief Calculate spherical interpolation between two vectors
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReVector4 reVector4Slerp(const ReVector4* a, const ReVector4* b, const f32 t) {
    const ReVector4 from = reVector4Normalized(a);
    const ReVector4 to = reVector4Normalized(b);
    const f32 theta = reVector4Angle(&from, &to);
    const f32 sinTheta = reMathSinF(theta);
    const f32 ap = reMathSinF(1.0f - t) * theta / sinTheta;
    const f32 bp = reMathSinF(t * theta) / sinTheta;

    const ReVector4 fromAp = reVector4Scale(&from, ap);
    const ReVector4 toBp = reVector4Scale(&to, bp);

    return reVector4Subtract(&fromAp, &toBp);
}

/**
 * @brief Calculate normal linear interpolation between two vectors
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReVector4 reVector4Nlerp(const ReVector4* a, const ReVector4* b, f32 t) {
    const ReVector4 result = {
            a->x + (b->x - a->x) * t,
            a->y + (b->y - a->y) * t,
            a->z + (b->z - a->z) * t,
            a->w + (b->w - a->w) * t
    };

    return reVector4Normalized(&result);
}

/**
 * @brief Compare if two vector are equal
 * @param a
 * @param b
 */
RE_API RE_INLINE b8 reVector4Equal(const ReVector4* a, const ReVector4* b) {
    const ReVector4 diff = reVector4Subtract(a, b);
    return reVector4LengthSq(&diff) < RE_EPSILON_F;
}


typedef ReVector4 ReVec4;


#define RE_VECTOR4_ZERO (ReVector4){0.0f, 0.0f, 0.0f, 0.0f}
#define RE_VECTOR4_ONE (ReVector4){1.0f, 1.0f, 1.0f, 1.0f}
#define RE_VECTOR4_X_AXIS (ReVector4){1.0f, 0.0f, 0.0f, 0.0f}
#define RE_VECTOR4_Y_AXIS (ReVector4){0.0f, 1.0f, 0.0f, 0.0f}
#define RE_VECTOR4_Z_AXIS (ReVector4){0.0f, 0.0f, 1.0f, 0.0f}
#define RE_VECTOR4_W_AXIS (ReVector4){0.0f, 0.0f, 0.0f, 1.0f}


#endif //RAVEN_ENGINE_MATH_VECTOR4_H
