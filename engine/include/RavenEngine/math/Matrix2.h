/// @file Matrix2.h
#ifndef RAVEN_ENGINE_MATH_MATRIX2_H
#define RAVEN_ENGINE_MATH_MATRIX2_H


#include "RavenEngine/math/Vector2.h"


/**
 * @brief Matrix 2-Dimension
 */
typedef struct ReMatrix2 {
    union {
        f32 values[4];
        f32 raw[2][2];
        ReVec2 columns[2];
    };
} ReMatrix2;

/**
 * @brief Add two matrices
 * @param a
 * @param b
 */
RE_API RE_INLINE ReMatrix2 reMatrix2Add(const ReMatrix2* a, const ReMatrix2* b) {
    ReMatrix2 result;
    result.raw[0][0] = a->raw[0][0] + b->raw[0][0];
    result.raw[0][1] = a->raw[0][1] + b->raw[0][1];
    result.raw[1][0] = a->raw[1][0] + b->raw[1][0];
    result.raw[1][1] = a->raw[1][1] + b->raw[1][1];

    return result;
}

/**
 * @brief Subtract two matrices
 * @param a
 * @param b
 */
RE_API RE_INLINE ReMatrix2 reMatrix2Subtract(const ReMatrix2* a, const ReMatrix2* b) {
    ReMatrix2 result;
    result.raw[0][0] = a->raw[0][0] - b->raw[0][0];
    result.raw[0][1] = a->raw[0][1] - b->raw[0][1];
    result.raw[1][0] = a->raw[1][0] - b->raw[1][0];
    result.raw[1][1] = a->raw[1][1] - b->raw[1][1];

    return result;
}

/**
 * @brief Scale matrix
 * @param a
 * @param s
 */
RE_API RE_INLINE ReMatrix2 reMatrix2Scale(const ReMatrix2* a, const f32 s) {
    ReMatrix2 result;
    result.raw[0][0] = a->raw[0][0] * s;
    result.raw[0][1] = a->raw[0][1] * s;
    result.raw[1][0] = a->raw[1][0] * s;
    result.raw[1][1] = a->raw[1][1] * s;

    return result;
}

/**
 * @brief Multiply vector to matrix
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector2 reMatrix2MultiplyV(const ReMatrix2* a, const ReVector2* b) {
    ReVector2 result;
    result.x = a->raw[0][0] * b->x + a->raw[1][0] * b->y;
    result.y = a->raw[0][1] * b->x + a->raw[1][1] * b->y;

    return result;
}

/**
 * @brief Multiply two matrices
 * @param a
 * @param b
 */
RE_API RE_INLINE ReMatrix2 reMatrix2MultiplyM(const ReMatrix2* a, const ReMatrix2* b) {
    ReMatrix2 result;
    result.raw[0][0] = a->raw[0][0] * b->raw[0][0] + a->raw[1][0] * b->raw[0][1];
    result.raw[0][1] = a->raw[0][1] * b->raw[0][0] + a->raw[1][1] * b->raw[0][1];
    result.raw[1][0] = a->raw[0][0] * b->raw[1][0] + a->raw[1][0] * b->raw[1][1];
    result.raw[1][1] = a->raw[0][1] * b->raw[1][0] + a->raw[1][1] * b->raw[1][1];

    return result;
}

/**
 * @brief Transpose matrix
 * @param a
 */
RE_API RE_INLINE void reMatrix2Transpose(ReMatrix2* a) {
    const f32 temp = a->raw[0][1];
    a->raw[0][1] = a->raw[1][0];
    a->raw[1][0] = temp;
}

/**
 * @brief Calculate matrix transpose
 * @param a
 */
RE_API RE_INLINE ReMatrix2 reMatrix2Transposed(const ReMatrix2* a) {
    ReMatrix2 result;
    result.raw[0][0] = a->raw[0][0];
    result.raw[0][1] = a->raw[1][0];
    result.raw[1][0] = a->raw[0][1];
    result.raw[1][1] = a->raw[1][1];

    return result;
}

/**
 * @brief Calculate matrix determinant
 * @param a
 */
RE_API RE_INLINE f32 reMatrix2Determinant(const ReMatrix2* a) {
    return a->raw[0][0] * a->raw[1][1] - a->raw[1][0] * a->raw[0][1];
}

/**
 * @brief Calculate matrix adjugate
 * @param a
 */
RE_API RE_INLINE ReMatrix2 reMatrix2Adjugate(const ReMatrix2* a) {
    ReMatrix2 result;
    result.raw[0][0] = a->raw[1][1];
    result.raw[0][1] = -a->raw[0][1];
    result.raw[1][0] = -a->raw[1][0];
    result.raw[1][1] = a->raw[0][0];

    return reMatrix2Transposed(&result);
}

/**
 * @brief Invert matrix
 * @param a
 */
RE_API RE_INLINE void reMatrix2Invert(ReMatrix2* a) {
    const f32 determinant = reMatrix2Determinant(a);
    const ReMatrix2 adjugate = reMatrix2Adjugate(a);
    *a = reMatrix2Scale(&adjugate, 1.0f / determinant);
}

/**
 * @brief Calculate matrix inverse
 * @param a
 */
RE_API RE_INLINE ReMatrix2 reMatrix2Inverse(const ReMatrix2* a) {
    const f32 determinant = reMatrix2Determinant(a);
    const ReMatrix2 adjugate = reMatrix2Adjugate(a);
    return reMatrix2Scale(&adjugate, 1.0f / determinant);
}

/**
 * @brief Compare if two matrices are equal
 * @param a
 * @param b
 */
RE_API RE_INLINE b8 reMatrix2Equal(const ReMatrix2* a, const ReMatrix2* b) {
    return a->values[0] - b->values[0] < RE_EPSILON_F &&
           a->values[1] - b->values[1] < RE_EPSILON_F &&
           a->values[2] - b->values[2] < RE_EPSILON_F &&
           a->values[3] - b->values[3] < RE_EPSILON_F;
}


typedef ReMatrix2 ReMat2;


#define RE_MATRIX2_IDENTITY (ReMatrix2){1.0f, 0.0f, 0.0f, 1.0f}


#endif //RAVEN_ENGINE_MATH_MATRIX2_H
