/// @file Vector2.h
#ifndef RAVEN_ENGINE_MATH_VECTOR2_H
#define RAVEN_ENGINE_MATH_VECTOR2_H


#include "RavenEngine/math/Math.h"


/**
 * @brief Vector 2-Dimension
 */
typedef struct ReVector2 {
    union {
        struct {
            f32 x;
            f32 y;
        };
        f32 values[2];
    };
} ReVector2;

/**
 * @brief Add two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector2 reVector2Add(const ReVector2* a, const ReVector2* b) {
    ReVector2 result;
    result.x = a->x + b->x;
    result.y = a->y + b->y;

    return result;
}

/**
 * @brief Subtract two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector2 reVector2Subtract(const ReVector2* a, const ReVector2* b) {
    ReVector2 result;
    result.x = a->x - b->x;
    result.y = a->y - b->y;

    return result;
}

/**
 * @brief Scale vector
 * @param a
 * @param s
 */
RE_API RE_INLINE ReVector2 reVector2Scale(const ReVector2* a, const f32 s) {
    ReVector2 result;
    result.x = a->x * s;
    result.y = a->y * s;

    return result;
}

/**
 * @brief Calculate dot-product of two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE f32 reVector2Dot(const ReVector2* a, const ReVector2* b) {
    return a->x * b->x + a->y * b->y;
}

/**
 * @brief Calculate vector length
 * @param a
 */
RE_API RE_INLINE f32 reVector2Length(const ReVector2* a) {
    return reMathSqrtF(a->x * a->x + a->y * a->y);
}

/**
 * @brief Calculate vector length squared
 * @param a
 */
RE_API RE_INLINE f32 reVector2LengthSq(const ReVector2* a) {
    return a->x * a->x + a->y * a->y;
}

/**
 * @brief Normalize vector
 * @param a
 */
RE_API RE_INLINE void reVector2Normalize(ReVector2* a) {
    const f32 inverse = 1.0f / (reMathSqrtF(a->x * a->x + a->y * a->y));
    a->x *= inverse;
    a->y *= inverse;
}

/**
 * @brief Calculate vector normal
 * @param a
 */
RE_API RE_INLINE ReVector2 reVector2Normalized(const ReVector2* a) {
    const f32 inverse = 1.0f / (reMathSqrtF(a->x * a->x + a->y * a->y));

    ReVector2 result;
    result.x = a->x * inverse;
    result.y = a->y * inverse;

    return result;
}

/**
 * @brief Calculate angle between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE f32 reVector2Angle(const ReVector2* a, const ReVector2* b) {
    const f32 aLen = reMathSqrtF(a->x * a->x + a->y * a->y);
    const f32 bLen = reMathSqrtF(b->x * b->x + b->y * b->y);
    const f32 dot = a->x * b->x + a->y * b->y;
    return reMathAcosF(dot / (aLen * bLen));
}

/**
 * @brief Calculate projection vector between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector2 reVector2Project(const ReVector2* a, const ReVector2* b) {
    const f32 bLen = reVector2LengthSq(b);
    const f32 scale = reVector2Dot(a, b) / bLen;
    return reVector2Scale(b, scale);
}

/**
 * @brief Calculate rejection vector between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector2 reVector2Reject(const ReVector2* a, const ReVector2* b) {
    const ReVector2 project = reVector2Project(a, b);
    return reVector2Subtract(a, &project);
}

/**
 * @brief Calculate refection vector between two vectors
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector2 reVector2Reflect(const ReVector2* a, const ReVector2* b) {
    const f32 bLen = reVector2Length(b);
    const f32 scale = reVector2Dot(a, b) / bLen;
    const ReVector2 proj2 = reVector2Scale(b, scale * 2);
    return reVector2Subtract(a, &proj2);
}

/**
 * @brief Calculate linear interpolation between two vectors
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReVector2 reVector2Lerp(const ReVector2* a, const ReVector2* b, const f32 t) {
    ReVector2 result;
    result.x = a->x + (b->x - a->x) * t;
    result.y = a->y + (b->y - a->y) * t;

    return result;
}

/**
 * @brief Calculate spherical interpolation between two vectors
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReVector2 reVector2Slerp(const ReVector2* a, const ReVector2* b, const f32 t) {
    const ReVector2 from = reVector2Normalized(a);
    const ReVector2 to = reVector2Normalized(b);
    const f32 theta = reVector2Angle(&from, &to);
    const f32 sinTheta = reMathSinF(theta);
    const f32 ap = reMathSinF(1.0f - t) * theta / sinTheta;
    const f32 bp = reMathSinF(t * theta) / sinTheta;

    const ReVector2 fromAp = reVector2Scale(&from, ap);
    const ReVector2 toBp = reVector2Scale(&to, bp);

    return reVector2Add(&fromAp, &toBp);
}

/**
 * @brief Calculate normalize linear interpolation between two vectors
 * @param a
 * @param b
 * @param t
 */
RE_API RE_INLINE ReVector2 reVector2Nlerp(const ReVector2* a, const ReVector2* b, const f32 t) {
    ReVector2 result;
    result.x = a->x + (b->x - a->x) * t;
    result.y = a->y + (b->y - a->y) * t;

    return reVector2Normalized(&result);
}

/**
 * @brief Compare if two vectors are equal
 * @param a
 * @param b
 */
RE_API RE_INLINE b8 reVector2Equal(const ReVector2* a, const ReVector2* b) {
    const ReVector2 diff = reVector2Subtract(a, b);
    return reVector2LengthSq(&diff) < RE_EPSILON_F;
}


typedef ReVector2 ReVec2;


#define RE_VECTOR2_ZERO (ReVector2){0.0f, 0.0f}
#define RE_VECTOR2_ONE (ReVector2){1.0f, 1.0f}
#define RE_VECTOR2_X_AXIS (ReVector2){1.0f, 0.0f}
#define RE_VECTOR2_Y_AXIS (ReVector2){0.0f, 1.0f}


#endif //RAVEN_ENGINE_MATH_VECTOR2_H
