/// @file Matrix4.h
#ifndef RAVEN_ENGINE_MATH_MATRIX4_H
#define RAVEN_ENGINE_MATH_MATRIX4_H


#include "RavenEngine/math/Vector4.h"


/**
 * @brief Matrix 4-Dimension
 */
typedef struct ReMatrix4 {
    union {
        f32 values[16];
        f32 raw[4][4];
        ReVec4 columns[4];
        struct {
            ReVec4 right;
            ReVec4 up;
            ReVec4 forward;
            ReVec4 position;
        };
    };
} ReMatrix4;

/**
 * @brief Add two matrices
 * @param a
 * @param b
 */
RE_API RE_INLINE ReMatrix4 reMatrix4Add(const ReMatrix4* a, const ReMatrix4* b) {
    return (ReMatrix4){
            a->raw[0][0] + b->raw[0][0], a->raw[0][1] + b->raw[0][1], a->raw[0][2] + b->raw[0][2], a->raw[0][3] + b->raw[0][3],
            a->raw[1][0] + b->raw[1][0], a->raw[1][1] + b->raw[1][1], a->raw[1][2] + b->raw[1][2], a->raw[1][3] + b->raw[1][3],
            a->raw[2][0] + b->raw[2][0], a->raw[2][1] + b->raw[2][1], a->raw[2][2] + b->raw[2][2], a->raw[2][3] + b->raw[2][3],
            a->raw[3][0] + b->raw[3][0], a->raw[3][1] + b->raw[3][1], a->raw[3][2] + b->raw[3][2], a->raw[3][3] + b->raw[3][3]
    };
}

/**
 * @brief Subtract two matrices
 * @param a
 * @param b
 */
RE_API RE_INLINE ReMatrix4 reMatrix4Subtract(const ReMatrix4* a, const ReMatrix4* b) {
    return (ReMatrix4){
            a->raw[0][0] - b->raw[0][0], a->raw[0][1] - b->raw[0][1], a->raw[0][2] - b->raw[0][2], a->raw[0][3] - b->raw[0][3],
            a->raw[1][0] - b->raw[1][0], a->raw[1][1] - b->raw[1][1], a->raw[1][2] - b->raw[1][2], a->raw[1][3] - b->raw[1][3],
            a->raw[2][0] - b->raw[2][0], a->raw[2][1] - b->raw[2][1], a->raw[2][2] - b->raw[2][2], a->raw[2][3] - b->raw[2][3],
            a->raw[3][0] - b->raw[3][0], a->raw[3][1] - b->raw[3][1], a->raw[3][2] - b->raw[3][2], a->raw[3][3] - b->raw[3][3]
    };
}

/**
 * @brief Scale matrix
 * @param a
 * @param s
 */
RE_API RE_INLINE ReMatrix4 reMatrix4Scale(const ReMatrix4* a, const f32 s) {
    return (ReMatrix4){
            a->raw[0][0] * s, a->raw[0][1] * s, a->raw[0][2] * s, a->raw[0][3] * s,
            a->raw[1][0] * s, a->raw[1][1] * s, a->raw[1][2] * s, a->raw[1][3] * s,
            a->raw[2][0] * s, a->raw[2][1] * s, a->raw[2][2] * s, a->raw[2][3] * s,
            a->raw[3][0] * s, a->raw[3][1] * s, a->raw[3][2] * s, a->raw[3][3] * s
    };
}

/**
 * @brief Multiply vector by matrix
 * @param a
 * @param b
 */
RE_API RE_INLINE ReVector4 reMatrix4MultiplyV(const ReMatrix4* a, const ReVector4* b) {
    return (ReVector4){
            a->raw[0][0] * b->x + a->raw[1][0] * b->y + a->raw[2][0] * b->z + a->raw[3][0] * b->w,
            a->raw[0][1] * b->x + a->raw[1][1] * b->y + a->raw[2][1] * b->z + a->raw[3][1] * b->w,
            a->raw[0][2] * b->x + a->raw[1][2] * b->y + a->raw[2][2] * b->z + a->raw[3][2] * b->w,
            a->raw[0][3] * b->x + a->raw[1][3] * b->y + a->raw[2][3] * b->z + a->raw[3][3] * b->w
    };
}

/**
 * @brief Multiply two matrices
 * @param a
 * @param b
 */
RE_API RE_INLINE ReMatrix4 reMatrix4MultiplyM(const ReMatrix4* a, const ReMatrix4* b) {
    const f32 _00 = a->raw[0][0] * b->raw[0][0] + a->raw[1][0] * b->raw[0][1] + a->raw[2][0] * b->raw[0][2] + a->raw[3][0] * b->raw[0][3];
    const f32 _01 = a->raw[0][0] * b->raw[1][0] + a->raw[1][0] * b->raw[1][1] + a->raw[2][0] * b->raw[1][2] + a->raw[3][0] * b->raw[1][3];
    const f32 _02 = a->raw[0][0] * b->raw[2][0] + a->raw[1][0] * b->raw[2][1] + a->raw[2][0] * b->raw[2][2] + a->raw[3][0] * b->raw[2][3];
    const f32 _03 = a->raw[0][0] * b->raw[3][0] + a->raw[1][0] * b->raw[3][1] + a->raw[2][0] * b->raw[3][2] + a->raw[3][0] * b->raw[3][3];

    const f32 _10 = a->raw[0][1] * b->raw[0][0] + a->raw[1][1] * b->raw[0][1] + a->raw[2][1] * b->raw[0][2] + a->raw[3][1] * b->raw[0][3];
    const f32 _11 = a->raw[0][1] * b->raw[1][0] + a->raw[1][1] * b->raw[1][1] + a->raw[2][1] * b->raw[1][2] + a->raw[3][1] * b->raw[1][3];
    const f32 _12 = a->raw[0][1] * b->raw[2][0] + a->raw[1][1] * b->raw[2][1] + a->raw[2][1] * b->raw[2][2] + a->raw[3][1] * b->raw[2][3];
    const f32 _13 = a->raw[0][1] * b->raw[3][0] + a->raw[1][1] * b->raw[3][1] + a->raw[2][1] * b->raw[3][2] + a->raw[3][1] * b->raw[3][3];

    const f32 _20 = a->raw[0][2] * b->raw[0][0] + a->raw[1][2] * b->raw[0][1] + a->raw[2][2] * b->raw[0][2] + a->raw[3][2] * b->raw[0][3];
    const f32 _21 = a->raw[0][2] * b->raw[1][0] + a->raw[1][2] * b->raw[1][1] + a->raw[2][2] * b->raw[1][2] + a->raw[3][2] * b->raw[1][3];
    const f32 _22 = a->raw[0][2] * b->raw[2][0] + a->raw[1][2] * b->raw[2][1] + a->raw[2][2] * b->raw[2][2] + a->raw[3][2] * b->raw[2][3];
    const f32 _23 = a->raw[0][2] * b->raw[3][0] + a->raw[1][2] * b->raw[3][1] + a->raw[2][2] * b->raw[3][2] + a->raw[3][2] * b->raw[3][3];

    const f32 _30 = a->raw[0][3] * b->raw[0][0] + a->raw[1][3] * b->raw[0][1] + a->raw[2][3] * b->raw[0][2] + a->raw[3][3] * b->raw[0][3];
    const f32 _31 = a->raw[0][3] * b->raw[1][0] + a->raw[1][3] * b->raw[1][1] + a->raw[2][3] * b->raw[1][2] + a->raw[3][3] * b->raw[1][3];
    const f32 _32 = a->raw[0][3] * b->raw[2][0] + a->raw[1][3] * b->raw[2][1] + a->raw[2][3] * b->raw[2][2] + a->raw[3][3] * b->raw[2][3];
    const f32 _33 = a->raw[0][3] * b->raw[3][0] + a->raw[1][3] * b->raw[3][1] + a->raw[2][3] * b->raw[3][2] + a->raw[3][3] * b->raw[3][3];

    // Column-major...
    return (ReMatrix4){
            _00, _10, _20, _30,
            _01, _11, _21, _31,
            _02, _12, _22, _32,
            _03, _13, _23, _33
    };
}

/**
 * @brief Transpose matrix
 * @param a
 */
RE_API RE_INLINE void reMatrix4Transpose(ReMatrix4* a) {
    reMathSwap(a->raw[0][1], a->raw[1][0]);
    reMathSwap(a->raw[0][2], a->raw[2][0]);
    reMathSwap(a->raw[0][3], a->raw[3][0]);
    reMathSwap(a->raw[1][2], a->raw[2][1]);
    reMathSwap(a->raw[1][3], a->raw[3][1]);
    reMathSwap(a->raw[2][3], a->raw[3][2]);
}

/**
 * @brief Calculate matrix transpose
 * @param a
 */
RE_API RE_INLINE ReMatrix4 reMatrix4Transposed(const ReMatrix4* a) {
    return (ReMatrix4){
            a->raw[0][0], a->raw[1][0], a->raw[2][0], a->raw[3][0],
            a->raw[0][1], a->raw[1][1], a->raw[2][1], a->raw[3][1],
            a->raw[0][2], a->raw[1][2], a->raw[2][2], a->raw[3][2],
            a->raw[0][3], a->raw[1][3], a->raw[2][3], a->raw[3][3],
    };
}

/**
 * @brief Calculate matrix determinant
 * @param a
 */
RE_API RE_INLINE f32 reMatrix4Determinant(const ReMatrix4* a) {
    const f32 x = a->raw[0][0] * ((a->raw[1][1] * (a->raw[2][2] * a->raw[3][3] - a->raw[2][3] * a->raw[3][2])) -
                               (a->raw[2][1] * (a->raw[1][2] * a->raw[3][3] - a->raw[1][3] * a->raw[3][2])) +
                               (a->raw[3][1] * (a->raw[1][2] * a->raw[2][3] - a->raw[1][3] * a->raw[2][2])));

    const f32 y = a->raw[1][0] * ((a->raw[0][1] * (a->raw[2][2] * a->raw[3][3] - a->raw[2][3] * a->raw[3][2])) -
                               (a->raw[2][1] * (a->raw[0][2] * a->raw[3][3] - a->raw[0][3] * a->raw[3][2])) +
                               (a->raw[3][1] * (a->raw[0][2] * a->raw[2][3] - a->raw[0][3] * a->raw[2][2])));

    const f32 z = a->raw[2][0] * ((a->raw[0][1] * (a->raw[1][2] * a->raw[3][3] - a->raw[1][3] * a->raw[3][2])) -
                               (a->raw[1][1] * (a->raw[0][2] * a->raw[3][3] - a->raw[0][3] * a->raw[3][2])) +
                               (a->raw[3][1] * (a->raw[0][2] * a->raw[1][3] - a->raw[0][3] * a->raw[1][2])));

    const f32 w = a->raw[3][0] * ((a->raw[0][1] * (a->raw[1][2] * a->raw[2][3] - a->raw[1][3] * a->raw[2][2])) -
                               (a->raw[1][1] * (a->raw[0][2] * a->raw[2][3] - a->raw[0][3] * a->raw[2][2])) +
                               (a->raw[2][1] * (a->raw[0][2] * a->raw[1][3] - a->raw[0][3] * a->raw[1][2])));

    return x - y + z - w;
}

/**
 * @brief Calculate matrix adjugate
 * @param a
 */
RE_API RE_INLINE ReMatrix4 reMatrix4Adjugate(const ReMatrix4* a) {
    ReMatrix4 x;
    x.raw[0][0] = ((a->raw[1][1] * (a->raw[2][2] * a->raw[3][3] - a->raw[2][3] * a->raw[3][2])) -
                   (a->raw[2][1] * (a->raw[1][2] * a->raw[3][3] - a->raw[1][3] * a->raw[3][2])) +
                   (a->raw[3][1] * (a->raw[1][2] * a->raw[2][3] - a->raw[1][3] * a->raw[2][2])));

    x.raw[0][1] = ((a->raw[1][0] * (a->raw[2][2] * a->raw[3][3] - a->raw[2][3] * a->raw[3][2])) -
                   (a->raw[2][0] * (a->raw[1][2] * a->raw[3][3] - a->raw[1][3] * a->raw[3][2])) +
                   (a->raw[3][0] * (a->raw[1][2] * a->raw[2][3] - a->raw[1][3] * a->raw[2][2]))) * -1;

    x.raw[0][2] = ((a->raw[1][0] * (a->raw[2][1] * a->raw[3][3] - a->raw[2][3] * a->raw[3][1])) -
                   (a->raw[2][0] * (a->raw[1][1] * a->raw[3][3] - a->raw[1][3] * a->raw[3][1])) +
                   (a->raw[3][0] * (a->raw[1][1] * a->raw[2][3] - a->raw[1][3] * a->raw[2][1])));

    x.raw[0][3] = ((a->raw[1][0] * (a->raw[2][1] * a->raw[3][2] - a->raw[2][2] * a->raw[3][1])) -
                   (a->raw[2][0] * (a->raw[1][1] * a->raw[3][2] - a->raw[1][2] * a->raw[3][1])) +
                   (a->raw[3][0] * (a->raw[1][1] * a->raw[2][2] - a->raw[1][2] * a->raw[2][1]))) * -1;

    x.raw[1][0] = ((a->raw[0][1] * (a->raw[2][2] * a->raw[3][3] - a->raw[2][3] * a->raw[3][2])) -
                   (a->raw[2][1] * (a->raw[0][2] * a->raw[3][3] - a->raw[0][3] * a->raw[3][2])) +
                   (a->raw[3][1] * (a->raw[0][2] * a->raw[2][3] - a->raw[0][3] * a->raw[2][2]))) * -1;

    x.raw[1][1] = ((a->raw[0][0] * (a->raw[2][2] * a->raw[3][3] - a->raw[2][3] * a->raw[3][2])) -
                   (a->raw[2][0] * (a->raw[0][2] * a->raw[3][3] - a->raw[0][3] * a->raw[3][2])) +
                   (a->raw[3][0] * (a->raw[0][2] * a->raw[2][3] - a->raw[0][3] * a->raw[2][2])));

    x.raw[1][2] = ((a->raw[0][0] * (a->raw[2][1] * a->raw[3][3] - a->raw[2][3] * a->raw[3][1])) -
                   (a->raw[2][0] * (a->raw[0][1] * a->raw[3][3] - a->raw[0][3] * a->raw[3][1])) +
                   (a->raw[3][0] * (a->raw[0][1] * a->raw[2][3] - a->raw[0][3] * a->raw[2][1]))) * -1;

    x.raw[1][3] = ((a->raw[0][0] * (a->raw[2][1] * a->raw[3][2] - a->raw[2][2] * a->raw[3][1])) -
                   (a->raw[2][0] * (a->raw[0][1] * a->raw[3][2] - a->raw[0][2] * a->raw[3][1])) +
                   (a->raw[3][0] * (a->raw[0][1] * a->raw[2][2] - a->raw[0][2] * a->raw[2][1])));

    x.raw[2][0] = ((a->raw[0][1] * (a->raw[1][2] * a->raw[3][3] - a->raw[1][3] * a->raw[3][2])) -
                   (a->raw[1][1] * (a->raw[0][2] * a->raw[3][3] - a->raw[0][3] * a->raw[3][2])) +
                   (a->raw[3][1] * (a->raw[0][2] * a->raw[1][3] - a->raw[0][3] * a->raw[1][2])));

    x.raw[2][1] = ((a->raw[0][0] * (a->raw[1][2] * a->raw[3][3] - a->raw[1][3] * a->raw[3][2])) -
                   (a->raw[1][0] * (a->raw[0][2] * a->raw[3][3] - a->raw[0][3] * a->raw[3][2])) +
                   (a->raw[3][3] * (a->raw[0][2] * a->raw[1][3] - a->raw[0][3] * a->raw[1][2]))) * -1;

    x.raw[2][2] = ((a->raw[0][0] * (a->raw[1][1] * a->raw[3][3] - a->raw[1][3] * a->raw[3][1])) -
                   (a->raw[1][0] * (a->raw[0][1] * a->raw[3][3] - a->raw[0][3] * a->raw[3][1])) +
                   (a->raw[3][0] * (a->raw[0][1] * a->raw[1][3] - a->raw[0][3] * a->raw[1][1])));

    x.raw[2][3] = ((a->raw[0][0] * (a->raw[1][1] * a->raw[3][2] - a->raw[1][2] * a->raw[3][1])) -
                   (a->raw[1][0] * (a->raw[0][1] * a->raw[3][2] - a->raw[0][2] * a->raw[3][1])) +
                   (a->raw[3][0] * (a->raw[0][1] * a->raw[1][2] - a->raw[0][2] * a->raw[1][1]))) * -1;

    x.raw[3][0] = ((a->raw[0][1] * (a->raw[1][2] * a->raw[2][3] - a->raw[1][3] * a->raw[2][2])) -
                   (a->raw[1][1] * (a->raw[0][2] * a->raw[2][3] - a->raw[0][3] * a->raw[2][2])) +
                   (a->raw[2][1] * (a->raw[0][2] * a->raw[1][3] - a->raw[0][3] * a->raw[1][2]))) * -1;

    x.raw[3][1] = ((a->raw[0][0] * (a->raw[1][2] * a->raw[2][3] - a->raw[1][3] * a->raw[2][2])) -
                   (a->raw[1][0] * (a->raw[0][2] * a->raw[2][3] - a->raw[0][3] * a->raw[2][2])) +
                   (a->raw[2][0] * (a->raw[0][2] * a->raw[1][3] - a->raw[0][3] * a->raw[1][2])));

    x.raw[3][2] = ((a->raw[0][0] * (a->raw[1][1] * a->raw[2][3] - a->raw[1][3] * a->raw[2][1])) -
                   (a->raw[1][0] * (a->raw[0][1] * a->raw[2][3] - a->raw[0][3] * a->raw[2][1])) +
                   (a->raw[2][0] * (a->raw[0][1] * a->raw[1][3] - a->raw[0][3] * a->raw[1][1]))) * -1;

    x.raw[3][3] = ((a->raw[0][0] * (a->raw[1][1] * a->raw[2][2] - a->raw[1][2] * a->raw[2][1])) -
                   (a->raw[1][0] * (a->raw[0][1] * a->raw[2][2] - a->raw[0][2] * a->raw[2][1])) +
                   (a->raw[2][0] * (a->raw[0][1] * a->raw[1][2] - a->raw[0][2] * a->raw[1][1])));

    return reMatrix4Transposed(&x);
}

/**
 * @brief Inverse matrix
 * @param a
 */
RE_API RE_INLINE void reMatrix4Invert(ReMatrix4* a) {
    const ReMatrix4 adjugate = reMatrix4Adjugate(a);
    *a = reMatrix4Scale(&adjugate, 1.0f / reMatrix4Determinant(a));
}

/**
 * @brief Calculate matrix inverse
 * @param a
 */
RE_API RE_INLINE ReMatrix4 reMatrix4Inverse(const ReMatrix4* a) {
    const ReMatrix4 adjugate = reMatrix4Adjugate(a);
    return reMatrix4Scale(&adjugate, 1.0f / reMatrix4Determinant(a));
}

/**
 * @brief Compare if two matrix are equal
 * @param a
 * @param b
 */
RE_API RE_INLINE b8 reMatrix4Equal(const ReMatrix4* a, const ReMatrix4* b) {
    return reMathFAbsF(a->values[0] - b->values[0]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[1] - b->values[1]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[2] - b->values[2]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[3] - b->values[3]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[4] - b->values[4]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[5] - b->values[5]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[6] - b->values[6]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[7] - b->values[7]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[8] - b->values[8]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[9] - b->values[9]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[10] - b->values[10]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[11] - b->values[11]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[12] - b->values[12]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[13] - b->values[13]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[14] - b->values[14]) < RE_EPSILON_F &&
           reMathFAbsF(a->values[15] - b->values[15]) < RE_EPSILON_F;
}


#define RE_MATRIX4_IDENTITY (ReMatrix4){ \
    1.0f, 0.0f, 0.0f, 0.0f,              \
    0.0f, 1.0f, 0.0f, 0.0f,              \
    0.0f, 0.0f, 1.0f, 0.0f,              \
    0.0f, 0.0f, 0.0f, 1.0f               \
}


typedef ReMatrix4 ReMat4;


#endif //RAVEN_ENGINE_MATH_MATRIX4_H
