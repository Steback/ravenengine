/// @file Archive.h
#ifndef RAVEN_ENGINE_SERIALIZE_ARCHIVE_H
#define RAVEN_ENGINE_SERIALIZE_ARCHIVE_H


#include <string.h>

#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Memory.h"


typedef u32 U32;


/**
 * @brief Serialize macro
 * @param archive Archive to use in serialize
 * @param type Type of value, this will match with the name in the function: reArchiveSerializeXXX
 * @param name Field name to set
 * @param value Value to serialize, must be match with the type in the function
 */
#define RE_SERIALIZE(archive, type, name, value) { \
    reArchiveSerializeStartField(archive, name); \
    reArchiveSerialize##type(archive, value); \
    reArchiveSerializeEndField(archive); \
}

/**
 * @brief Deserialize macro
 * @param archive Archive to use in deserialize
 * @param type Type of value, this will match with the name in the function: reArchiveDeserializeXXX
 * @param name Field name to search
 * @param value Value to deserialize, must be match with the type in the function
 */
#define RE_DESERIALIZE(archive, type, name, value) { \
    reArchiveDeserializeStartField(archive, name); \
    reArchiveDeserialize##type(archive, value); \
    reArchiveDeserializeEndField(archive); \
}


/**
 * @brief Serialize value type
 */
typedef enum ReSerializeValueType {
    RE_SERIALIZE_VALUE_TYPE_UNKNOWN = 0,
    RE_SERIALIZE_VALUE_TYPE_BOOL,
    RE_SERIALIZE_VALUE_TYPE_U8,
    RE_SERIALIZE_VALUE_TYPE_U16,
    RE_SERIALIZE_VALUE_TYPE_U32,
    RE_SERIALIZE_VALUE_TYPE_U64,
    RE_SERIALIZE_VALUE_TYPE_I8,
    RE_SERIALIZE_VALUE_TYPE_I16,
    RE_SERIALIZE_VALUE_TYPE_I32,
    RE_SERIALIZE_VALUE_TYPE_I64,
    RE_SERIALIZE_VALUE_TYPE_F32,
    RE_SERIALIZE_VALUE_TYPE_F64,
    RE_SERIALIZE_VALUE_TYPE_STRING,
    RE_SERIALIZE_VALUE_TYPE_MAX,
} ReSerializeValueType;

// TODO: Improve string serialization, and add ReString serialization
/**
 * @brief Serialize value
 */
typedef struct ReSerializeValue {
    ReSerializeValueType type;
    union {
        b8 bool;
        u8 u8;
        u16 u16;
        u32 u32;
        u64 u64;
        i8 i8;
        i16 i16;
        i32 i32;
        i64 i64;
        f32 f32;
        f64 f64;
        char string[30];
    };
} ReSerializeValue;

// TODO: Add support to recursive fields
/**
 * @brief Serialize field
 */
typedef struct ReSerializeField {
    const char* name;
    void* internal;
} ReSerializeField;

/**
 * @brief Archive serialization type
 */
typedef enum ReArchiveType {
    RE_ARCHIVE_TYPE_UNKNOWN = 0,
    RE_ARCHIVE_TYPE_JSON,
    RE_ARCHIVE_TYPE_MAX,
} ReArchiveType;

// TODO: Use variable to check the serialize mode, to avoid using serialize/deserialize functions
/**
 * @brief "virtual" serialization archive
 */
typedef struct ReArchive {
    ReArchiveType type;
    ReMemoryAllocator* allocator;
    void* internal;
    b8 (*save)(struct ReArchive*, const char*);
    b8 (*load)(const char*, struct ReArchive*);
    b8 (*serializeStartField)(struct ReArchive*, const char*, b8);
    void (*serializeEndField)(struct ReArchive*);
    void (*serialize)(struct ReArchive*, ReSerializeValue*);
    u32 (*deserializeStartField)(struct ReArchive*, const char*, b8);
    void (*deserializeEndField)(struct ReArchive*);
    void (*deserialize)(struct ReArchive*, ReSerializeValue*);
    // Internal use only
    ReSerializeField* currentField;
} ReArchive;

/**
 * @brief Save archive into file
 * @param archive Archive to save
 * @param path Absolute path to file
 * @return Ture if save was successfully, false otherwise
 */
RE_API RE_INLINE b8 reArchiveSave(ReArchive* archive, const char* path) {
    return archive->save(archive, path);
}

/**
 * @brief Load archive from file
 * @param path Absolute path to file
 * @param outArchive Output archive
 * @return True if load was successfully, false otherwise
 */
RE_API RE_INLINE b8 reArchiveLoad(const char* path, ReArchive* outArchive) {
    return outArchive->load(path, outArchive);
}

/**
 * @brief Serialize bool value
 * @param archive Archive to serialize value
 * @param value Bool value
 */
RE_API RE_INLINE void reArchiveSerializeB8(ReArchive* archive, const b8 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_BOOL;
    serialize.bool = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize unsigned int of 8 bits value
 * @param archive ReArchive to serialize value
 * @param value Unsigned int of 8 bits value
 */
RE_API RE_INLINE void reArchiveSerializeU8(ReArchive* archive, const u8 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_U8;
    serialize.u8 = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize unsigned int of 16 bits value
 * @param archive ReArchive to serialize value
 * @param value Unsigned int of 16 bits value
 */
RE_API RE_INLINE void reArchiveSerializeU16(ReArchive* archive, const u16 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_U16;
    serialize.u16 = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize unsigned int of 32 bits value
 * @param archive ReArchive to serialize value
 * @param value Unsigned int of 32 bits value
 */
RE_API RE_INLINE void reArchiveSerializeU32(ReArchive* archive, const u32 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_U32;
    serialize.u32 = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize unsigned int of 64 bits value
 * @param archive ReArchive to serialize value
 * @param value Unsigned int of 64 bits value
 */
RE_API RE_INLINE void reArchiveSerializeU64(ReArchive* archive, const u64 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_U64;
    serialize.u64 = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize int of 8 bits value
 * @param archive ReArchive to serialize value
 * @param value Int of 8 bits value
 */
RE_API RE_INLINE void reArchiveSerializeI8(ReArchive* archive, const i8 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_I8;
    serialize.i8 = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize int of 16 bits value
 * @param archive ReArchive to serialize value
 * @param value Int of 16 bits value
 */
RE_API RE_INLINE void reArchiveSerializeI16(ReArchive* archive, const i16 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_I16;
    serialize.i16 = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize int of 32 bits value
 * @param archive ReArchive to serialize value
 * @param value Int of 32 bits value
 */
RE_API RE_INLINE void reArchiveSerializeI32(ReArchive* archive, const i32 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_I32;
    serialize.i32 = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize int of 64 bits value
 * @param archive ReArchive to serialize value
 * @param value Int of 64 bits value
 */
RE_API RE_INLINE void reArchiveSerializeI64(ReArchive* archive, const i64 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_I64;
    serialize.i64 = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize float value
 * @param archive ReArchive to serialize value
 * @param value Float value
 */
RE_API RE_INLINE void reArchiveSerializeF32(ReArchive* archive, const f32 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_F32;
    serialize.f32 = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize double value
 * @param archive ReArchive to serialize value
 * @param value Double value
 */
RE_API RE_INLINE void reArchiveSerializeF64(ReArchive* archive, const f64 value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_F64;
    serialize.f64 = value;
    archive->serialize(archive, &serialize);
}

/**
 * @brief Serialize string value
 * @param archive ReArchive to serialize value
 * @param value String value
 */
RE_API RE_INLINE void reArchiveSerializeString(ReArchive* archive, const char value[30]) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_STRING;
    strcpy(serialize.string, value);
    archive->serialize(archive, &serialize);
}

/**
 * @brief Deserialize bool value
 * @param archive ReArchive to deserialize value
 * @param value Bool value
 */
RE_API RE_INLINE void reArchiveDeserializeB8(ReArchive* archive, b8* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_BOOL;
    archive->deserialize(archive, &serialize);

    *value = serialize.bool;
}

/**
 * @brief Deserialize unsigned int of 8 bits value
 * @param archive ReArchive to deserialize value
 * @param value Unsigned int of 8 bits value
 */
RE_API RE_INLINE void reArchiveDeserializeU8(ReArchive* archive, u8* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_U8;
    archive->deserialize(archive, &serialize);

    *value = serialize.u8;
}

/**
 * @brief Deserialize unsigned int of 16 bits value
 * @param archive ReArchive to deserialize value
 * @param value Unsigned int of 16 bits value
 */
RE_API RE_INLINE void reArchiveDeserializeU16(ReArchive* archive, u16* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_U16;
    archive->deserialize(archive, &serialize);

    *value = serialize.u16;
}

/**
 * @brief Deserialize unsigned int of 32 bits value
 * @param archive ReArchive to deserialize value
 * @param value Unsigned int of 32 bits value
 */
RE_API RE_INLINE void reArchiveDeserializeU32(ReArchive* archive, u32* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_U32;
    archive->deserialize(archive, &serialize);

    *value = serialize.u32;
}

/**
 * @brief Deserialize unsigned int of 64 bits value
 * @param archive ReArchive to deserialize value
 * @param value Unsigned int of 64 bits value
 */
RE_API RE_INLINE void reArchiveDeserializeU64(ReArchive* archive, u64* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_U64;
    archive->deserialize(archive, &serialize);

    *value = serialize.u64;
}

/**
 * @brief Serialize int of 8 bits value
 * @param archive ReArchive to serialize value
 * @param value Int of 8 bits value
 */
RE_API RE_INLINE void reArchiveDeserializeI8(ReArchive* archive, i8* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_I8;
    archive->deserialize(archive, &serialize);

    *value = serialize.i8;
}

/**
 * @brief Serialize int of 16 bits value
 * @param archive ReArchive to serialize value
 * @param value Int of 16 bits value
 */
RE_API RE_INLINE void reArchiveDeserializeI16(ReArchive* archive, i16* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_I16;
    archive->deserialize(archive, &serialize);

    *value = serialize.i16;
}

/**
 * @brief Serialize int of 32 bits value
 * @param archive ReArchive to serialize value
 * @param value Int of 32 bits value
 */
RE_API RE_INLINE void reArchiveDeserializeI32(ReArchive* archive, i32* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_I32;
    archive->deserialize(archive, &serialize);

    *value = serialize.i32;
}

/**
 * @brief Serialize int of 64 bits value
 * @param archive ReArchive to serialize value
 * @param value Int of 64 bits value
 */
RE_API RE_INLINE void reArchiveDeserializeI64(ReArchive* archive, i64* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_I64;
    archive->deserialize(archive, &serialize);

    *value = serialize.i64;
}

/**
 * @brief Deserialize float value
 * @param archive ReArchive to deserialize value
 * @param value Float value
 */
RE_API RE_INLINE void reArchiveDeserializeF32(ReArchive* archive, f32* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_F32;
    archive->deserialize(archive, &serialize);

    *value = serialize.f32;
}

/**
 * @brief Deserialize double value
 * @param archive ReArchive to deserialize value
 * @param value Double value
 */
RE_API RE_INLINE void reArchiveDeserializeF64(ReArchive* archive, double* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_F64;
    archive->deserialize(archive, &serialize);

    *value = serialize.f64;
}

/**
 * @brief Deserialize string value
 * @param archive ReArchive to deserialize value
 * @param value String value
 */
RE_API RE_INLINE void reArchiveDeserializeString(ReArchive* archive, char* value) {
    ReSerializeValue serialize = {};
    serialize.type = RE_SERIALIZE_VALUE_TYPE_STRING;
    archive->deserialize(archive, &serialize);

    strcpy(value, serialize.string);
}

/**
 * @brief Start a field to serialize
 * @param archive Reference to Archive to use
 * @param name Field name
 */
RE_API RE_INLINE void reArchiveSerializeStartField(ReArchive* archive, const char* name) {
    ReSerializeField* field = RE_ALLOCATE(archive->allocator, ReArchive, RE_MEMORY_TAG_SERIALIZATION);

    field->name = name;

    archive->currentField = field;
    archive->serializeStartField(archive, name, false);
}

/**
 * @brief End field to serialize
 * @param archive Reference to Archive to use
 */
RE_API RE_INLINE void reArchiveSerializeEndField(ReArchive* archive) {
    RE_FREE(archive->allocator, archive->currentField, RE_MEMORY_TAG_SERIALIZATION);

    archive->currentField = NULL;
    archive->serializeEndField(archive);
}

/**
 * @brief Start a field to deserialize
 * @param archive Reference to Archive to use
 * @param name Field name
 */
RE_API RE_INLINE void reArchiveDeserializeStartField(ReArchive* archive, const char* name) {
    ReSerializeField* field = RE_ALLOCATE(archive->allocator, ReSerializeField, RE_MEMORY_TAG_SERIALIZATION);

    field->name = name;

    archive->currentField = field;
    archive->deserializeStartField(archive, name, false);
}

/**
 * @brief End field to deserialize
 * @param archive Reference to Archive to use
 */
RE_API RE_INLINE void reArchiveDeserializeEndField(ReArchive* archive) {
    RE_FREE(archive->allocator, archive->currentField, RE_MEMORY_TAG_SERIALIZATION);

    archive->currentField = NULL;
    archive->deserializeEndField(archive);
}


#endif //RAVEN_ENGINE_SERIALIZE_ARCHIVE_H