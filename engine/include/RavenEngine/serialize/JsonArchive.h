/// @file JsonArchive.h
#ifndef RAVEN_ENGINE_SERIALIZE_JSON_ARCHIVE_H
#define RAVEN_ENGINE_SERIALIZE_JSON_ARCHIVE_H


#include "RavenEngine/serialize/Archive.h"


/**
 * @brief Create a archive for JSON serialization
 * @param allocator Memory allocator to use, could be null
 * @return Reference to JSON archive
 */
RE_API ReArchive* reArchiveJsonCreate(ReMemoryAllocator* allocator);

/**
 * @brief Destroy JSON archive
 * @param archive Reference to archive to destroy
 */
RE_API void reArchiveJsonDestroy(ReArchive* archive);




#endif //RAVEN_ENGINE_SERIALIZE_JSON_ARCHIVE_H