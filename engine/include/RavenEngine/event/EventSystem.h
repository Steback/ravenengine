/// @file EventSystem.h
#ifndef RAVEN_ENGINE_EVENT_EVENT_SYSTEM_H
#define RAVEN_ENGINE_EVENT_EVENT_SYSTEM_H


#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Logger.h"
#include "RavenEngine/core/Memory.h"
#include "RavenEngine/event/Event.h"
#include "RavenEngine/containers/Queue.h"


/**
 * @brief Event system type
 */
typedef enum ReEventType {
    RE_EVENT_TYPE_UNKNOWN = -1,
    RE_EVENT_TYPE_SHUTDOWN = 0,
    RE_EVENT_TYPE_KEY,
    RE_EVENT_TYPE_BUTTON,
    RE_EVENT_TYPE_MOUSE,
    RE_EVENT_TYPE_WHEEL,
    RE_EVENT_TYPE_WINDOW_FOCUS,
    RE_EVENT_TYPE_CURSOR_ENTER,
    RE_EVENT_TYPE_CHAR,
    RE_EVENT_TYPE_MAX_ENGINE_EVENTS,
    RE_EVENT_TYPE_MAX = 256,
} ReEventType;

typedef struct ReEventFireData {
    u32 event;
    void* sender;
    ReEventData data;
} ReEventFireData;

/**
 * @brief Define events array
 */
RE_ARRAY_DEFINE(ReEvent, Event);

/**
 * @brief Define events queue
 */
RE_QUEUE_DEFINE(ReEventFireData, EventFireData);

/**
 * @brief Event system
 */
typedef struct ReEventSystem {
    ReArrayEvent events;
    ReQueueEventFireData eventQueue;
    ReLogger* logger;
    ReMemoryAllocator* allocator;
} ReEventSystem;

/**
 * @brief Event system create info
 */
typedef struct ReEventSystemCreateInfo {
    ReLogger* logger;
    ReMemoryAllocator* allocator;
} ReEventSystemCreateInfo;

/**
 * @brief Create event system
 * @param createInfo Event system create info reference
 * @return Reference to created event system
 */
RE_API ReEventSystem* reEventSystemCreate(const ReEventSystemCreateInfo* createInfo);

/**
 * @brief Destroy event system
 * @param system Event system reference
 */
RE_API void reEventSystemDestroy(ReEventSystem* system);

/**
 * @brief Register event
 * @param system Event system reference
 * @return Event id
 */
RE_API u32 reEventSystemRegisterEvent(ReEventSystem* system);

/**
 * @brief Add listener to event
 * @param system Event system reference
 * @param event Event id
 * @param listener Listener reference
 * @param callback Listener callback
 * @return
 */
RE_API b8 reEventSystemAddListener(const ReEventSystem* system, const u32 event, void* listener, ReEventCallback callback);

/**
 * @brief Remove listener to event
 * @param system Event system reference
 * @param event Event id
 * @param listener Listener reference
 * @param callback Listener callback
 */
RE_API void reEventSystemRemoveListener(const ReEventSystem* system, const u32 event, void* listener, ReEventCallback callback);

/**
 * @brief Fire event
 * @param system Event system reference
 * @param event Event id
 * @param sender Sender reference
 * @param data Event data
 */
RE_API void reEventSystemFire(const ReEventSystem* system, u32 event, void* sender, const ReEventData* data);

/**
 * @brief Queue event
 * @param system Event system reference
 * @param event Event id
 * @param sender Sender reference
 * @param data Event data
 */
RE_API void reEventSystemQueueEvent(ReEventSystem* system, u32 event, void* sender, const ReEventData* data);

/**
 * @brief Event system update tick
 * @param system Event system reference
 */
void reEventSystemUpdate(ReEventSystem* system);


#endif //RAVEN_ENGINE_EVENT_EVENT_SYSTEM_H
