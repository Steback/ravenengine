/// @file Event.h
#ifndef RAVEN_ENGINE_EVENT_EVENT_H
#define RAVEN_ENGINE_EVENT_EVENT_H


#include "RavenEngine/Defines.h"
#include "RavenEngine/containers/Array.h"


/**
 * @brief Event buffer max size
 */
#define RE_EVENT_MAX_BUFFER_SIZE 64


/**
 * @brief Event data
 */
typedef struct ReEventData {
    u32 size;
    u8 buffer[RE_EVENT_MAX_BUFFER_SIZE];
} ReEventData;

/**
 * @brief Event callback
 */
typedef b8 (*ReEventCallback)(u32 id, void* sender, void* listener, const ReEventData* data);

/**
 * @brief Event listener
 */
typedef struct ReEventListener {
    void* listener;
    ReEventCallback callback;
} ReEventListener;

/**
 * @brief Event listener array definition
 */
RE_ARRAY_DEFINE(ReEventListener, EventListener);

/**
 * @brief Event
 */
typedef struct ReEvent {
    i32 id;
    ReArrayEventListener listeners;
} ReEvent;

/**
 * @brief Fire event
 * @param event Event reference
 * @param sender Sender reference
 * @param data Event data reference
 */
void reEventFire(const ReEvent* event, void* sender, const ReEventData* data);

/**
 * @brief Add listener to event
 * @param event Event reference
 * @param listener Listener reference
 * @param callback Listener callback
 * @return
 */
b8 reEventAddListener(ReEvent* event, void* listener, ReEventCallback callback);

/**
 * @brief Remove listener from event
 * @param event Event reference
 * @param listener Listener reference
 * @param callback Listener callback
 */
void reEventRemoveListener(ReEvent* event, void* listener, ReEventCallback callback);


#endif //RAVEN_ENGINE_EVENT_EVENT_H
