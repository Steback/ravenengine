/// @file Platform.h
#ifndef RAVEN_ENGINE_PLATFORM_PLATFORM_HPP
#define RAVEN_ENGINE_PLATFORM_PLATFORM_HPP


#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Memory.h"


/**
 * @brief Console color text
 */
typedef enum ReConsoleColor {
    RE_CONSOLE_COLOR_NONE = 0,
    RE_CONSOLE_COLOR_MAGENTA,
    RE_CONSOLE_COLOR_RED,
    RE_CONSOLE_COLOR_YELLOW,
    RE_CONSOLE_COLOR_GREEN,
    RE_CONSOLE_COLOR_BLUE,
    RE_CONSOLE_COLOR_CYAN,
    RE_CONSOLE_COLOR_MAX,
} ReConsoleColor;

/**
 * @brief Platform system info
 */
typedef struct RePlatform {
    char name[16];
    char os[32];
    char cpu[64];
    u32 ram;
    ReMemoryAllocator* allocator;
} RePlatform;

/**
 * @brief Get platform with system specific data filled
 * @param allocator Allocator pointer
 * @return Pointer to platform, null if something go wrong
 */
RePlatform* rePlatformCreate(ReMemoryAllocator* allocator);

/**
 * @brief Destroy platform
 * @param platform Reference to platform
 */
void rePlatformDestroy(RePlatform* platform);

/**
 * @brief Platform specific console output, must be implement in module
 * @param color Text color output
 * @param fmt Format message string
 * @param ... Arguments to format
 */
void rePlatformConsoleWrite(const ReConsoleColor color, const char* fmt, ...);

/**
 * @brief Platform allocate memory, must be implement in module
 * @param size Memory size to allocate
 * @return Pointer to memory block
 */
void* rePlatformMemoryAllocate(u64 size);

/**
 * @brief Platform reallocate memory, must be implement in module
 * @param block Old block pointer
 * @param size New block size
 * @return Pointer to new memory block
 */
void* rePlatformMemoryReallocate(void* block, u64 size);

/**
 * @brief Platform memory free, must be implement in module
 * @param block Pointer to memory block to free
 */
void rePlatformMemoryFree(void* block);

/**
 * @brief Platform memory set zero, must be implement in module
 * @param block Pointer to memory block
 * @param size Size of memory to ser
 * @return Pointer to new memory block with values set to zero
 */
void* rePlatformMemoryZero(void* block, u64 size);

/**
 * @brief Platform memory copy, must be implement in module
 * @param dst Pointer to destination memory block
 * @param src Pointer to source memory block
 * @param size Memory size to copy
 */
void rePlatformMemoryCopy(void* dst, const void* src, u64 size);

/**
 * @brief Platform memory ser, must be implement in module
 * @param block Pointer to memory block
 * @param value Value to set to memory
 * @param size Memory size to set
 * @return Pointer to new memory block with value set
 */
void* rePlatformMemorySet(void* block, i32 value, u64 size);

/**
 * @brief Check if path exists, must be implement in module
 * @param path String with path to check
 * @return True if path exists
 */
b8 rePlatformPathExists(const char* path);

/**
 * @brief Check if path is a directory, must be implement in module
 * @param path String with path to check
 * @return True if path is a directory
 */
b8 rePlatformPathIsDirectory(const char* path);

/**
 * @brief Check if path is absolute, must be implement in module
 * @param path String with path to check
 * @return True if is absolute
 */
b8 rePlatformPathIsAbsolute(const char* path);

/**
 * @brief Get current working directory path
 * @param output Output string, must have size enough, use RE_PATH_MAX_SIZE
 * @return Path filled, must be destroyed manually
 */
void rePlatformGetCurrentPath(char* output);

/**
 * @brief Create a directory, using platform specific, must be implemented in module
 * @param path Relative or absolute path to directory to create
 */
void rePlatformCreateDirectory(const char* path);

/**
 * @brief Get current time
 * @return Current time in seconds
 */
f64 rePlatformGetCurrentTime();


#endif //RAVEN_ENGINE_PLATFORM_PLATFORM_HPP
