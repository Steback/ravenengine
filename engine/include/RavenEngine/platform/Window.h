/// @file Window.h
#ifndef RAVEN_ENGINE_PLATFORM_WINDOW_H
#define RAVEN_ENGINE_PLATFORM_WINDOW_H


#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Logger.h"
#include "RavenEngine/core/Memory.h"
#include "RavenEngine/event/EventSystem.h"


/**
 * @brief Platform specific window
 */
typedef struct ReWindow ReWindow;

/**
 * @brief Window size
 */
typedef struct ReWindowSize {
    i32 width;
    i32 height;
} ReWindowSize;

/**
 * @brief Window create info
 */
typedef struct ReWindowCreateInfo {
    ReWindowSize size;
    const char* title;
    ReLogger* logger;
    ReMemoryAllocator* allocator;
    ReEventSystem* eventSystem;
} ReWindowCreateInfo;

/**
 * @brief Create window
 * @param createInfo Reference to create info
 * @return Pointer to window, will be null if something go wrong
 */
ReWindow* reWindowCreate(const ReWindowCreateInfo* createInfo);

/**
 * @brief Destroy window
 * @param window Reference to window
 */
void reWindowDestroy(ReWindow* window);

/**
 * @brief Get window size
 * @param window Reference to window
 * @return Window size
 */
ReWindowSize reWindowGetSize(const ReWindow* window);

/**
 * @brief Get window framebuffer size
 * @param window Reference to window
 * @return Window framebuffer size
 */
ReWindowSize reWindowGetFramebufferSize(const ReWindow* window);

/**
 * @brief Update window title
 * @param window Reference to window
 * @param title New window title
 */
void reWindowUpdateTitle(const ReWindow* window, const char* title);

/**
 * @brief Process input and check for close window
 * @param window Reference to window
 * @return True if there is no close window input
 */
b8 reWindowProcessInput(const ReWindow* window);


#endif //RAVEN_ENGINE_PLATFORM_WINDOW_H
