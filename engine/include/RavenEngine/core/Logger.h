/// @file Logger.h
#ifndef RAVEN_ENGINE_LOGGER_LOGGER_HPP
#define RAVEN_ENGINE_LOGGER_LOGGER_HPP


#include <stdio.h>
#ifdef _MSC_VER
#include <string.h>
#endif

#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Memory.h"


#ifdef RE_DEBUG_BUILD
#ifndef LOG_TRACE_ENABLED
#define LOG_TRACE_ENABLED 1
#endif
#endif

/**
* @brief Log default macro
*/
#define LOG(logger, level, ...) reLoggerLog(logger, level, __FILE_NAME__, __LINE__, __VA_ARGS__)

/**
* @brief Log fatal macro
*/
#define LOG_FATAL(logger, ...) LOG(logger, RE_LOG_LEVEL_FATAL, __VA_ARGS__);

/**
* @brief Log error macro
*/
#define LOG_ERROR(logger, ...) LOG(logger, RE_LOG_LEVEL_ERROR, __VA_ARGS__);

/**
* @brief Log warm macro
*/
#define LOG_WARN(logger, ...) LOG(logger, RE_LOG_LEVEL_WARN, __VA_ARGS__);

/**
* @brief Log info macro
*/
#define LOG_INFO(logger, ...) LOG(logger, RE_LOG_LEVEL_INFO, __VA_ARGS__);

#if RE_DEBUG_BUILD
/**
* @brief Log debug macro
* @note Only available in debug builds
*/
#define LOG_DEBUG(logger, ...) LOG(logger, RE_LOG_LEVEL_DEBUG, __VA_ARGS__);
#else
/**
* @brief Log debug macro
* @note Only available in debug builds
*/
#define LOG_DEBUG(logger, ...)
#endif

#if defined(RE_DEBUG_BUILD) && LOG_TRACE_ENABLED
/**
* @brief Log trace macro
* @note Only available in debug builds and LOG_TRACE_ENABLED == 1
*/
#define LOG_TRACE(logger, ...) LOG(logger, RE_LOG_LEVEL_TRACE, __VA_ARGS__);
#else
/**
* @brief Log trace macro
* @note Only available in debug builds and LOG_TRACE_ENABLED == 1
*/
#define LOG_TRACE(logger, ...)
#endif


typedef struct RePath RePath;

/**
* @brief Log level enumeration
*/
typedef enum ReLogLevel {
    RE_LOG_LEVEL_FATAL = 0,
    RE_LOG_LEVEL_ERROR,
    RE_LOG_LEVEL_WARN,
    RE_LOG_LEVEL_INFO,
    RE_LOG_LEVEL_DEBUG,
    RE_LOG_LEVEL_TRACE,
    RE_LOG_LEVEL_MAX
} ReLogLevel;

/**
* @brief Logger callback
*/
typedef void (*ReLogCallback)(ReLogLevel level, const char* file, unsigned int line, const char* message);

/**
* @brief Logger
*/
typedef struct ReLogger {
    ReLogCallback callback;
    FILE* file;
    ReMemoryAllocator* allocator;
} ReLogger;

/**
* @brief Logger create info data
*/
typedef struct ReLoggerCreteInfo {
    ReLogCallback callback;
    RePath* path;
    ReMemoryAllocator* allocator;
} ReLoggerCreteInfo;

/**
 * @brief Create logger
 * @param creationInfo Reference to create info logger data
 * @return pointer to logger, null if something go wrong
 */
RE_API ReLogger* reLoggerCreate(const ReLoggerCreteInfo* creationInfo);

/**
 * @brief Destroy logger
 * @param logger valid reference to logger to destroy
 */
RE_API void reLoggerDestroy(ReLogger* logger);

/**
 * @brief Log to function
 * @param logger Reference to logger, if is null just log to console
 * @param level Log level
 * @param file File when is the log called
 * @param line Line when is the log called
 * @param fmt Message format
 * @param ... Message arguments
 */
RE_API void reLoggerLog(const ReLogger* logger, ReLogLevel level, const char* file, unsigned int line, const char* fmt, ...);


#endif //RAVEN_ENGINE_LOGGER_LOGGER_HPP
