/// @file Assert.h
#ifndef RAVEN_ENGINE_CORE_ASSERT_H
#define RAVEN_ENGINE_CORE_ASSERT_H


#include <stdlib.h>

#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Logger.h"


#if defined(_MSC_VER)
#define DEBUG_BREAK() __debugbreak()
#elif defined(__GNUC__) || defined(__clang__)
#define DEBUG_BREAK() __builtin_trap()
#else
#define DEBUG_BREAK() abort()
#endif

#if RE_DEBUG_BUILD || RE_ENABLE_ASSERTS
#define RE_ASSERT(expr) \
if (!(expr)) { \
    LOG_FATAL(NULL, "Assertion failed: (%s) | Function %s", #expr, __func__, __FILE__, __LINE__); \
    DEBUG_BREAK(); \
    abort(); \
}

#define RE_ASSERT_MSG(expr, message) \
if (!(expr)) { \
    LOG_FATAL(NULL, "Assertion failed: (%s) | Function %s | %s", #expr, __func__, message); \
    DEBUG_BREAK(); \
    abort(); \
}
#else
#define RE_ASSERT(expr)
#define RE_ASSERT_MSG(expr, message)
#endif


#endif //RAVEN_ENGINE_CORE_ASSERT_H
