/// @file Settings.h
#ifndef RAVEN_ENGINE_CORE_SETTINGS_HPP
#define RAVEN_ENGINE_CORE_SETTINGS_HPP


#include "RavenEngine/Defines.h"
#include "RavenEngine/serialize/Archive.h"


#define SETTINGS_FILENAME "settings.json"


typedef struct RePath RePath;

/**
 * @brief Settings create info
 */
typedef struct ReSettingsCreateInfo {
    ReLogger* logger;
    ReMemoryAllocator* allocator;
} ReSettingsCreateInfo;

/**
 * @brief Settings
 */
typedef struct ReSettings {
    u32 width;
    u32 height;
    const char* applicationName;
    ReLogger* logger;
    ReMemoryAllocator* allocator;
} ReSettings;

/**
 * @brief Create settings
 * @param createInfo Settings create info data
 * @return Pointer to settings, nullptr if something go wrong
 */
ReSettings* reSettingsCreate(const ReSettingsCreateInfo* createInfo);

/**
 * @brief  Destroy settings
 * @param settings Reference to settings to destroy
 */
void reSettingsDestroy(ReSettings* settings);

/**
 * @brief Set default values to settings
 * @param settings Reference to settings to fill
 */
void reSettingSetDefaults(ReSettings* settings);

/**
 * @brief Save setting to file
 * @param settings Reference to setting to save
 * @param path Absolute path to settings file
 * @return True if the save was successfully
 */
b8 reSettingsSave(const ReSettings* settings, const RePath* path);

/**
 * @brief Load settings from file
 * @param path Absolute path to settings file
 * @param settings Reference settings to fill
 * @return True if load was successfully
 */
b8 reSettingsLoad(const RePath* path, ReSettings* settings);

/**
 * @brief Serialize settings data
 * @param archive Reference to archive to use
 * @param settings Reference to settings to use
 */
void reArchiveSerializeSettings(ReArchive* archive, const ReSettings* settings);

/**
 * @brief Deserialize settings data
 * @param archive Reference to archive to use
 * @param settings Reference to settings to fill
 */
void reArchiveDeserializeSettings(ReArchive* archive, ReSettings* settings);


#endif //RAVEN_ENGINE_CORE_SETTINGS_HPP
