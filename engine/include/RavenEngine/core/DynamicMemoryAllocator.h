/// @file DynamicMemoryAllocator.h
#ifndef RAVEN_ENGINE_CORE_DYNAMIC_MEMORY_ALLOCATOR_H
#define RAVEN_ENGINE_CORE_DYNAMIC_MEMORY_ALLOCATOR_H


#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Memory.h"


/**
 * @brief Dynamic memory allocator
 */
typedef struct DynamicMemoryAllocator DynamicMemoryAllocator;

/**
 * @brief Create dynamic memory allocator
 * @param createInfo Memory allocator create info pointer
 * @return Memory allocator valid pointer, null if something go wrong
 */
ReMemoryAllocator* reDynamicMemoryAllocatorCreate(ReMemoryAllocatorCreateInfo* createInfo);

/**
 * @brief Destroy dynamic memory allocator
 * @param self Memory allocator reference
 */
void reDynamicMemoryAllocatorDestroy(ReMemoryAllocator* self);


#endif //RAVEN_ENGINE_CORE_DYNAMIC_MEMORY_ALLOCATOR_H
