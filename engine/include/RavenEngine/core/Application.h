/// @file Application.h
#ifndef RAVEN_ENGINE_CORE_APPLICATION_H
#define RAVEN_ENGINE_CORE_APPLICATION_H


#include "RavenEngine/Engine.h"


/**
 * @brief Application
 */
typedef struct ReApplication {
    const char* name;
    void (*setup)(ReApplication*, ReEngine*);
    void (*shutdown)(ReApplication*, ReEngine*);
    void (*update)(ReApplication*, ReEngine*, float);
    void (*render)(ReApplication*, ReEngine*);
    ReMemoryAllocator* allocator;
    ReLogger* logger;
} ReApplication;

/**
 * @brief Application create info
 */
typedef struct ReApplicationCreateInfo {
    const char* name;
    void (*setup)(ReApplication*, ReEngine*);
    void (*shutdown)(ReApplication*, ReEngine*);
    void (*update)(ReApplication*, ReEngine*, float);
    void (*render)(ReApplication*, ReEngine*);
} ReApplicationCreateInfo;


#endif //RAVEN_ENGINE_CORE_APPLICATION_H
