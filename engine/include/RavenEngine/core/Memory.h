/// @file Memory.h
#ifndef RAVEN_ENGINE_CORE_MEMORY_H
#define RAVEN_ENGINE_CORE_MEMORY_H


#include "RavenEngine/Defines.h"


#define RE_DEFAULT_ALIGNMENT 16
#define RE_MEMORY_BLOCK_SIZE (1024 * 1024) // 1MiB

#define RE_GET_SIZE_ALIGNED(size, alignment) (((size) + alignment - 1) & ~(alignment - 1))

#define RE_ALLOCATE(allocator, type, tag) reMemoryAllocate(allocator, sizeof(type), tag)

#define RE_ALLOCATE_SIZE(allocator, size, tag) reMemoryAllocate(allocator, size, tag)

#define RE_REALLOCATE(allocator, block, size, tag) reMemoryReallocate(allocator, block, size, tag)

#define RE_FREE(allocator, block, tag) reMemoryFree(allocator, block, tag)

#define RE_FREE_SIZE(allocator, block, tag) reMemoryFree(allocator, block, tag)


typedef struct ReLogger ReLogger;

/**
 * @brief Memory tag
 */
typedef enum ReMemoryTag {
    RE_MEMORY_TAG_UNKNOWN = 0,
    RE_MEMORY_TAG_ENGINE,
    RE_MEMORY_TAG_LOGGER,
    RE_MEMORY_TAG_STRING,
    RE_MEMORY_TAG_FILESYSTEM,
    RE_MEMORY_TAG_SERIALIZATION,
    RE_MEMORY_TAG_ARRAY,
    RE_MEMORY_TAG_QUEUE,
    RE_MEMORY_TAG_EVENT,
    RE_MEMORY_TAG_INPUT,
    RE_MEMORY_TAG_MAX,
} ReMemoryTag;

/**
* @brief Memory stats
*/
typedef struct ReMemoryStats {
    u64 totalMemoryAllocated;
    u64 taggedAllocations[RE_MEMORY_TAG_MAX];
} ReMemoryStats;

/**
 * @brief Memory system
 */
typedef struct ReMemorySystem {
    ReMemoryStats stats;
    u32 size;
    u8* memory;
} ReMemorySystem;

/**
 * @brief Memory system create info
 */
typedef struct ReMemorySystemCreateInfo {
    u64 blockSize;
} ReMemorySystemCreateInfo;

typedef struct ReMemoryAllocator ReMemoryAllocator;

/**
 * @brief Allocate function definition
 */
typedef void* (*ReAllocateFn)(ReMemoryAllocator* self, u64 size, ReMemoryTag tag);

/**
 * @brief Reallocate function definition
 */
typedef void* (*ReReallocateFn)(ReMemoryAllocator* self, void* block, u64 size, ReMemoryTag tag);

/**
 * @brief Free function definition
 */
typedef void (*ReFreeFn)(ReMemoryAllocator* self, void* block, ReMemoryTag tag);

/**
 * @brief Static memory allocator
 */
typedef struct ReMemoryAllocator {
    u32 alignment;
    ReMemorySystem* system;
    ReAllocateFn allocate;
    ReReallocateFn reallocate;
    ReFreeFn free;
    void* internal;
} ReMemoryAllocator;

/**
 * @brief Memory allocator create info data
 */
typedef struct ReMemoryAllocatorCreateInfo {
    u32 alignment;
    ReMemorySystem* system;
    ReAllocateFn allocate;
    ReReallocateFn reallocate;
    ReFreeFn free;
} ReMemoryAllocatorCreateInfo;

/**
 * @brief @brief Create memory system
 * @param createInfo Memory system create info pointer
 * @return Valid pointer to memory system, NULL if something go wrong
 */
ReMemorySystem* reMemorySystemCreate(const ReMemorySystemCreateInfo* createInfo);

/**
 * @brief Destroy memory system
 * @param self Memory system pointer
 */
void reMemorySystemDestroy(ReMemorySystem* self);

/**
 * @brief Create static memory allocator
 * @param createInfo Memory allocator create info
 * @return Pointer to memory allocator reference, null if something go wrong
 */
RE_API ReMemoryAllocator* reMemoryAllocatorCreate(const ReMemoryAllocatorCreateInfo* createInfo);

/**
 * @brief Destroy and free memory allocator reference
 * @param allocator Reference to valid memory allocator
 */
RE_API void reMemoryAllocatorDestroy(ReMemoryAllocator* allocator);

/**
 * @brief Allocate memory block
 * @param allocator Memory allocator pointer, could be NULL
 * @param size Size of memory to allocate
 * @param tag Memory tag to match memory use
 * @note If memory allocator is null the memory will be allocated using the heap
 * @return Reference to memory block
 */
RE_API void* reMemoryAllocate(ReMemoryAllocator* allocator, u64 size, ReMemoryTag tag);

/**
 * @brief Memory re allocation
 * @param allocator Memory allocator pointer, could be NULL
 * @param block Memory block to re allocate
 * @param size New desired size
 * @param tag Memory tag to match memory use
 * @note If memory allocator is null the memory will be allocated using the heap
 * @return New resized memory block
 */
RE_API void* reMemoryReallocate(ReMemoryAllocator* allocator, void* block, u64 size, ReMemoryTag tag);

/**
 * @brief Free memory block
 * @param allocator Memory allocator pointer, could be NULL
 * @param block Reference to memory block
 * @param tag Memory tag to match memory use
 * @note If the allocator is null, the memory must be allocated using the heap, otherwise undefined behaviour will happen
 */
RE_API void reMemoryFree(ReMemoryAllocator* allocator, void* block, ReMemoryTag tag);

/**
 * @brief Set memory block to zero value
 * @param block Reference to block to use
 * @param size Size of memory block
 * @return Reference to block with zero value
 */
RE_API void* reMemoryZero(void* block, u64 size);

/**
 * @brief Copy content from one memory block to another
 * @param dst Destination memory block reference
 * @param src Source memory block reference
 * @param size Size of memory block to copy
 * @return Reference to source block with data copied
 */
RE_API void reMemoryCopy(void* dst, const void* src, u64 size);

/**
 * @brief Set specific value to memory block
 * @param dst Destination memory block to set value
 * @param value Value to set
 * @param size Size of memory to set value
 * @return Pointer to destination block with value set
 */
RE_API void* reMemorySet(void* dst, i32 value, u64 size);


#endif //RAVEN_ENGINE_CORE_MEMORY_H
