/// @file ToString.h
#ifndef RAVEN_ENGINE_UTILS_TO_STRING_H
#define RAVEN_ENGINE_UTILS_TO_STRING_H


#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Memory.h"
#include "RavenEngine/filesystem/Filesystem.h"


/**
 * @brief Get memory tag string
 * @param tag Memory tag to use
 * @return String that match tag
 */
RE_API RE_INLINE const char* reToStringMemoryTag(const ReMemoryTag tag) {
    switch (tag) {
        case RE_MEMORY_TAG_UNKNOWN:
            return "Unknown";
        case RE_MEMORY_TAG_ENGINE:
            return "Engine";
        case RE_MEMORY_TAG_LOGGER:
            return "Logger";
        case RE_MEMORY_TAG_STRING:
            return "String";
        case RE_MEMORY_TAG_SERIALIZATION:
            return "Serialization";
        case RE_MEMORY_TAG_FILESYSTEM:
            return "Filesystem";
        case RE_MEMORY_TAG_MAX:
            return "Max";
        case RE_MEMORY_TAG_ARRAY:
            return "Array";
        case RE_MEMORY_TAG_EVENT:
            return "Event";
        case RE_MEMORY_TAG_INPUT:
            return "Input";
        case RE_MEMORY_TAG_QUEUE:
            return "Queue";
        default: ;
    }

    return "Unknown";
}

/**
 * @brief Get filesystem path string
 * @param path Path enum type
 * @return String that match tag
 */
RE_API RE_INLINE const char* reToStringFilesystemPath(const ReFilesystemPath path) {
    switch (path) {
        case RE_FILESYSTEM_PATH_ROOT:
            return "Root";
        case RE_FILESYSTEM_PATH_BINARIES:
            return "Binaries";
        case RE_FILESYSTEM_PATH_DATA:
            return "Data";
        case RE_FILESYSTEM_PATH_CACHE:
            return "Cache";
        case RE_FILESYSTEM_PATH_LOGS:
            return "Logs";
        case RE_FILESYSTEM_PATH_MAX:
            return "MaX";
        default: ;
    }

    return "Unknown";
}


#endif //RAVEN_ENGINE_UTILS_TO_STRING_H
