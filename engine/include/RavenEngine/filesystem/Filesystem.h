/// @file Filesystem.h
#ifndef RAVEN_ENGINE_FILESYSTEM_FILESYSTEM_H
#define RAVEN_ENGINE_FILESYSTEM_FILESYSTEM_H


#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Memory.h"
#include "RavenEngine/filesystem/Path.h"


#define RE_DATA_DIR_NAME "data"
#define RE_CACHE_DIR_NAME "cache"
#define RE_LOGS_DIR_NAME "logs"


/**
 * @brief Filesystem path type
 */
typedef enum ReFilesystemPath {
    RE_FILESYSTEM_PATH_ROOT = 0,
    RE_FILESYSTEM_PATH_BINARIES = 1,
    RE_FILESYSTEM_PATH_DATA = 2,
    RE_FILESYSTEM_PATH_CACHE = 3,
    RE_FILESYSTEM_PATH_LOGS = 4,
    RE_FILESYSTEM_PATH_MAX,
} ReFilesystemPath;

/**
 * @brief Filesystem
 */
typedef struct ReFilesystem {
    RePath paths[RE_FILESYSTEM_PATH_MAX];
    ReMemoryAllocator* allocator;
} ReFilesystem;

/**
 * @brief Filesystem create info
 */
typedef struct ReFilesystemCreateInfo {
    ReMemoryAllocator* allocator;
} ReFilesystemCreateInfo;

/**
 * @brief Filesystem create and setup all paths
 * @param createInfo Filesystem create info data
 * @return Pointer to filesystem, will be NULL if something go wrong
 */
ReFilesystem* reFilesystemCreate(const ReFilesystemCreateInfo* createInfo);

/**
 * @brief Cleanup paths and destroy filesystem
 * @param filesystem Reference to filesystem
 */
void reFilesystemDestroy(ReFilesystem* filesystem);

/**
 * @brief Search for dir/file using registered paths
 * @param filesystem Reference to filesystem
 * @param path Relative path to use
 * @return Path filled if the dir/file exits, empty otherwise
 */
RE_API RePath reFilesystemSearch(const ReFilesystem* filesystem, const char* path);

/**
 * @brief Create a directory
 * @param path Reference to path to create directory
 */
RE_API void reFilesystemCreateDirectory(const RePath* path);


#endif //RAVEN_ENGINE_FILESYSTEM_FILESYSTEM_H
