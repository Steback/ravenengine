/// @file Path.h
#ifndef RAVEN_ENGINE_FILESYSTEM_PATH_H
#define RAVEN_ENGINE_FILESYSTEM_PATH_H


#include "RavenEngine/Defines.h"
#include "RavenEngine/core/Memory.h"


#ifdef PLATFORM_WINDOWS
#define RE_PATH_SEPARATOR '\\'
#define RE_PATH_SEPARATOR_STR "\\"
#else
#define RE_PATH_SEPARATOR '/'
#define RE_PATH_SEPARATOR_STR "/"
#endif

/**
 * @brief Max path string size
 */
#define RE_PATH_MAX_SIZE 1024

/**
 * @brief Max path extensions string size
 */
#define RE_PATH_EXT_MAX_SIZE 8

/**
 * @brief Max path filename string size
 */
#define RE_PATH_FILENAME_MAX_SIZE 64


/**
 * @brief Filesystem path
 */
typedef struct RePath {
    u32 length;
    char* path;
    ReMemoryAllocator* allocator;
} RePath;

/**
 * @brief Create path from string
 * @param data C-Style string of path
 * @param allocator Memory allocator to use in memory handle
 * @return RePath struct data filled if everything is good, otherwise empty
 */
RE_API RePath rePathCreate(const char* data, ReMemoryAllocator* allocator);

/**
 * @brief Cleanup resources and empty path struct
 * @param path Reference to path to destroy
 */
RE_API void rePathDestroy(RePath* path);

/**
 * @brief Append a relative path to the other path
 * @param path Path to make append
 * @param data Relative path to append
 */
RE_API void rePathAppend(RePath* path, const char* data);

/**
 * @brief Check if path exists
 * @param path Reference to valid path to check
 * @return True if path exists
 */
RE_API b8 rePathExists(const RePath* path);

/**
 * @brief Check if path is a directory
 * @param path Reference to path to check
 * @return True if path is a directory
 */
RE_API b8 rePathIsDirectory(const RePath* path);

/**
 * @brief Check if path is absolute
 * @param path String with path to check
 * @return True if is absolute
 */
RE_API b8 rePathIsAbsolute(const RePath* path);

/**
 * @brief Get path filename
 * @param path Reference to path to use
 * @param outFilename Output filename, must have enough size, use RE_PATH_FILENAME_MAX_SIZE
 */
RE_API void rePathFilename(const RePath* path, char* outFilename);

/**
 * @brief Get path filename without extension
 * @param path Reference to path to use
 * @param outFilename Output filename without extension, must have enough size, use RE_PATH_FILENAME_MAX_SIZE
 */
RE_API void rePathStem(const RePath* path, char* outFilename);

/**
 * @brief Get path extensions
 * @param path Reference path to use
 * @param outExtension Output extension, must have enough size, use RE_PATH_FILENAME_MAX_SIZE
 */
RE_API void rePathExtension(const RePath* path, char* outExtension);

/**
 * @brief Get parent path from path
 * @param path Reference to path to use
 * @param outPath Output path, must have size enough, use RE_PATH_MAX_SIZE
 */
RE_API void rePathParent(const RePath* path, char* outPath);


#endif //RAVEN_ENGINE_FILESYSTEM_PATH_H
