/// @file Defines.h
#ifndef RAVEN_ENGINE_DEFINES_H
#define RAVEN_ENGINE_DEFINES_H


#define ENGINE_NAME "Raven Engine"
#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_PATCH 0

#define RE_MAX_MESSAGE_SIZE 2048
#define RE_MAX_DATE_FORMAt_SIZE 32

#define MAKE_VERSION(major, minor, patch) ((((u32)(major)) << 16U) | (((u32)(minor)) << 8U) | ((u32)(patch)))

/** @brief Unsigned 8-bit integer */
typedef unsigned char u8;

/** @brief Unsigned 16-bit integer */
typedef unsigned short u16;

/** @brief Unsigned 32-bit integer */
typedef unsigned int u32;

/** @brief Unsigned 64-bit integer */
typedef unsigned long long u64;

// Signed int types.

/** @brief Signed 8-bit integer */
typedef signed char i8;

/** @brief Signed 16-bit integer */
typedef signed short i16;

/** @brief Signed 32-bit integer */
typedef signed int i32;

/** @brief Signed 64-bit integer */
typedef signed long long i64;

// Floating point types

/** @brief 32-bit floating point number */
typedef float f32;

/** @brief 64-bit floating point number */
typedef double f64;

// Boolean types

/** @brief 32-bit boolean type, used for APIs which require it */
typedef int b32;

/** @brief 8-bit boolean type */
typedef _Bool b8;

/** @brief True.*/
#define true 1

/** @brief False. */
#define false 0

// Platform detection
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#define PLATFORM_WINDOWS
#ifndef _WIN64
#error "64-bit is required on Windows!"
#endif
#elif defined(__linux__) || defined(__gnu_linux__)
// Linux OS
#define PLATFORM_LINUX
#else
#error "Unknown platform!"
#endif

#ifndef __FILE_NAME__
#ifdef PLATFORM_WINDOWS
#define __FILE_NAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#else
#define __FILE_NAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif
#endif

#ifdef RE_EXPORT
#ifdef _MSC_VER
#define RE_API __declspec(dllexport)
#else
#define RE_API __attribute__((visibility("default")))
#endif
#else
#ifdef _MSC_VER
/** @brief Import/export qualifier */
#define RE_API
#else
/** @brief Import/export qualifier */
#define RE_API
#endif
#endif

// Inlining
#if defined(__clang__) || defined(__gcc__)
/** @brief Inline qualifier */
#define INLINE __attribute__((always_inline)) inline

/** @brief No-inline qualifier */
#define NOINLINE __attribute__((noinline))
#elif defined(_MSC_VER)

/** @brief Inline qualifier */
#define INLINE __forceinline

/** @brief No-inline qualifier */
#define NOINLINE __declspec(noinline)
#else

/** @brief Inline qualifier */
#define RE_INLINE static inline

/** @brief No-inline qualifier */
#define NOINLINE
#endif


#endif // RAVEN_ENGINE_DEFINES_H
