/// @file Input.h
#ifndef RAVEN_ENGINE_INPUT_INPUT_H
#define RAVEN_ENGINE_INPUT_INPUT_H


#include "RavenEngine/Defines.h"


/**
 * @brief Input mod flag
 */
typedef enum ReInputMod {
    RE_INPUT_MOD_NONE = 0x0,
    RE_INPUT_MOD_SHIFT = 0x1,
    RE_INPUT_MOD_CONTROL = 0x2,
    RE_INPUT_MOD_ALT = 0x4,
    RE_INPUT_MOD_SUPER = 0x8,
    RE_INPUT_MOD_CAPSLOCK = 0x10,
    RE_INPUT_MOD_NUM_LOCK = 0x20,
} ReInputMod;

/**
 * @brief Input mod flags
 */
typedef u32 ReInputMods;

/**
 * @brief Input button enumeration
 */
typedef enum ReInputButton {
    RE_INPUT_BUTTON_N1 = 0,
    RE_INPUT_BUTTON_N2 = 1,
    RE_INPUT_BUTTON_N3 = 2,
    RE_INPUT_BUTTON_N4 = 3,
    RE_INPUT_BUTTON_N5 = 4,
    RE_INPUT_BUTTON_N6 = 5,
    RE_INPUT_BUTTON_N7 = 6,
    RE_INPUT_BUTTON_N8 = 7,
    RE_INPUT_BUTTON_MAX,
    RE_INPUT_BUTTON_LEFT = RE_INPUT_BUTTON_N1,
    RE_INPUT_BUTTON_RIGHT = RE_INPUT_BUTTON_N2,
    RE_INPUT_BUTTON_MIDDLE = RE_INPUT_BUTTON_N3,
    RE_INPUT_BUTTON_LAST = RE_INPUT_BUTTON_N8,
} ReInputButton;

/**
 * @brief Input mode enumeration
 */
typedef enum ReInputMode {
    RE_INPUT_MODE_RELEASE = 0,
    RE_INPUT_MODE_PRESS = 1,
    RE_INPUT_MODE_REPEAT = 2,
} ReInputMode;

/**
 * @brief Input key enumeration
 */
typedef enum ReInputKey {
    RE_INPUT_KEY_UNKNOWN = -1,
    RE_INPUT_KEY_SPACE = 32,
    RE_INPUT_KEY_APOSTROPHE = 39,   /* ' */
    RE_INPUT_KEY_COMMA = 44,   /* , */
    RE_INPUT_KEY_MINUS = 45,   /* - */
    RE_INPUT_KEY_PERIOD = 46,   /* . */
    RE_INPUT_KEY_SLASH = 47,   /* / */
    RE_INPUT_KEY_0 = 48,
    RE_INPUT_KEY_1 = 49,
    RE_INPUT_KEY_2 = 50,
    RE_INPUT_KEY_3 = 51,
    RE_INPUT_KEY_4 = 52,
    RE_INPUT_KEY_5 = 53,
    RE_INPUT_KEY_6 = 54,
    RE_INPUT_KEY_7 = 55,
    RE_INPUT_KEY_8 = 56,
    RE_INPUT_KEY_9 = 57,
    RE_INPUT_KEY_SEMICOLON = 59,   /* ; */
    RE_INPUT_KEY_EQUAL = 61,   /* = */
    RE_INPUT_KEY_A = 65,
    RE_INPUT_KEY_B = 66,
    RE_INPUT_KEY_C = 67,
    RE_INPUT_KEY_D = 68,
    RE_INPUT_KEY_E = 69,
    RE_INPUT_KEY_F = 70,
    RE_INPUT_KEY_G = 71,
    RE_INPUT_KEY_H = 72,
    RE_INPUT_KEY_I = 73,
    RE_INPUT_KEY_J = 74,
    RE_INPUT_KEY_K = 75,
    RE_INPUT_KEY_L = 76,
    RE_INPUT_KEY_M = 77,
    RE_INPUT_KEY_N = 78,
    RE_INPUT_KEY_O = 79,
    RE_INPUT_KEY_P = 80,
    RE_INPUT_KEY_Q = 81,
    RE_INPUT_KEY_R = 82,
    RE_INPUT_KEY_S = 83,
    RE_INPUT_KEY_T = 84,
    RE_INPUT_KEY_U = 85,
    RE_INPUT_KEY_V = 86,
    RE_INPUT_KEY_W = 87,
    RE_INPUT_KEY_X = 88,
    RE_INPUT_KEY_Y = 89,
    RE_INPUT_KEY_Z = 90,
    RE_INPUT_KEY_LEFT_BRACKET = 91,   /* [ */
    RE_INPUT_KEY_BACKSLASH = 92,   /* \ */
    RE_INPUT_KEY_RIGHT_BRACKET = 93,   /* ] */
    RE_INPUT_KEY_GRAVE_ACCENT = 96,   /* ` */
    RE_INPUT_KEY_WORLD1 = 161,  /* non-US #1 */
    RE_INPUT_KEY_WORLD2 = 162,  /* non-US #2 */
    RE_INPUT_KEY_ESCAPE = 256,
    RE_INPUT_KEY_ENTER = 257,
    RE_INPUT_KEY_TAB = 258,
    RE_INPUT_KEY_BACKSPACE = 259,
    RE_INPUT_KEY_INSERT = 260,
    RE_INPUT_KEY_DELETE = 261,
    RE_INPUT_KEY_RIGHT = 262,
    RE_INPUT_KEY_LEFT = 263,
    RE_INPUT_KEY_DOWN = 264,
    RE_INPUT_KEY_UP = 265,
    RE_INPUT_KEY_PAGE_UP = 266,
    RE_INPUT_KEY_PAGE_DOWN = 267,
    RE_INPUT_KEY_HOME = 268,
    RE_INPUT_KEY_END = 269,
    RE_INPUT_KEY_CAPS_LOCK = 280,
    RE_INPUT_KEY_SCROLL_LOCK = 281,
    RE_INPUT_KEY_NUM_LOCK = 282,
    RE_INPUT_KEY_PRINT_SCREEN = 283,
    RE_INPUT_KEY_PAUSE = 284,
    RE_INPUT_KEY_F1 = 290,
    RE_INPUT_KEY_F2 = 291,
    RE_INPUT_KEY_F3 = 292,
    RE_INPUT_KEY_F4 = 293,
    RE_INPUT_KEY_F5 = 294,
    RE_INPUT_KEY_F6 = 295,
    RE_INPUT_KEY_F7 = 296,
    RE_INPUT_KEY_F8 = 297,
    RE_INPUT_KEY_F9 = 298,
    RE_INPUT_KEY_F10 = 299,
    RE_INPUT_KEY_F11 = 300,
    RE_INPUT_KEY_F12 = 301,
    RE_INPUT_KEY_F13 = 302,
    RE_INPUT_KEY_F14 = 303,
    RE_INPUT_KEY_F15 = 304,
    RE_INPUT_KEY_F16 = 305,
    RE_INPUT_KEY_F17 = 306,
    RE_INPUT_KEY_F18 = 307,
    RE_INPUT_KEY_F19 = 308,
    RE_INPUT_KEY_F20 = 309,
    RE_INPUT_KEY_F21 = 310,
    RE_INPUT_KEY_F22 = 311,
    RE_INPUT_KEY_F23 = 312,
    RE_INPUT_KEY_F24 = 313,
    RE_INPUT_KEY_F25 = 314,
    RE_INPUT_KEY_KP0 = 320,
    RE_INPUT_KEY_KP1 = 321,
    RE_INPUT_KEY_KP2 = 322,
    RE_INPUT_KEY_KP3 = 323,
    RE_INPUT_KEY_KP4 = 324,
    RE_INPUT_KEY_KP5 = 325,
    RE_INPUT_KEY_KP6 = 326,
    RE_INPUT_KEY_KP7 = 327,
    RE_INPUT_KEY_KP8 = 328,
    RE_INPUT_KEY_KP9 = 329,
    RE_INPUT_KEY_KP_DECIMAL = 330,
    RE_INPUT_KEY_KP_DIVIDE = 331,
    RE_INPUT_KEY_KP_MULTIPLY = 332,
    RE_INPUT_KEY_KP_SUBTRACT = 333,
    RE_INPUT_KEY_KP_ADD = 334,
    RE_INPUT_KEY_KP_ENTER = 335,
    RE_INPUT_KEY_KP_EQUAL = 336,
    RE_INPUT_KEY_LEFT_SHIFT = 340,
    RE_INPUT_KEY_LEFT_CONTROL = 341,
    RE_INPUT_KEY_LEFT_ALT = 342,
    RE_INPUT_KEY_LEFT_SUPER = 343,
    RE_INPUT_KEY_RIGHT_SHIFT = 344,
    RE_INPUT_KEY_RIGHT_CONTROL = 345,
    RE_INPUT_KEY_RIGHT_ALT = 346,
    RE_INPUT_KEY_RIGHT_SUPER = 347,
    RE_INPUT_KEY_MENU = 348,
    RE_INPUT_KEY_MAX
} ReInputKey;

/**
 * @brief Input event key data
 */
typedef struct ReInputEventKey {
    ReInputKey key;
    ReInputMode mode;
    ReInputMods mods;
} ReInputEventKey;

/**
 * @brief Input event button data
 */
typedef struct ReInputEventButton {
    ReInputButton button;
    ReInputMode mode;
    ReInputMods mods;
} ReInputEventButton;

/**
 * @brief Input event mouse data
 */
typedef struct ReInputEventMouse {
    ReInputMods mods;
    float x;
    float y;
} ReInputEventMouse;

/**
 * @brief Input event mouse wheel data
 */
typedef struct ReInputEventMouseWheel {
    ReInputMods mods;
    float x;
    float y;
} ReInputEventMouseWheel;

/**
 * @brief Input event window focus data
 */
typedef struct ReInputEventWindowFocus {
    i32 focused;
} ReInputEventWindowFocus;

/**
 * @brief Input event cursor enter
 */
typedef struct ReInputEventCursorEnter {
    i32 entered;
} ReInputEventCursorEnter;

/**
 * @brief Input event char data
 */
typedef struct ReInputEventChar {
    unsigned char character;
} ReInputEventChar;

/**
 * @brief Input type enumeration
 */
typedef enum ReInputType {
    RE_INPUT_TYPE_KEY = 0,
    RE_INPUT_TYPE_BUTTON = 1,
    RE_INPUT_TYPE_MOUSE = 2,
    RE_INPUT_TYPE_WHEEL = 3,
    RE_INPUT_TYPE_WINDOW_FOCUS = 4,
    RE_INPUT_TYPE_CURSOR_ENTER = 5,
    RE_INPUT_TYPE_CHAR = 6,
    RE_INPUT_TYPE_MAX
} ReInputType;

/**
 * @brief Input data
 */
typedef struct ReInput {
    ReInputType type;
    ReInputMode mode;
    ReInputMods mods;
    struct {
        union {
            unsigned char character;
            ReInputKey key;
            ReInputButton button;
            i32 focused;
            i32 entered;
            float wheel;
            struct {
                float x;
                float y;
            } mouse;
        };
    } data;
} ReInput;

/**
 * @brief Input binding callback
 */
typedef void (*ReInputBindingCallback)(ReInput input);


#endif //RAVEN_ENGINE_INPUT_INPUT_H
