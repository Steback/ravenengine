/// @file InputSystem.h
#ifndef RAVEN_ENGINE_INPUT_INPUT_SYSTEM_H
#define RAVEN_ENGINE_INPUT_INPUT_SYSTEM_H


#include "RavenEngine/input/Input.h"
#include "RavenEngine/core/Memory.h"
#include "RavenEngine/core/Logger.h"
#include "RavenEngine/event/EventSystem.h"


/**
 * @brief Input system
 */
typedef struct ReInputSystem {
    ReLogger* logger;
    ReMemoryAllocator* allocator;
    ReEventSystem* eventSystem;
} ReInputSystem;

/**
 * @brief Input system create info
 */
typedef struct ReInputSystemCreateInfo {
    ReLogger* logger;
    ReMemoryAllocator* allocator;
    ReEventSystem* eventSystem;
} ReInputSystemCreateInfo;

/**
 * @brief Create input system
 * @param createInfo Create info data reference
 * @return Input system reference, null if something go wrong
 */
ReInputSystem* reInputSystemCreate(const ReInputSystemCreateInfo* createInfo);

/**
 * @brief Destroy input system
 * @param system Input system reference
 */
void reInputSystemDestroy(ReInputSystem* system);


#endif //RAVEN_ENGINE_INPUT_INPUT_SYSTEM_H
