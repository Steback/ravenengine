/// @file Engine.h
#ifndef RAVEN_ENGINE_ENGINE_HPP
#define RAVEN_ENGINE_ENGINE_HPP


#include "Defines.h"


typedef struct ReWindow ReWindow;
typedef struct ReLogger ReLogger;
typedef struct ReSettings ReSettings;
typedef struct RePlatform RePlatform;
typedef struct ReFilesystem ReFilesystem;
typedef struct ReEventSystem ReEventSystem;
typedef struct ReApplication ReApplication;
typedef struct ReApplication ReApplication;
typedef struct ReInputSystem ReInputSystem;
typedef struct ReMemorySystem ReMemorySystem;
typedef struct ReMemoryAllocator ReMemoryAllocator;
typedef struct ReApplicationCreateInfo ReApplicationCreateInfo;

/**
 * @brief Engine context
 */
typedef struct ReEngine {
    b8 isRunning;
    u32 version;
    f64 deltaTime;
    ReSettings* settings;
    ReMemoryAllocator* allocator;
    ReLogger* logger;
    ReFilesystem* filesystem;
    ReApplication* application;
    RePlatform* platform;
    ReWindow* window;
    ReEventSystem* eventSystem;
    ReInputSystem* inputSystem;
    ReMemorySystem* memorySystem;
} ReEngine;

/**
 * @brief Engine context create info
 */
typedef struct ReEngineCreateInfo {
    ReApplicationCreateInfo* applicationCreateInfo;
} ReEngineCreateInfo;

/**
 * @brief Setup engine and systems
 * @param createInfo Engine create info data
 * @return Pointer to engine context, null if something is wrong
 */
ReEngine* reSetup(const ReEngineCreateInfo* createInfo);

/**
 * @brief Destroy and shutdown engine and systems
 * @param engine Valid pointer to engine context
 */
void reShutdown(ReEngine* engine);

/**
 * @brief Start loop and update/render functions
 * @param engine Valid pointer to engine context
 */
void reRun(ReEngine* engine);

/**
 * @brief Update all systems
 * @param engine Valid pointer to engine context
 * @param dt Delta time calculation
 */
void reUpdate(ReEngine* engine, float dt);

/**
 * @brief  buffers and send render commands
 * @param engine Valid pointer to engine context
 */
void reRender(ReEngine* engine);


#endif //RAVEN_ENGINE_ENGINE_HPP
