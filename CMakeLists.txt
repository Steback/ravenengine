cmake_minimum_required(VERSION 3.18)
project(RavenEngine VERSION 0.1 LANGUAGES C)

#### Settings ###
set(CMAKE_C_STANDARD 99)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY $<1:${CMAKE_SOURCE_DIR}/lib>)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY $<1:${CMAKE_SOURCE_DIR}/lib>)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY $<1:${CMAKE_SOURCE_DIR}/bin>)

if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    set(DEBUG_BUILD ON)
else ()
    set(DEBUG_BUILD OFF)
endif()

message(STATUS "[${PROJECT_NAME}] Debug enable: ${DEBUG_BUILD}")

function(set_debug_options name)
    if (${DEBUG_BUILD})
        target_compile_definitions(${name} PUBLIC RE_DEBUG_BUILD)
        target_compile_definitions(${name} PUBLIC LOG_TRACE_ENABLED=1)
        set_target_properties(${name} PROPERTIES OUTPUT_NAME "${name}-Debug")
    endif ()
endfunction()

if (NOT PLATFORM)
    if (WIN32)
        set(PLATFORM "Windows")
    elseif (UNIX AND NOT APPLE)
        set(PLATFORM "Linux")
    else ()
        error("Unknown platform")
    endif ()
endif ()

message(STATUS "[${PROJECT_NAME}] Platform: ${PLATFORM}")

### Engine ###
add_subdirectory(engine)

## Modules ###
file(GLOB directories LIST_DIRECTORIES true ${PROJECT_SOURCE_DIR}/modules/*)
foreach(directory ${directories})
    if(IS_DIRECTORY ${directory})
        if(EXISTS "${directory}/CMakeLists.txt")
            add_subdirectory(${directory})
        else()
            message(WARNING "[${PROJECT_NAME}] Directory in modules folder doesn't have a CMakeLists.txt: ${directory}")
        endif()
    endif()
endforeach()

## Editor ###
add_subdirectory(editor)

### Cleanup Cache ###
unset(PLATFORM CACHE)